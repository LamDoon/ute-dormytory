<?php

namespace App\Exports;

use App\Models\RoomRegistrationForm;
use App\Models\Semester;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ContractExport implements FromCollection, WithHeadings
{

    public function headings():array{
        return[            
            'Mã hợp đồng',
            'Học kỳ',
            'Họ tên sinh viên',
            'MSSV',
            'Khoá học',            
            'Số phòng',            
            'Thời gian đăng ký',
            'Ngày bắt đầu',
            'Ngày kết thúc',
        ];       
    }    
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $semester = Semester::where('status',1)->First();
        return collect(RoomRegistrationForm::getContract($semester));
    }
}
