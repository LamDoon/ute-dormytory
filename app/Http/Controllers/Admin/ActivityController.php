<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Service\CommendService;
use App\Http\Service\DisciplineService;
use App\Http\Service\StudentService;
use App\Models\Commend;
use App\Models\Discipline;

class ActivityController extends Controller
{
    protected $commendService;
    protected $studentService;
    protected $disciplineService;

    public function __construct(CommendService $commendService, 
    StudentService $studentService, DisciplineService $disciplineService)
    {
        $this->commendService = $commendService;
        $this->studentService = $studentService;
        $this->disciplineService = $disciplineService;
    }  

    public function commendList(){
        return view('admin.activities.commend-list',['title'=>'Danh sách khen thưởng', 
        'commends' => $this->commendService->getAll(),        
    ]);
    }

    public function createCommend(){
        return view('admin.activities.commend-create',['title'=>'Thêm khen thưởng',
        'students' => $this->studentService->getAllStudent(),
    ]);
    }

    public function storeCommend(Request $request){       
        $this->commendService->create($request);
        return redirect()->back();
    }  

    public function deleteCommend($id){
        $i = Commend::find($id);
        $i->del_flg = 1;
        $i->save();
        return response()->json(['message'=>'Xoá thành công.']);
    }    

    public function disciplineList(){
        return view('admin.activities.discipline-list',['title'=>'Danh sách kỷ luật', 
        'disciplines' => $this->disciplineService->getAll(),        
    ]);
    }

    public function createDiscipline(){
        return view('admin.activities.discipline-create',['title'=>'Thêm kỷ luật',
        'students' => $this->studentService->getAllStudent(),
    ]);
    }

    public function storeDiscipline(Request $request){       
        $this->disciplineService->create($request);
        return redirect()->back();
    }  

    public function deleteDiscipline($id){
        $i = Discipline::find($id);
        $i->del_flg = 1;
        $i->save();
        return response()->json(['message'=>'Xoá thành công.']);
    }    
    
}
