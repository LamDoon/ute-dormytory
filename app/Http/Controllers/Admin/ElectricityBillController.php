<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Service\ElectricBillService;
use App\Models\ElectricityBill;
use Illuminate\Http\Request;

class ElectricityBillController extends Controller
{
    protected $electricBillService;

    public function __construct(ElectricBillService $electricBillService)
    {
        $this->electricBillService = $electricBillService;
    }  

    public function index(Request $request){        
        return view('admin.electricity.bill',['title'=>'Danh sách hoá đơn điện nước', 
        'bills_unpaid' => $this->electricBillService->getBills($request,0),
        'bills_paid' => $this->electricBillService->getBills($request,1),
        'semester' =>$this->electricBillService->getAllSemester(),
        
    ]);        
    }

    public function updatePaid(ElectricityBill $bill){             
        $this->electricBillService->updatePaid($bill);
        return redirect()->back();
    }

    public function create(){
        return view('admin.electricity.create-bill',[
            'title'=>'Tạo phiếu thông tin điện nước',
            'semester'=>$this->electricBillService->getSemester(),
            'electrics' => $this->electricBillService->getElectricIndex(),
            'rooms'=> $this->electricBillService->getRoom(),
            'bills'=>$this->electricBillService->getCreatedBills(),
        ]);
    }

    public function store(Request $request){
        $this->electricBillService->create($request);
        return redirect()->back();
    }

    public function delete($id){
        $e = ElectricityBill::find($id);
        $e->del_flg = 1;
        $e->save();
        return response()->json(['message'=>'Xoá thành công thông tin chỉ số điện.']);
    }

}
