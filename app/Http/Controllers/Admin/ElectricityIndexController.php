<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Service\ElectricIndexService;
use App\Models\ElectricityIndex;
use Illuminate\Http\Request;

class ElectricityIndexController extends Controller
{
    protected $electricIndexService;

    public function __construct(ElectricIndexService $electricIndexService)
    {
        $this->electricIndexService = $electricIndexService;
    }  

    public function index(){
        return view('admin.electricity.list',['title'=>'Danh sách chỉ số điện nước', 
        'electrics' => $this->electricIndexService->getAll()]);
    }

    public function update(ElectricityIndex $electric){
        $this->electricIndexService->update($electric);
        return redirect()->back();
    }

    public function delete($id){
        $e = ElectricityIndex::find($id);
        $e->del_flg = 1;
        $e->save();
        return response()->json(['message'=>'Xoá thành công thông tin chỉ số điện.']);
    }

    public function create(){       
        return view('admin.electricity.create',['title'=>'Thêm chỉ số điện nước',
        'electrics'=>$this->electricIndexService->getAll(),
        ]);
    }

    public function store(Request $request){               
        $this->electricIndexService->create($request);
        return redirect()->back();
    }

}
