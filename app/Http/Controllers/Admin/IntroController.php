<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Service\IntroService;
use App\Models\Intro;

class IntroController extends Controller
{

    protected $introService;

    public function __construct(IntroService $introService)
    {
        $this->introService = $introService;
    }  

    public function index(){
        return view('admin.intros.list',['title'=>'Danh sách tin giới thiệu', 
        'intros' => $this->introService->getAll()]);
    }

    public function create(){
        return view('admin.intros.create',['title'=>'Thêm tin giới thiệu']);
    }

    public function store(Request $request){       
        $this->introService->create($request);
        return redirect()->back();
    }

    public function showIntro(Intro $intro){
        $this->introService->showIntro($intro);
        return redirect()->back();
    }

    public function delete($id){
        $i = Intro::find($id);
        $i->del_flg = 1;
        $i->save();
        return response()->json(['message'=>'Xoá thành công tin giới thiệu.']);
    }    
    
}
