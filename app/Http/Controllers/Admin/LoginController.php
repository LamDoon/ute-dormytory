<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function getLogin(){
        return view('admin.login',['title'=>'Đăng nhập Quản trị viên']);
    }
    public function create(){
        echo  bcrypt('123');
    }

    public function postLogin(Request $request){
        $this->validate($request,[
            'email'=>'required|email:filter',
            'password'=>'required'
        ]);

        if (Auth::attempt([
            'email'=>$request->input('email'),
            'password'=>$request->input('password'),
            'status'=>1,
            'del_flg'=>0,
        ], $request->input('remember'))){                                  
            return redirect()->route('admin');
        }

        Session::flash('error','Email or Password incorrect');
        return redirect()->back();
    }

    public function getLogout(){
        Auth::logout();
        return redirect()->route('login'); 
    }
}
