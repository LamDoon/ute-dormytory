<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MainController extends Controller
{
    public function index(Request $request){   
        $admin = Auth::user()->id;             
        $request->session('admin')->put('admin',$admin);             
        return view('admin.home',['title'=>'Home']);
    }
}
