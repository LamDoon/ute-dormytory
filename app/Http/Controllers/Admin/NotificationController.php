<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Http\Service\NoteService;
use App\Models\Notification;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    protected $noteService;

    public function __construct(NoteService $noteService)
    {
        $this->noteService = $noteService;
    }  

    public function index(){
        return view('admin.notes.list',['title'=>'Danh sách tin thông báo', 
        'notes' => $this->noteService->getAll()]);
    }

    public function create(){
        return view('admin.notes.create',['title'=>'Thêm tin thông báo']);
    }

    public function store(Request $request){       
        $this->noteService->create($request);
        return redirect()->back();
    }

    public function delete($id){
        $note = Notification::find($id);
        $note->del_flg = 1;
        $note->save();
        return response()->json(['message'=>'Xoá thành công thông báo.']);
    }

}
