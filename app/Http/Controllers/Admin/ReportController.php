<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Service\RoomRegistrationFormService;
use App\Http\Service\SemesterService;
use Illuminate\Http\Request;
use App\Exports\ContractExport;
use App\Http\Service\StudentService;
use App\Http\Service\VehicleService;
use Excel;

class ReportController extends Controller
{

    protected $contractService;
    protected $semesterService;
    protected $studentService;
    protected $vehicleService;


    public function __construct(RoomRegistrationFormService $contractService, 
    SemesterService $semesterService, StudentService $studentService, VehicleService $vehicleService)
    {
        $this->contractService = $contractService;
        $this->semesterService = $semesterService;;
        $this->studentService = $studentService;
        $this->vehicleService = $vehicleService;
    }   

    public function getReport(){
        $semester = $this->semesterService->getCurrentSemester();       
        $contracts = $this->contractService->getContractsBySemester($semester);   
        $students = $this->studentService->getAllStudent();       
        $stuInSem = (count($contracts) * 100) / count($students);
        $vehInSem = ($semester->veh_num * 100) / $semester->veh_max;    
        return view('admin.reports.detail',[
            'title'=>'Thống kê',
            'contracts'=>$contracts,
            'semester'=> $semester,
            'stuNum' => count($students),
            'stuInSem'=> $stuInSem,
            'vehInSem'=>$vehInSem,
        ]);
    }

    public function exportIntoExcel(){
        return Excel::download(new ContractExport,'ContractList.xlsx');
    }

    public function exportIntoCSV(){
        return Excel::download(new ContractExport,'ContractList.csv');
    }
}
