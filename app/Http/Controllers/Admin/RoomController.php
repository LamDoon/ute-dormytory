<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Http\Service\RoomService;
use App\Models\Room;
use Illuminate\Http\Request;

class RoomController extends Controller
{
    protected $roomService;

    public function __construct(RoomService $roomService)
    {
        $this->roomService = $roomService;
    }

    public function index(){       
        return view('admin.rooms.list',['title'=>'Danh sách phòng',
        'a1'=> $this->roomService->getRoom('A',1),
        'a2'=> $this->roomService->getRoom('A',2),
        'a3'=> $this->roomService->getRoom('A',3),
        'a4'=> $this->roomService->getRoom('A',4),
        'a5'=> $this->roomService->getRoom('A',5),
        'b1'=> $this->roomService->getRoom('B',1),
        'b2'=> $this->roomService->getRoom('B',2),
        'b3'=> $this->roomService->getRoom('B',3),
        'b4'=> $this->roomService->getRoom('B',4),
        'b5'=> $this->roomService->getRoom('B',5),
        'c1'=> $this->roomService->getRoom('C',1),
        'c2'=> $this->roomService->getRoom('C',2),
        'c3'=> $this->roomService->getRoom('C',3),
        'c4'=> $this->roomService->getRoom('C',4),
        'c5'=> $this->roomService->getRoom('C',5),    
    ]);
    }

    public function detail(Room $room){
        return view('admin.rooms.detail',['title'=>'Chi tiết phòng',
        'room'=>$room,
        'students'=>$this->roomService->getDetailStudent($room),
    ]);
    }

    public function update(Request $request, Room $room){
        $this->roomService->update($request, $room);
        return redirect()->back();
    }

    public function reset(){
        $this->roomService->reset();
        return redirect()->back();    }
}
