<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Service\RoomRegistrationFormService;
use App\Models\RoomRegistrationForm;
use Illuminate\Support\Facades\Redis;

class RoomRegistrationFormController extends Controller
{

    protected $roomRegistrationFormService;

    public function __construct(RoomRegistrationFormService $roomRegistrationFormService)
    {
        $this->roomRegistrationFormService = $roomRegistrationFormService;
    }

    public function index(){
        return view('admin.contracts.list',[
            'title'=>'Đơn đăng ký nội trú',
            'contracts'=>$this->roomRegistrationFormService->getContracts(),        
        ]
    );
    }

    public function approveForm(Request $request){        
        return view('admin.contracts.approve',
        ['title'=>'Phê duyệt đơn đăng ký nội trú',
        'contracts'=>$this->roomRegistrationFormService->getApproveForms($request),
    ]);
    }

    public function detail($contract){      
        return view('admin.contracts.detail',
        ['title'=>'Phê duyệt yêu cầu đăng ký nội trú',
        'contract'=>$this->roomRegistrationFormService->getApproveFormsDetail($contract),
        'semester'=>$this->roomRegistrationFormService->getSemester(),
    ]);
    }

    public function update(RoomRegistrationForm $contract){
        $contractId = $contract['id'];
        $data = $this->roomRegistrationFormService->getApproveFormsDetail($contractId);
        $this->roomRegistrationFormService->update($contract, $data);
       return redirect()->route('approveContract');
    }

    public function cancel(Request $request, RoomRegistrationForm $contract){
        $this->roomRegistrationFormService->cancelContract($contract, $request->reason);
        return redirect()->route('approveContract');
    }

    public function getFeeList(Request $request){
        return view('admin.contracts.fee-management', [
            'title'=> 'Danh sách phí nội trú sinh viên',
            'contracts_unpaid'=>$this->roomRegistrationFormService->getFeeList($request, 0),
            'contracts_paid'=>$this->roomRegistrationFormService->getFeeList($request, 1),
            'semester'=>$this->roomRegistrationFormService->getAllSemester(),
        ]);     
    }

    public function updatePaid(RoomRegistrationForm $contract){
        $this->roomRegistrationFormService->updatePaid($contract);
        return redirect()->back();
    }

    public function delete($id){
        $form = RoomRegistrationForm::find($id);
        $form->del_flg = 1;
        $form->save();
        return response()->json(['message'=>'Xoá thành công.']);
    }
    
}
