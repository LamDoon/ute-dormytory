<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Http\Service\SemesterService;
use App\Models\Semester;
use Illuminate\Http\Request;

class SemesterController extends Controller
{
    protected $semesterService;

    public function __construct(SemesterService $semesterService)
    {
        $this->semesterService = $semesterService;
    }     

    public function create(){       
        return view('admin.semesters.create',['title'=>'Thêm học kỳ',
        'semesters'=>$this->semesterService->getAll(),
        ]);
    }

    public function store(Request $request){       
        $this->semesterService->create($request);
        return redirect()->back();
    }

    public function getCreateVehicle(){
        return view('admin.vehicles.create',[
            'title'=>'Tạo đăng ký xe',
            'semester'=>$this->semesterService->getCurrentSemester(),
        ]);
    }

    public function postCreateVehicle(Semester $semester, Request $request){
        $this->semesterService->createVehicle($semester, $request);
        return redirect()->back();
    }

}
