<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Http\Service\StudentParentService;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Http\Request;
use App\Mail\SignupEmail;
use Illuminate\Support\Facades\Mail;
use App\Http\Service\StudentService;
use App\Models\Student;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Session;

class StudentController extends Controller 
{
    protected $studentService;

    protected $parentService;

    public function __construct(StudentService $studentService, StudentParentService $parentService)
    {
        $this->studentService = $studentService;
        $this->parentService = $parentService;
    }  
    
    public function index(Request $request){     
        return view('admin.students.list',['title'=>'Danh sách sinh viên',
        'students'=>$this->studentService->getAll($request),
    ]);
    }

    public function create(){
        return view('admin.students.create',['title'=>'Tạo tài khoản sinh viên',
        'depart'=>$this->studentService->getDepart(),
        'parents'=>$this->parentService->getAll(),
    ]);
    }

    public function store(Request $request){
        $this->studentService->create($request);
        return redirect()->back();
    }

    public function detail(Student $student){
        $parent = $this->parentService->getParentByStudent($student->relat_id);         
        return view('admin.students.detail',['title'=>'Thông tin sinh viên',
        'student'=>$student,
        'parent'=>$parent,
    ]);
    }

    public function getActive(Student $student){
        return view('admin.students.disable',['title'=>'Vô hiệu hoá tài khoản sinh viên',
        'student'=>$student,        
        's_disable'=>$this->studentService->getDisable(),
    ]);
    }

    public function postActive(Student $student){
        $this->studentService->active($student);
        return redirect()->back();
    }

    public function login(Request $request){
        $this->studentService->postLogin($request);
        return redirect()->back();
    }

    public function logout(){
        if (session()->has('student')){
            session()->pull('student');
            session()->pull('name');            
        }
        return redirect('home');
    }

    public function delete($id){
        $s = Student::find($id);
        $s->del_flg = 1;
        $s->save();
        return response()->json(['message'=>'Xoá sinh viên khỏi danh sách thành công.']);
    }

    public function getVehicle(Student $student){
        return view('admin.students.vehicle',[
            'title'=>'Thêm đăng ký xe',
            'student'=>$student,
        ]);
    }

    public function postVehicle(Student $student, Request $request){        
        $this->studentService->postVehicle($student, $request);
        return redirect()->back();
    }

    
}
