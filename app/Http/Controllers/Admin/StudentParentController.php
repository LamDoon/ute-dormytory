<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Service\StudentParentService;

class StudentParentController extends Controller
{
    protected $parentService;

    public function __construct(StudentParentService $parentService)
    {
        $this->parentService = $parentService;
    }

    public function index(){
        return view('admin.parents.list',['title'=>'Danh sách thân nhân', 
        'parents' => $this->parentService->getAll()]);
    }

    public function create(){
        return view('admin.parents.create',[
            'title'=>'Tạo thông tin nhân nhân',            
        ]);
    }

    public function store(Request $request){
        $this->parentService->create($request);
        return redirect()->back();
    }
}
