<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Service\UserService;
use App\Models\User;
use Illuminate\Support\Facades\Redis;

class UserController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }
    public function index(){
        return view('admin.users.list',[
            'title'=>'Danh sách nhân viên',
            'users'=>$this->userService->getAll(),
            'positions'=>$this->userService->getPosition(),
        ]);
    }

    public function create(){
        return view('admin.users.create',[
            'title'=>'Thêm nhân viên',
            'positions'=>$this->userService->getPosition(),
        ]);
    }

    public function store(Request $request){
        $this->userService->create($request);
        return redirect()->back();
    }

    public function detail(User $user){
        return view('admin.users.detail',['title'=>'Thông tin nhân viên',
        'user'=>$user,
        'positions'=>$this->userService->getPosition()]);
    }

    public function getActive(User $user){
        return view('admin.users.disable',['title'=>'Vô hiệu hoá tài khoản nhân viên',
        'user'=>$user,
        'positions'=>$this->userService->getPosition(),
        'userdisable'=>$this->userService->getDisable(),
    ]);
    }

    public function postActive(User $user){
        $this->userService->active($user);
        return redirect()->back();
    }

    public function show(User $user){
        return view('admin.users.edit',['title'=>'Cập nhật thông tin cá nhân',
        'user'=>$user, 
        'positions'=>$this->userService->getPosition(),   
    ]);
    }

    public function update(Request $request, User $user){
       $this->userService->updateInfo($request, $user);
       return redirect()->back();
    }

    public function postAtm(Request $request, User $user)
    {
        $this->userService->updateAtm($request, $user);
        return redirect()->back();
    }
}
