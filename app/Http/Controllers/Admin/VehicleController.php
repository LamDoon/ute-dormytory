<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Service\VehicleService;
use App\Models\Vehicle;

class VehicleController extends Controller
{
    protected $vehicleService;

    public function __construct(VehicleService $vehicleService)
    {
        $this->vehicleService = $vehicleService;
    }

    public function index(){
        return view('admin.vehicles.list',['title'=>'Danh sách đăng ký vé gửi xe', 
        'forms' => $this->vehicleService->getForm()]);
    }

    public function delete($id){
        $v = Vehicle::find($id);
        $v->del_flg = 1;
        $v->save();
        return response()->json(['message'=>'Xoá thành công.']);
    }
}
