<?php

namespace App\Http\Controllers\Index;

use App\Http\Controllers\Controller;
use App\Http\Service\HomeService;
use App\Http\Service\RoomRegistrationFormService;
use App\Http\Service\SemesterService;
use App\Http\Service\StudentService;
use App\Http\Service\VehicleService;
use App\Models\Intro;
use App\Models\Room;
use Illuminate\Http\Request;

class MainIndexController extends Controller
{
    protected $homeService;
    protected $semesterService;
    protected $studentService;
    protected $vehicleService;
    protected $contractService;

    public function __construct(HomeService $homeService, SemesterService $semesterService, 
    StudentService $studentService, VehicleService $vehicleService, RoomRegistrationFormService $contractService)
    {
        $this->homeService = $homeService;
        $this->semesterService = $semesterService; 
        $this->studentService = $studentService;    
        $this->vehicleService   = $vehicleService; 
        $this->contractService = $contractService;   
      
    }  

    public function index(){        
        return view('index.home',
        [   'title'=>'Ký túc xá Đại học Sư phạm Kỹ thuật Đà Nẵng',
            'intros'=>$this->homeService->getIntros(),
            'aF'=>$this->homeService->getRoomsFirst('A'),
            'aL'=>$this->homeService->getRoomsLast('A'),
            'bF'=>$this->homeService->getRoomsFirst('B'),
            'bL'=>$this->homeService->getRoomsLast('B'),
            'cF'=>$this->homeService->getRoomsFirst('C'),
            'cL'=>$this->homeService->getRoomsLast('C'),
            'notes'=>$this->homeService->getNotes(),
            'semester'=>$this->homeService->getSemester(),
        ]);
    }

    public function getRegist($room){  
        $student =$this->homeService->getStudent();
        $semester =$this->homeService->getSemester();
        $check = $this->contractService->checkRegistered($student, $semester);        
        return view('index.room-registration',
        [   'title'=>'Đăng ký phòng',
            'student'=>$student,       
            'room'=>$this->homeService->getRoom($room),
            'semester'=>$semester,
            'checkRegistered'=>$check,        
        ]);
    }

    public function postRegist($room){
        $semester=$this->homeService->getSemester();
        
        $data = [
            'student_id'=>session('student'),
            'room_id'=>$room,
            'semester_id'=>$semester->id,
            'start_date'=>$semester->start_date,
            'end_date'=>$semester->end_date  
        ]; 

        $this->homeService->createRegist($data);
        return redirect()->back();      
    }

    public function getRegistVehicle(){
        $semester = $this->semesterService->getCurrentSemester();       
        $student = $this->studentService->getById(session('student'));
        $check = $this->vehicleService->checkRegistered($student,$semester);        
        return view('index.vehicle-registration',[
            'title'=>'Đăng ký xe',
            'semester'=>$semester,
            'student'=>$student,
            'checkRegistered'=>$check,
        ]);
    }

    public function postRegistVehicle(){
        $semester = $this->semesterService->getCurrentSemester();
        $student = $this->studentService->getById(session('student'));
        $this->vehicleService->create($student, $semester);
        $this->semesterService->updateVehicle($semester);
        return redirect()->back();
    }

    public function getProfile(){
        return view('index.profile',[
            'title'=>'Thông tin cá nhân',
            'student' => $this->studentService->getById(session('student')),
        ]);
    }

    public function changePass(Request $request){        
        $student = $this->studentService->getById(session('student'));
        $this->studentService->changePass($student, $request);
        return redirect()->back();
    }

    public function history(){
        $semester = $this->semesterService->getCurrentSemester();
        $student = $this->studentService->getById(session('student'));
        return view('index.history',[
            'title'=>'Lịch sử đăng ký phòng',
            'contracts' => $this->contractService->getContractsByStudent($student,$semester),
        ]);
    }

    
}
