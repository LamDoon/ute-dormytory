<?php

namespace App\Http\Service;

use App\Models\Commend;
use App\Models\Intro;
use Exception;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class CommendService {


    public function getAll(){       
        return Commend::where('del_flg','not like',1)->orderByDesc('id')->paginate(10);       
    }

    public function create($request){            
        $request->validate([
            'student_id'=>'required',            
        ]);

        $commend = new Commend();

        try {
            
            $commend->student_id = $request->student_id;
            $commend->reason = $request->reason; 
            $commend->pattern = $request->pattern;
            $commend->created_date = $request->created_date;          

            
            $commend->save();
            Session::flash('success','Một khen thưởng đã được thêm thành công.');

        }catch(Exception $err){
            Session::flash('error',$err->getMessage());
            return false;
        }
        return true;
    }

    public function showIntro($intro){
        if($intro->status == 1)
        {
            $intro->status = 0;
        }else{
            $intro->status = 1;
        }        
        $intro->save();
        Session::flash('success','Thao tác thành công!');
    }


}
