<?php

namespace App\Http\Service;

use App\Models\Commend;
use App\Models\Discipline;
use App\Models\Intro;
use Exception;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class DisciplineService {


    public function getAll(){       
        return Discipline::where('del_flg','not like',1)->orderByDesc('id')->paginate(10);       
    }

    public function create($request){            
        $request->validate([
            'student_id'=>'required',            
        ]);

        $discipline = new Discipline();

        try {
            
            $discipline->student_id = $request->student_id;
            $discipline->reason = $request->reason; 
            $discipline->pattern = $request->pattern;
            $discipline->created_date = $request->created_date;          

            
            $discipline->save();
            Session::flash('success','Một kỷ luật đã được thêm thành công.');

        }catch(Exception $err){
            Session::flash('error',$err->getMessage());
            return false;
        }
        return true;
    }

   


}
