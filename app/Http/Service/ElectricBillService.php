<?php

namespace App\Http\Service;

use App\Models\ElectricityBill;
use App\Models\ElectricityIndex;
use App\Models\Room;
use App\Models\Semester;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class ElectricBillService {

    public function getBills($request, $status){
        $bills = ElectricityBill::select(
            'electricity_bills.*',
            'rooms.code as room_code',
            'semesters.name as semester_name'
            )        
        ->join('rooms', 'rooms.id', '=', 'electricity_bills.room_id')   
        ->join('semesters', 'semesters.id', '=', 'electricity_bills.room_id')       
        ->where('electricity_bills.paid',$status)         
        ->where('electricity_bills.semester_id',$request->semester_id)
        ->where('rooms.code','like','%'.$request->room.'%')    
        ->where('electricity_bills.del_flg','not like',1)   
        ->paginate(20);
        return $bills;
    }

    public function getAllSemester(){
        return Semester::orderByDesc('name')->get();
    }

    public function getSemester(){
        return Semester::where('status',1)->First();
    }

    public function getRoom(){
        return Room::orderBy('region')->get();
    }

    public function getElectricIndex(){
        return ElectricityIndex::where('del_flg',0)->orderByDESC('id')->get();   
    }

    public function updatePaid($bill){           
        $current = Carbon::now()->format('Y-m-d H:i:s');   
        $bill->paid = 1; 
        $bill->paid_date = $current;            
        $bill->save();
        Session::flash('success','Cập nhật thành công!');        
    }   
    
    public function getCreatedBills(){
        $s = $this->getSemester();
        
        $bills = ElectricityBill::select(
            'electricity_bills.*',
            'rooms.code as room_code',
            'rooms.region',
            )        
        ->join('rooms', 'rooms.id', '=', 'electricity_bills.room_id')               
        ->where('electricity_bills.semester_id',$s->id)            
        ->where('electricity_bills.del_flg','not like',1)   
        ->paginate(20);        
        return $bills;
    }

    public function create($request){     
          
        $request->validate([
            'old_e'=>'required|numeric|min:0',
            'new_e'=>'required|numeric|min:0',
            'old_w'=>'required|numeric|min:0',
            'new_w'=>'required|numeric|min:0',           
        ]);

        $bill = new ElectricityBill();

        $electricIndex = ElectricityIndex::where('id',$request->index_id)->First();
        $total = (abs($request->new_e - $request->old_e) * $electricIndex->e_price) + (abs($request->new_w - $request->old_w) * $electricIndex->w_price);

        $s = $this->getSemester();
        try {
            $bill->old_e = $request->old_e;
            $bill->new_e = $request->new_e; 
            $bill->old_w = $request->old_w;
            $bill->new_w = $request->new_w;  
            $bill->index_id = $request->index_id;
            $bill->room_id = $request->room_id;
            $bill->semester_id = $s->id;
            $bill->employee_id = Auth::user()->id;
            $bill->total = $total;

            $bill->save();
            Session::flash('success','Đã thêm phiếu điện nước thành công.');

        }catch(Exception $err){
            Session::flash('error',$err->getMessage());
            return false;
        }
        return true;
    }

}