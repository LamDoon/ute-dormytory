<?php

namespace App\Http\Service;

use App\Models\ElectricityIndex;
use App\Models\Semester;
use Exception;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class ElectricIndexService {


    public function getAll(){
        return ElectricityIndex::where('del_flg',0)->orderByDESC('id')->paginate(20);        
    }

    public function update($electric){
        if($electric->status == 0)
        {
            $electric->status = 1;
            $e = ElectricityIndex::where('status',1)->First();
                $e->update([
                    $e->status= 0,
                ]);
        }              
        $electric->save();
        Session::flash('success','Thao tác thành công!');
    }

    public function create($request){        
        $request->validate([            
            'e_price'=>'required',   
            'w_price'=>'required',         
        ]);

        $electric = new ElectricityIndex();

        try {           

            $electric->e_price = $request->e_price;
            $electric->w_price = $request->w_price;  
            
            $electric->save();
            Session::flash('success','Chỉ số điện đã được thêm thành công.');
        }catch(Exception $err){
            Session::flash('error',$err->getMessage());
            return false;
        }
        return true;
    }
}