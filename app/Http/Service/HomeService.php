<?php

namespace App\Http\Service;

use App\Models\Intro;
use App\Models\Notification;
use App\Models\Room;
use App\Models\RoomRegistrationForm;
use App\Models\Semester;
use App\Models\Student;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class HomeService {

    public function getIntros(){       
        return Intro::where('del_flg','not like',1)
        ->where('status',1)->orderByDesc('id')->get();       
    }

    public function getRoomsFirst($region){
        return Room::where('region',$region)->orderBy('code','ASC')->take(25)->get();
    }

    public function getRoomsLast($region){
        return Room::where('region',$region)->orderBy('code','DESC')->take(25)->get();
    }

    public function getNotes(){
        return Notification::where('del_flg','not like',1)
        ->orderByDesc('id')->take(3)->get();
    }

    public function getStudent(){
        if (session()->has('student')){
            $id = session('student');            
        }
        else {
            return false;
        }                 
        return Student::where('id', $id)->First(); 
    }

    public function getRoom($id){
        return Room::where('id', $id)->First(); 
    }

    public function getSemester(){
        return Semester::where('status', 1)->First(); 
    }

    public function createRegist($data){      
        $current = Carbon::now()->format('Y-m-d H:i:s');        
        try{
                RoomRegistrationForm::create([
                    'student_id'=>$data['student_id'],
                    'room_id'=>$data['room_id'],
                    'semester_id'=>$data['semester_id'],
                    'start_date'=>$data['start_date'],
                    'end_date'=>$data['end_date'],
                    'created_date'=> $current,
                    
                ]);        
                Session::flash('success','Bạn đã gửi yêu cầu đăng ký phòng thành công. Hãy đợi BQL phê duyệt.');
            }catch(Exception $err){
                Session::flash('error',$err->getMessage());
                return false;
            }
            return true;
    
        
    }

}


