<?php

namespace App\Http\Service;

use App\Models\Intro;
use Exception;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class IntroService {


    public function getAll(){       
        return Intro::where('del_flg','not like',1)->orderByDesc('status')->paginate(10);       
    }

    public function create($request){        
        $request->validate([
            'title'=>'required',
            'content'=>'required',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);

        $intro = new Intro();

        try {

            if(isset($request->status)){
                $intro->status = 1;
            }
            $intro->title = $request->title;
            $intro->content = $request->content;           

            if ($request->hasFile('image')){
                $file = $request->file('image');
                $extension = $file->getClientOriginalExtension();
                $newImageName = time().'.intro_image.'.$extension;
                $file->move(public_path('assets/index/assets/images'),$newImageName);
                $intro->image = $newImageName;
            }
            $intro->save();
            Session::flash('success','Một tin giới thiệu đã được thêm thành công.');

        }catch(Exception $err){
            Session::flash('error',$err->getMessage());
            return false;
        }
        return true;
    }

    public function showIntro($intro){
        if($intro->status == 1)
        {
            $intro->status = 0;
        }else{
            $intro->status = 1;
        }        
        $intro->save();
        Session::flash('success','Thao tác thành công!');
    }


}
