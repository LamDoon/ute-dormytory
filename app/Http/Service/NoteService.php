<?php

namespace App\Http\Service;

use App\Models\Notification;
use Exception;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class NoteService {


    public function getAll(){       
        return Notification::where('del_flg','not like',1)->orderByDesc('status')->paginate(10);       
    }

    public function create($request){        
        $request->validate([
            'title'=>'required',
            'content'=>'required',            
        ]);

        $note = new Notification();

        try {
            $note->title = $request->title;
            $note->content = $request->content;    

            $note->save();
            Session::flash('success','Một tin thông báo đã được thêm thành công.');

        }catch(Exception $err){
            Session::flash('error',$err->getMessage());
            return false;
        }
        return true;
    }
}