<?php

namespace App\Http\Service;

use App\Models\Room;
use App\Models\RoomRegistrationForm;
use App\Models\Semester;
use App\Models\Student;
use App\Mail\ContractEmail;
use App\Mail\CancelContractEmail;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use League\Flysystem\Exception;
use PhpParser\Node\Expr\FuncCall;

class RoomRegistrationFormService
{

    public function getContracts(){    
        $contracts = RoomRegistrationForm::select(
            'room_registration_forms.*',         
            'students.name',
            'students.code as student_code',            
            'rooms.id as room_id',
            'rooms.code as room_code',
            )
        ->join('students', 'students.id', '=', 'room_registration_forms.student_id')
        ->join('rooms', 'rooms.id', '=', 'room_registration_forms.room_id')  
        ->where('room_registration_forms.status',1)    
        ->where('room_registration_forms.del_flg','not like',1)   
        ->paginate(20);
        return $contracts;
    } 

    public function getApproveForms($request){    
        $contracts = RoomRegistrationForm::select(
            'room_registration_forms.id',   
            'room_registration_forms.status', 
            'room_registration_forms.created_date',         
            'students.name',
            'students.code as student_code',            
            'rooms.id as room_id',
            'rooms.code as room_code',
            )
        ->join('students', 'students.id', '=', 'room_registration_forms.student_id')
        ->join('rooms', 'rooms.id', '=', 'room_registration_forms.room_id')  
        ->where('room_registration_forms.status',0)        
        ->where('rooms.code','like','%'.$request->key_search.'%')    
        ->where('room_registration_forms.del_flg','not like',1)   
        ->paginate(20);

        return $contracts;
    }  
    
    public function getSemester(){
        return Semester::where('status', 1)->First(); 
    }

    public function getApproveFormsDetail($contract){    
        $contract = RoomRegistrationForm::select(
            'room_registration_forms.id',
            'room_registration_forms.created_date',
            'room_registration_forms.start_date',
            'room_registration_forms.end_date',
            'room_registration_forms.total',
            'room_registration_forms.status',
            'students.name',
            'students.code as student_code',
            'students.email',
            'students.image',        
            'students.address',        
            'students.card_id',        
            'students.gender',
            'students.class',
            'students.depart',
            'students.course',
            'students.national',
            'students.ethnic',
            'students.phone',
            'rooms.id as room_id',
            'rooms.code as room_code',
            'rooms.regist_num',
            'rooms.regist_max',
            'rooms.region',
            'rooms.floors',)
        ->join('students', 'students.id', '=', 'room_registration_forms.student_id')
        ->join('rooms', 'rooms.id', '=', 'room_registration_forms.room_id')          
        ->where('room_registration_forms.id', $contract)        
        ->First();
        return $contract;
    }   

    public function update($contract, $data){        
       $day =  abs(strtotime($data['end_date']) - strtotime($data['start_date'])) / (60*60*24);      
       $month = $day / 30;
       
       $semester = $this->getSemester();
       $price = $semester->price;

       $total = $month * $price;

       $room = Room::where('id',$data['room_id'])->first();    
        try{
            $contract->update([
                'status'=> 1,
                'employee_id'=>Auth::user()->id,    
                'total' => $total,
            ]); 
            $room->update([
                'regist_num'=> $room->regist_num +1,
            ]);            
         $this->sendMail($data['email'], $data['name'], $data['room_code'], $data['start_date'], $data['end_date'], $contract->total);   
           Session::flash('success','Yêu cầu đăng ký đã được phê duyệt. Hệ thống sẽ gửi mail thông báo đến sinh viên.');
        }catch(Exception $err){
            Session::flash('error',$err->getMessage());
            return false;
        }
        return true;
    }
    
    public function cancelContract($contract, $reason){   
      
         $student = Student::where('id',$contract->student_id)->First();      
         $email = $student->email;
         $name = $student->name;  
         try{
             $contract->update([
                 'del_flg'=> 1,                 
             ]);                        
         $this->sendCancelMail($email, $reason, $name);   
            Session::flash('error','Yêu cầu đăng ký đã được huỷ bỏ. Hệ thống sẽ gửi mail thông báo đến sinh viên.');
         }catch(Exception $err){
             Session::flash('error',$err->getMessage());
             return false;
         }
         return true;
     }

    public function sendMail($email,$name, $room, $start, $end, $total){
        $data = [
            'name'=>$name,
            'room'=>$room,
            'start'=>$start,
            'end'=>$end,
            'total'=>$total,
        ];
        Mail::to($email)->send(new ContractEmail($data));    
    }  

    public function sendCancelMail($email, $reason, $name){
        $data = [
            'reason'=>$reason,
            'name'=>$name,            
        ];
        Mail::to($email)->send(new CancelContractEmail($data));    
    } 
    
    public function getFeeList($request, $status){
        $contracts = RoomRegistrationForm::select(
            'room_registration_forms.id',   
            'room_registration_forms.paid_date', 
            'room_registration_forms.created_date',
            'room_registration_forms.semester_id',  
            'room_registration_forms.total',       
            'students.name',
            'students.image',
            'students.code as student_code',            
            'rooms.id as room_id',
            'rooms.code as room_code',
            )
        ->join('students', 'students.id', '=', 'room_registration_forms.student_id')
        ->join('rooms', 'rooms.id', '=', 'room_registration_forms.room_id')  
        ->where('room_registration_forms.status',1)
        ->where('room_registration_forms.paid',$status)         
        ->where('room_registration_forms.semester_id',$request->semester_id)
        ->where('students.code','like','%'.$request->key_search.'%')    
        ->where('room_registration_forms.del_flg','not like',1)   
        ->paginate(20);

        return $contracts;
    }

    public function getAllSemester(){
        return Semester::orderByDesc('name')->get();
    }

    public function updatePaid($contract){     
        $current = Carbon::now()->format('Y-m-d H:i:s');   
        $contract->paid = 1; 
        $contract->paid_date = $current;            
        $contract->save();
        Session::flash('success','Cập nhật thanh công!');        
    }

    public function checkRegistered($student, $semester){
        $form = RoomRegistrationForm::where('semester_id',$semester->id)->where('student_id',$student->id)
        ->First();
        if (isset($form)){
            return true;
        }else{
            return false;
        }
    }

    public function getContractsByStudent($student, $semester){    
        $contracts = RoomRegistrationForm::select(
            'room_registration_forms.*',         
            'students.name',
            'students.code as student_code',            
            'rooms.id as room_id',
            'rooms.code as room_code',
            'rooms.type as room_type',
            'semesters.name as semester_name'
            )
        ->join('students', 'students.id', '=', 'room_registration_forms.student_id')
        ->join('rooms', 'rooms.id', '=', 'room_registration_forms.room_id') 
        ->join('semesters', 'semesters.id', '=', 'room_registration_forms.semester_id') 
        ->where('room_registration_forms.status',1)   
        ->where('room_registration_forms.student_id',$student->id) 
        ->where('room_registration_forms.semester_id',$semester->id) 
        ->where('room_registration_forms.del_flg','not like',1)   
        ->paginate(20);      
        return $contracts;
    } 

    public function getContractsBySemester($semester){    
        $contracts = RoomRegistrationForm::select(
            'room_registration_forms.*',         
            'students.name',
            'students.id as student_id',
            'students.image',
            'students.gender',
            'students.status as student_status',
            'students.code as student_code',            
            'rooms.id as room_id',
            'rooms.code as room_code',
            'rooms.type as room_type',
            'semesters.name as semester_name'
            )
        ->join('students', 'students.id', '=', 'room_registration_forms.student_id')
        ->join('rooms', 'rooms.id', '=', 'room_registration_forms.room_id') 
        ->join('semesters', 'semesters.id', '=', 'room_registration_forms.semester_id') 
        ->where('room_registration_forms.status',1)           
        ->where('room_registration_forms.semester_id',$semester->id) 
        ->where('room_registration_forms.del_flg','not like',1)   
        ->get();      
        return $contracts;
    } 

}