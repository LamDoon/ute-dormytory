<?php

namespace App\Http\Service;

use App\Models\Room;
use App\Models\RoomRegistrationForm;
use App\Models\Semester;
use Illuminate\Support\Facades\Session;
use League\Flysystem\Exception;

class RoomService
{
    public function getRoom($region, $floor){
        return Room::where('region',$region)->where('floors',$floor)->orderBy('code','ASC')->get();
    }

    public function update($request, $room){
        try{
            $room->update([
                'message'=>$request->input('message'),
                'message_title'=>$request->input('message_title'),                
            ]);            
            Session::flash('success','Cập nhật thông tin ghi chú thành công.');
        }catch(Exception $err){
            Session::flash('error',$err->getMessage());
            return false;
        }
        return true;
    }

    public function getSemester(){
        return Semester::where('status', 1)->First(); 
    }

    public function getDetailStudent($roomId){
        $room_id = $roomId->id;
        $semester = $this->getSemester();
        $semesterId = $semester->id;
        $contract = RoomRegistrationForm::select(
            'room_registration_forms.id',  
            'room_registration_forms.paid',           
            'students.name',
            'students.code',
            'students.email',
            'students.id as student_id',        
            'students.address',        
            'students.card_id',        
            'students.gender',
            'students.class',
            'students.depart',
            'students.course',
            'students.national',
            'students.ethnic',
            'students.phone',
           )
        ->join('students', 'students.id', '=', 'room_registration_forms.student_id')
        ->join('rooms', 'rooms.id', '=', 'room_registration_forms.room_id')         
        ->where('room_registration_forms.status',1)
        ->where('room_registration_forms.semester_id',$semesterId)
        ->where('rooms.id', $room_id)        
        ->get();      
        return $contract;
    }

    public function reset(){               
        try{
            Room::query()->update([
                'regist_num' => 0,
                'message' => "",
                'message_title'=> ""
            ]);                       
            Session::flash('success','Reset các phòng thành công.');
        }catch(Exception $err){
            Session::flash('error',$err->getMessage());
            return false;
        }
        return true;
    }
}