<?php

namespace App\Http\Service;

use App\Models\Notification;
use App\Models\Semester;
use Exception;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class SemesterService {


    public function getAll(){
        return Semester::orderByDESC('end_date')->get();        
    }

    public function create($request){        
        $request->validate([
            'name'=>'required',
            'start_date'=>'required',   
            'end_date'=>'required',         
        ]);

        $semester = new Semester();

        try {

            if(isset($request->status)){
                $semester->status = 1;
                $s = Semester::where('status',1)->First();
                $s->update([
                    $s->status= 0,
                ]);
            }

            $semester->name = $request->name;
            $semester->start_date = $request->start_date;  
            $semester->end_date = $request->end_date;  

            $semester->save();
            Session::flash('success','Học kỳ đã được thêm thành công.');
        }catch(Exception $err){
            Session::flash('error',$err->getMessage());
            return false;
        }
        return true;
    }

    public function getCurrentSemester(){
        return Semester::where('status',1)->First();
    }

    public function createVehicle($semester, $request){        
        $semester->veh_max = $request->veh_max;        
        $semester->save();
        Session::flash('success','Thao tác thành công!');    
    }

    public function updateVehicle($semester){
        $semester->veh_num = $semester->veh_num +1;
        $semester->save();
       // Session::flash('success','Thao tác thành công!'); 
    }
}