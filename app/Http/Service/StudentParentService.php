<?php

namespace App\Http\Service;

use App\Models\Intro;
use App\Models\StudentParent;
use Exception;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class StudentParentService {


    public function getAll(){       
        return StudentParent::where('del_flg','not like',1)->orderByDesc('id')->paginate(20);       
    }

    public function create($request){        
        $request->validate([
            'name'=>'required',
            'phone'=>'required|numeric|min:10',
            'address' => 'required'
        ]);

        $parent = new StudentParent();

        try {

            $parent->name = $request->name;
            $parent->phone = $request->phone; 
            $parent->address = $request->address;      

            $parent->save();
            Session::flash('success','Một thông tin thân nhân đã được thêm thành công.');

        }catch(Exception $err){
            Session::flash('error',$err->getMessage());
            return false;
        }
        return true;
    }

    public function getParentByStudent($student){
        return StudentParent::where('id',$student)->First();
    }
    

}
