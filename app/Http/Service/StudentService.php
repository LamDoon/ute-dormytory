<?php

namespace App\Http\Service;

use App\Models\Student;
use Exception;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;
use App\Mail\SignupEmail;
use App\Models\Department;
use Illuminate\Support\Facades\Auth;

class StudentService{

    public function getAll($request){
         if($request->filter == 'code'){
             return Student::where('code','like','%'.$request->key_search.'%')->where('del_flg','not like',1)->orderByDesc('id')->paginate(20);
         }
         elseif($request->filter == 'name'){
             return Student::where('name','like','%'.$request->key_search.'%')->where('del_flg','not like',1)->orderByDesc('id')->paginate(20);
         }
         elseif($request->filter == 'course'){
             return Student::where('course','like','%'.$request->key_search.'%')->where('del_flg','not like',1)->where('del_flg','not like',1)->orderByDesc('id')->paginate(20);
         }
        return Student::where('name','like','%'.$request->key_search.'%')->where('del_flg','not like',1)->orderByDesc('id')->paginate(20);
    }

    public function getDepart(){
        return Department::all();
    }

    public function create($request){           
        $request->validate([
            'name'=>'required',
            'email'=>'required|email',
            'card_id'=>'required|numeric|min:13',
            'gender'=>'required|integer',
            'phone'=>'required|min:10|numeric',
            'code'=>'required|numeric|unique:students',
            'class'=>'required',
            'depart'=>'required',
            'course'=>'required',
            'dob'=>'required',
            'national'=>'required',
            'relat_id'=>'required',
            'ethnic'=>'required',
            'address'=>'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);

        $student = new Student();
        try{            
            $student->name = $request->input('name');
            $student->email = $request->input('email');
            $student->card_id = $request->input('card_id');
            $student->gender = $request->input('gender');
            $student->phone = $request->input('phone');
            $student->code = $request->input('code');
            $student->class = $request->input('class');
            $student->depart = $request->input('depart');
            $student->course = $request->input('course');
            $student->dob = $request->input('dob');
            $student->national = $request->input('national');
            $student->ethnic = $request->input('ethnic');
            $student->address = $request->input('address');
            $student->relat_id = $request->input('relat_id');
            $student->password = Hash::make('Abc123*');
              
            if ($request->hasFile('image')){
                $file = $request->file('image');
                $extension = $file->getClientOriginalExtension();
                $newImageName = time().'.'.$request->name.'.'.$extension;
                $file->move(public_path('images/students'),$newImageName);
                $student->image = $newImageName;
            }
            $student->save();         
            $this->sendMail($request->input('email'), $request->input('name'), $request->input('code'));
            Session::flash('success','Tài khoản sinh viên đã được tạo thành công.');
        }catch(Exception $err){
            Session::flash('error',$err->getMessage());
            return false;
        }
        return true;

    }

    public function sendMail($email, $name, $code){
        $data = [
            'name'=>$name,
            'code'=>$code,
        ];
        Mail::to($email)->send(new SignupEmail($data));    
    }  
    
    public function getDisable(){
        return Student::where('status',0)->orderByDesc('id')->paginate(20);
    }    

    public function active($student){
        if($student->status == 1)
        {
            $student->status = 0;
        }else{
            $student->status = 1;
        }        
        $student->save();
        Session::flash('success','Thao tác thành công!');
    }

    public function postVehicle($student, $request){        
        $student->vehicle = $request->vehicle;           
        $student->save();
        Session::flash('success','Thêm đăng ký xe thành công!');
    }

    public function postLogin($request){      
        $request->validate([
            'code'=>'required|numeric',
            'password'=>'required'
        ]);
        $s_pass = $request->input('password');        
        $s_code = $request->input('code');
        $s_data = $this->selectStudent($s_code);
      
        if(isset($s_data)){
            $student_pass = $s_data->password;
            if(password_verify($s_pass,$student_pass)){
                $request->session()->put('student',$s_data->id);
                $request->session()->put('name',$s_data->name);                
                redirect()->back();
            }else{
                Session::flash('error','Mật khẩu sai. Vui lòng nhập lại');
                redirect()->back();
            }
        }else{
            Session::flash('error','Mã sinh viên không tồn tại.');
            redirect()->back();
        }
       
    }

    public function selectStudent($code){
        return Student::where('code',$code)->where('status',1)->where('del_flg','not like',1)->First();         
    }

    public function getById($id){
        return Student::where('id',$id)->where('status',1)
        ->where('del_flg','not like',1)->First();  
    }

    public function changePass($student, $request){        
        $request->validate([
            'password'=>'required']);  
        try{
            $student->password = Hash::make($request->password);
            $student->save();
            Session::flash('success','Cập nhật mật khẩu thành công!');
            
        }catch(Exception $err){
            Session::flash('error',$err->getMessage());
            return false;
        }
        return true;
        
    }

    public function getAllStudent(){
        return Student::where('del_flg','not like',1)->get();         
    }

}