<?php

namespace App\Http\Service;

use App\Models\Position;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Session;

class UserService
{
    public function getAll(){
        return User::where('status',1)
        ->where('del_flg','not like',1)->orderByDesc('id')->paginate(20);
    }

    public function getPosition(){
        return Position::all();
    }

    public function create($request){        
        $request->validate([
            'name'=>'required',
            'email'=>'required|email',
            'card_id'=>'required|numeric',
            'gender'=>'required|integer',
            'phone'=>'required|min:10|numeric',
            'position_id'=>'required',
            'address'=>'required',
        ]);

        try{
            User::create([
                'name'=>$request->input('name'),
                'email'=>$request->input('email'),
                'card_id'=>$request->input('card_id'),
                'gender'=>$request->input('gender'),
                'phone'=>$request->input('phone'),
                'position_id'=>$request->input('position_id'),
                'address'=>$request->input('address'),
                'password'=>Hash::make('Abc123*'),
            ]);
            Session::flash('success','Một nhân viên đã được thêm thành công.');
        }catch(Exception $err){
            Session::flash('error',$err->getMessage());
            return false;
        }
        return true;
    }

    public function active($user){
        if($user->status == 1)
        {
            $user->status = 0;
        }else{
            $user->status = 1;
        }        
        $user->save();
        Session::flash('success','Thao tác thành công!');
    }

    public function getDisable(){
        return User::where('status',0)->orderByDesc('id')->paginate(20);
    }

    public function updateInfo($request, $user){
        $request->validate([
            'name'=>'required',
            'email'=>'required|email',
            'card_id'=>'required|numeric',
            'gender'=>'required|integer',
            'phone'=>'required|min:10|numeric',            
            'address'=>'required',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);

        try{
            $user->update([
                'name'=>$request->input('name'),
                'email'=>$request->input('email'),
                'card_id'=>$request->input('card_id'),
                'gender'=>$request->input('gender'),
                'phone'=>$request->input('phone'),               
                'address'=>$request->input('address'),
                'password'=>Hash::make($request->input('password')),
            ]);

            if ($request->hasFile('image')){
                $file = $request->file('image');
                $extension = $file->getClientOriginalExtension();
                $newImageName = time().'.'.$request->name.'.'.$extension;
                $file->move(public_path('images'),$newImageName);
                $user->image = $newImageName;
            }
            $user->save();

            Session::flash('success','Cập nhật thông tin cá nhân thành công.');
        }catch(Exception $err){
            Session::flash('error',$err->getMessage());
            return false;
        }
        return true;

    }

    public function updateAtm($request, $user){
        $request->validate([            
            'account_number'=>'nullable|numeric',
            'bank_name'=>'nullable',            
        ]);

        try{
            $user->update([
                'account_number'=>$request->input('account_number'),
                'bank_name'=>$request->input('bank_name'),                
            ]);
            Session::flash('success','Cập nhật thông tin tài khoản ngân hàng thành công.');
        }catch(Exception $err){
            Session::flash('error',$err->getMessage());
            return false;
        }
        return true;
    }
}