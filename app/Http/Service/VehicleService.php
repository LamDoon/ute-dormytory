<?php

namespace App\Http\Service;

use App\Models\Notification;
use App\Models\Vehicle;
use Exception;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Mail\VehicleEmail;
use Illuminate\Support\Facades\Session;

class VehicleService {


    public function getAll(){       
        return Vehicle::where('del_flg','not like',1)->orderByDesc('id')->paginate(20);       
    }

    public function getForm(){
        $forms = Vehicle::select(
            'vehicles.*',                
            'students.name', 
            'students.vehicle',           
            'students.code as student_code',            
            'semesters.name as semester_name',            
            )
        ->join('students', 'students.id', '=', 'vehicles.student_id')
        ->join('semesters', 'semesters.id', '=', 'vehicles.semester_id')             
        ->where('vehicles.del_flg','not like',1)   
        ->paginate(20);
        return $forms;
    }

    public function checkRegistered($student, $semester){
        $form = Vehicle::where('semester_id',$semester->id)->where('student_id',$student->id)
        ->First();
        if (isset($form)){
            return true;
        }else{
            return false;
        }
    }

    public function create($student, $semester){        

        $form = new Vehicle();

        try {
            $form->student_id = $student->id;
            $form->veh_code = $student->vehicle;  
            $form->semester_id = $semester->id; 

            $form->save();
            $this->sendMail($student->email, $student->name, $student->vehicle, $semester->name );
            Session::flash('success','Bạn đã đăng ký vé gửi xe thành công.');
        }catch(Exception $err){
            Session::flash('error',$err->getMessage());
            return false;
        }
        return true;
    }

    public function sendMail($email, $name, $vehicle, $semester){
        $data = [
            'name'=>$name,
            'vehicle'=>$vehicle,
            'semester'=>$semester,
        ];
        Mail::to($email)->send(new VehicleEmail($data));    
    }  

    public function getBySemester($semester){       
        return Vehicle::where('del_flg','not like',1)
        ->where('semester_id',$semester)->get();       
    }
    
}