<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VehicleEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->email_data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_USERNAME'),'Ban quản lý KTX Đại học Sư Phạm Kỹ Thuật Đà Nẵng')
            ->subject("Thông báo đăng ký thành công vé gửi xe tại ký túc xá")
            ->view('admin.vehicles.email',['email_data'=>$this->email_data]);
    }
}
