<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Commend extends Model
{
    use HasFactory;

    protected $fillable=[
        'student_id',
        'reason',
        'pattern',
        'created_date',
        'del_flg'
    ];    
}
