<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ElectricityBill extends Model
{
    use HasFactory;

    protected $fillable=[
        'index_id',
        'room_id',
        'old_e',
        'new_e',
        'old_w',
        'new_w',
        'total',
        'employee_id',
        'semester_id',
        'paid',
        'paid_date',
        'del_flg'
    ];
    
}
