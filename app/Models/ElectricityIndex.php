<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ElectricityIndex extends Model
{
    use HasFactory;

    protected $fillable=[        
        'e_price',
        'w_price',
        'status',
        'del_flg'
    ];
}
