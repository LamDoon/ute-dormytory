<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    use HasFactory;

    protected $fillable = [
        'code',
        'regist_num',
        'regist_max',
        'region',
        'floors',
        'message_title',
        'message',
    ];
}
