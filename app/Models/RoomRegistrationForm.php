<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class RoomRegistrationForm extends Model
{
    use HasFactory;

    protected $table = "room_registration_forms";

    protected $fillable = [
        'student_id',
        'employee_id',
        'room_id',
        'semester_id',
        'created_date',
        'start_date',
        'end_date',
        'total',
        'paid',
        'paid_date',
        'status',
        'del_flg',
    ];

    public static function getContract($semester){       

        $records = RoomRegistrationForm::select(
            'room_registration_forms.id',  
            'semesters.name as semester_name',       
            'students.name',
            'students.code',  
            'students.course',  
            'rooms.code as room_code',
            'room_registration_forms.created_date',
            'room_registration_forms.start_date',
            'room_registration_forms.end_date',
            )
        ->join('students', 'students.id', '=', 'room_registration_forms.student_id')
        ->join('rooms', 'rooms.id', '=', 'room_registration_forms.room_id') 
        ->join('semesters', 'semesters.id', '=', 'room_registration_forms.semester_id') 
        ->where('room_registration_forms.status',1)           
        ->where('room_registration_forms.semester_id',$semester->id) 
        ->where('room_registration_forms.del_flg','not like',1)   
        ->get()->toArray();     

        return $records;
    }
}
