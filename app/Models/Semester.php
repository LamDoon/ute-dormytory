<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Semester extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',        
        'start_date',
        'end_date', 
        'price',
        'veh_max',
        'veh_num',
        'veh_price',        
        'status'       
    ];
}
