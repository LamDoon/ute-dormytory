<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Student extends Authenticatable implements MustVerifyEmail
{
    use HasFactory;
    use Notifiable;

    protected $fillable = [
        'name',
        'email',
        'image',
        'dob',
        'code',
        'address',
        'account_number',
        'bank_name',
        'national',
        'ethnic',
        'card_id',
        'course',
        'class',
        'gender',
        'phone',
        'password',
        'vehicle',
        'relat_id',
        'depart',
        'position_id',
        'status',
        'del_flg'
    ];


    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
