<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();            
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');            
            $table->string('image',255)->default('usericon.png');           
            $table->string('address',255);
            $table->string('account_number',255)->nullable();
            $table->string('bank_name',255)->nullable();            
            $table->string('card_id',50);
            $table->boolean('gender');
            $table->string('phone');            
            $table->integer('position_id');
            $table->integer('status')->default(1);
            $table->integer('del_flg')->nullable();
            $table->rememberToken();          
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
