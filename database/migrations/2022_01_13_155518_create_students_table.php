<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('image',255)->default('usericon.png');  
            $table->date('dob');
            $table->string('code')->unique();         
            $table->string('address',255);
            $table->string('account_number',255)->nullable();
            $table->string('bank_name',255)->nullable();
            $table->string('national',100);
            $table->string('ethnic',50);
            $table->string('card_id',50);
            $table->string('course',50);
            $table->string('class',50);
            $table->integer('gender');
            $table->string('phone');
            $table->string('password');
            $table->string('vehicle')->nullable();
            $table->integer('relat_id')->nullable();
            $table->string('depart',255);
            $table->integer('position_id')->default(0);
            $table->integer('status')->default(1);
            $table->integer('del_flg')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
