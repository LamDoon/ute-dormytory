<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSemestersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('semesters', function (Blueprint $table) {
            $table->id();
            $table->string('name',20);
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable(); 
            $table->integer('veh_max')->nullable();
            $table->integer('veh_num')->nullable();
            $table->decimal('veh_price',10,3)->nullable()->default(2.000);
            $table->tinyInteger('status',3)->default(0);
            $table->decimal('price',10,3)->nullable()->default(100.000);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('semesters');
    }
}
