<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoomRegistrationForms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room_registration_forms', function (Blueprint $table) {
            $table->id();
            $table->integer('student_id');
            $table->integer('employee_id')->nullable();
            $table->integer('room_id');
            $table->integer('semester_id');
            $table->dateTime('created_date')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();                  
            $table->decimal('total',10,3)->nullable();
            $table->integer('paid')->default(0)->nullable();
            $table->date('paid_date')->nullable();
            $table->integer('status')->default(0)->nullable();
            $table->integer('del_flg')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('room_registration_forms');
    }
}
