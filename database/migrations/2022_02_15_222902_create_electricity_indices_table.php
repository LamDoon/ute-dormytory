<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateElectricityIndicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('electricity_indices', function (Blueprint $table) {
            $table->id();                    
            $table->decimal('e_price',10,3)->nullable();
            $table->decimal('w_price',10,3)->nullable();            
            $table->integer('status')->default(0)->nullable();
            $table->integer('del_flg')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('electricity_indices');
    }
}
