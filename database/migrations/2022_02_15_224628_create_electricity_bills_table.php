<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateElectricityBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('electricity_bills', function (Blueprint $table) {
            $table->id();
            $table->integer('index_id');
            $table->integer('room_id');
            $table->integer('employee_id');
            $table->integer('semester_id')->nullable();   
            $table->decimal('old_e',10,2)->nullable();
            $table->decimal('new_e',10,2)->nullable();
            $table->decimal('old_w',10,2)->nullable();
            $table->decimal('new_w',10,2)->nullable();
            $table->decimal('total',10,2)->nullable();
            $table->integer('paid')->default(0)->nullable();
            $table->date('paid_date')->nullable();
            $table->integer('del_flg')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('electricity_bills');
    }
}
