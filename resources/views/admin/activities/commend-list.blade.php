@extends('admin.main')
@section('content')

        <div class="app-main__inner">
            <div class="app-page-title">
                    <div class="page-title-wrapper">
                        <div class="page-title-heading"> 
                            <div class="page-title-icon">
                                <i class="pe-7s-film icon-gradient bg-premium-dark">
                                </i>
                            </div>                                   
                            <div>{{ $title}}
                                <div class="page-title-subheading">Danh sách sinh viên khen thưởng.
                                </div>
                            </div>
                        </div>
                        <div class="page-title-actions">                                   
                            <div class="d-inline-block dropdown">
                                <a href="{{ url('/admin/activities/commend/create') }}"    class="mb-2 mr-2 btn btn-info">                                            
                                    Thêm khen thưởng
                                </a>                                       
                            </div>
                        </div>    
                    </div>
                </div>      
                <div class="social-container">
                    <span>@include('admin.alert')</span>
                </div>          
                    <div class="main-card mb-3 card">
                        @foreach($commends as $commend)
                            <div class="no-gutters row" id="i{{$commend->id}}">
                                <div class="col-md-6">
                                    <div class="widget-content">
                                        <div class="widget-content-wrapper">                                                
                                            <div class="widget-content-left">
                                                <div class="widget-heading">Mã sinh viên: {{ $commend->student_id }}</div>
                                                <div class="widget-subheading">Lý do khen thưởng: {{ $commend->reason }}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                                   
                                <div class="col-md-6">
                                    <div class="widget-content">
                                        <div class="widget-content-wrapper">                                                
                                            <div class="widget-content-left">
                                              Ngày khen thưởng: {{$commend->created_date}}
                                            </div>
                                            <div class="widget-content-right">
                                                <a href="javascript:void(0)" onclick="deleteCommend({{$commend->id}})"
                                                class="mb-2 mr-2 btn btn-danger"><i class="pe-7s-trash"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
        {{ $commends->links() }}   
                            
@endsection

<script>
    function deleteCommend(id){       
        if(confirm("Bạn có chắc chắn muốn xoá khen thưởng?")){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url:'/admin/activities/commend/delete/'+id,                
                type:'GET',
                 data:{
                     _token : $("input[name=_token]").val()
                 },
                success:function(response){
                  $('#i'+id).remove();                  
                }
            });
        }
    }
</script>