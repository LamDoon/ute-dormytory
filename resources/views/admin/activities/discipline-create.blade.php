@extends('admin.main')
@section('content')
@can('admin-position') 
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading"> 
                        <div class="page-title-icon">
                            <i class="pe-7s-albums icon-gradient bg-premium-dark">
                            </i>
                        </div>                                   
                        <div>{{ $title}}
                            <div class="page-title-subheading">.
                            </div>
                        </div>
                    </div>
                    <div class="page-title-actions">                                   
                        <div class="d-inline-block dropdown">
                            <a href="{{ url('/admin/activities/discipline') }}"    class="mb-2 mr-2 btn btn-info">                                            
                                Danh sách kỷ luật
                            </a>                                       
                        </div>
                    </div>    
                </div>
            </div>       
            <div class="social-container">
                <span>@include('admin.alert')</span>
            </div>         
            <div class="row">   
                <div class="col-md-12"> 
                    <div class="main-card mb-3 card">
                        <div class="card-body">
                            <h5 class="card-title">Thêm kỷ luật</h5>                                                          
                                <form class="needs-validation" method="post" enctype="multipart/form-data">  
                                <div class="position-relative form-group">
                                            <label for="exampleSelect" class="">Danh sách sinh viên</label>
                                            <select name="student_id" id="exampleSelect" class="form-control">
                                            @foreach($students as $s)                                                            
                                                <option value="{{$s->id}}">Họ tên sinh viên: {{ $s->name }} - MSSV: {{ $s->code }}</option>                                                   
                                            @endforeach 
                                            </select>
                                        </div>                                              
                                    <div class="position-relative form-group">
                                        <label for="exampleTitle" class="">Lý do khen thưởng</label>
                                        <input name="reason" id="exampleTitle" placeholder="Nhập lý do" type="text" class="form-control">
                                    </div>  
                                    <div class="position-relative form-group">
                                        <label for="exampleTitle" class="">Hình thức</label>
                                        <input name="pattern" id="exampleTitle" placeholder="Nhập hình thức" type="text" class="form-control">
                                    </div> 
                                    <div class="position-relative form-group">
                                        <label for="exampleTitle" class="">Ngày khen thưởng</label>
                                        <input name="created_date" id="exampleTitle"  type="date" class="form-control">
                                    </div>                                                        
                                                                                                                                        
                                    <button class="mt-1 btn btn-primary">Thêm kỷ luật</button>
                                    @csrf
                                </form>
                        </div>
                    </div>                            
                </div>   
            </div>                  
        </div>
@endcan                 
@endsection
