@extends('admin.main')
@section('content')

        <div class="app-main__inner">
            <div class="app-page-title">
                    <div class="page-title-wrapper">
                        <div class="page-title-heading"> 
                            <div class="page-title-icon">
                                <i class="pe-7s-film icon-gradient bg-premium-dark">
                                </i>
                            </div>                                   
                            <div>{{ $title}}
                                <div class="page-title-subheading">Danh sách sinh viên kỷ luật.
                                </div>
                            </div>
                        </div>
                        <div class="page-title-actions">                                   
                            <div class="d-inline-block dropdown">
                                <a href="{{ url('/admin/activities/discipline/create') }}"    class="mb-2 mr-2 btn btn-info">                                            
                                    Thêm kỷ luật
                                </a>                                       
                            </div>
                        </div>    
                    </div>
                </div>      
                <div class="social-container">
                    <span>@include('admin.alert')</span>
                </div>          
                    <div class="main-card mb-3 card">
                        @foreach($disciplines as $discipline)
                            <div class="no-gutters row" id="i{{$discipline->id}}">
                                <div class="col-md-6">
                                    <div class="widget-content">
                                        <div class="widget-content-wrapper">                                                
                                            <div class="widget-content-left">
                                                <div class="widget-heading">Mã sinh viên: {{ $discipline->student_id }}</div>
                                                <div class="widget-subheading">Lý do kỷ luật: {{ $discipline->reason }}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                                   
                                <div class="col-md-6">
                                    <div class="widget-content">
                                        <div class="widget-content-wrapper">                                                
                                            <div class="widget-content-left">
                                              Ngày kủ luật: {{$discipline->created_date}}
                                            </div>
                                            <div class="widget-content-right">
                                                <a href="javascript:void(0)" onclick="deleteDiscipline({{$discipline->id}})"
                                                class="mb-2 mr-2 btn btn-danger"><i class="pe-7s-trash"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
        {{ $disciplines->links() }}   
                            
@endsection

<script>
    function deleteDiscipline(id){       
        if(confirm("Bạn có chắc chắn muốn xoá kỷ luật?")){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url:'/admin/activities/discipline/delete/'+id,                
                type:'GET',
                 data:{
                     _token : $("input[name=_token]").val()
                 },
                success:function(response){
                  $('#i'+id).remove();                  
                }
            });
        }
    }
</script>