@extends('admin.main')
@section('content')

                <div class="app-main__inner">
                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    <div class="page-title-icon">
                                        <i class="pe-7s-note2 icon-gradient bg-premium-dark">
                                        </i>
                                    </div>
                                    <div>{{ $title}}
                                        <div class="page-title-subheading">Ban quản lý kiểm tra thông tin và xác nhận yêu cầu đăng ký phòng từ sinh viên.
                                        </div>
                                    </div>
                                </div>
                                <div class="page-title-actions">                                    
                                    <div class="d-inline-block dropdown">
                                        <a href="{{ url('/admin/contracts/list') }}"    class="mb-2 mr-2 btn btn-info">                                            
                                            Danh sách đơn nội trú
                                        </a>                                         
                                    </div>
                                </div>   
                             </div>
                        </div>    
                        <div class="social-container">
			                <span>@include('admin.alert')</span>
			            </div>                                
                        @if(count($contracts)>0) 
                            <div class="main-card mb-3 card">
                                 <div class="card-body">
                                <h5 class="card-title">Danh sách yêu cầu đăng ký nội trú chưa được phê duyệt</h5>
                                @foreach($contracts as $contract)
                                    <div class="no-gutters row">
                                        <div class="col-md-6">
                                            <div class="widget-content">
                                                <div class="widget-content-wrapper">                                                
                                                    <div class="widget-content-left">
                                                        <div class="widget-heading">Số phòng: {{ $contract->room_code }}</div>
                                                        <div class="widget-heading">Sinh viên đăng ký: {{ $contract->name }} ||  {{ $contract->student_code }}</div>
                                                        <div class="widget-subheading">Ngày đăng ký {{ $contract->created_date }}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>                                   
                                        <div class="col-md-6">
                                            <div class="widget-content">
                                                <div class="widget-content-wrapper">                                                
                                                    <div class="widget-content-left">                                                  
                                                        <a href="{{ url('admin/contracts/show/'.$contract->id) }}" class="badge badge-danger">Chưa phê duyệt</a>
                                                    </div>
                                                    <div class="widget-content-right"> 
                                                    <a href="{{ url('admin/contracts/approve/'.$contract->id) }}" class="mb-2 mr-2 btn btn-info">Xem chi tiết</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                               
                                @endforeach
                            </div>
                            </div>
                        @else 
                            <div class="list-group-item-warning list-group-item">Không tìm thấy yêu cầu đăng ký cho phòng cần tìm.</div>
                        @endif
                           
                            {{ $contracts->links() }}    
                    
                            
@endsection