<h3>Xin chào sinh viên {{ $email_data['name'] }}</h3>

Yêu cầu đăng ký phòng nội trú tại Ký túc xá Đại học Sư Phạm Kỹ Thuật Đà Nẵng của bạn đã bị từ chối
<br><br>
Lý do: 
<br>
<h4> {{ $email_data['reason'] }}</h4>
<p>Vui lòng kiểm tra và thực hiện đăng ký khác.</p>
Mọi thắc mắc vui lòng liên hệ BQL Ký túc xá Đại học Sư Phạm Kỹ Thuật Đà Nẵng.
<br>
<p>------------------------------------------------------</p>

Mr. Le Van Anh</br>

University of Technology and Education dormitory</br>

University of Technology and Education</br>

The University of Danang</br>

Address: 48 Cao Thang, Da Nang, Viet Nam</br>

Tel.: (+84) 789 451 245, (+84) 904 512 032</br>

E-mail: UTEdomytory@ute.udn.vn, UTEdomytory@gmail.com 