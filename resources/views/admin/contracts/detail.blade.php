@extends('admin.main')
@section('content')
<div class="app-main__inner">
<div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    <div class="page-title-icon">
                                        <i class="pe-7s-note2 icon-gradient bg-premium-dark">
                                        </i>
                                    </div>
                                    <div>{{ $title}}
                                        <div class="page-title-subheading">Ban quản lý kiểm tra thông tin và xác nhận yêu cầu đăng ký phòng từ sinh viên.
                                        </div>
                                    </div>
                                </div>
                                <div class="page-title-actions">                                    
                                    <div class="d-inline-block dropdown">
                                        <a href="{{ url('/admin/contracts/list') }}"    class="mb-2 mr-2 btn btn-info">                                            
                                            Danh sách đơn nội trú
                                        </a>                                      
                                    </div>
                                </div>   
                             </div>
                        </div>                                              
                        
                        <div class="tab-content">
                            <div class="tab-pane tabs-animation fade show active" id="tab-content-0" role="tabpanel">
                                <div class="main-card mb-3 card">
                                    <div class="card-body">
                                        <h5 class="card-title">Thông tin sinh viên</h5>
                                        <form class="" method="post">
                                            <div class="form-row">
                                                <div class="col-md-4">
                                                    <img src="/images/students/{{ $contract->image }}" class="rounded-circle" width="150"/></br></br>                                             
                                                </div>   
                                                <div class="col-md-4">
                                                    <div class="position-relative form-group">
                                                        <label for="code" class="">Mã số sinh viên</label>
                                                        <input id="code" placeholder="with a placeholder" type="text" class="form-control" value="{{ $contract->student_code }}" readonly>
                                                    </div>
                                                </div>
                                                @if ($contract->gender == 0)
                                                <div class="col-md-2">
                                                    <div class="position-relative form-group">
                                                        <label for="gender" class="">Giới tính</label>
                                                        <input id="gender" placeholder="" type="text" class="form-control" value="Nữ" readonly>
                                                    </div>
                                                </div>
                                                @else
                                                <div class="col-md-2">
                                                    <div class="position-relative form-group">
                                                        <label for="gender" class="">Giới tính</label>
                                                        <input id="gender" placeholder="" type="text" class="form-control" value="Nam" readonly>
                                                    </div>
                                                </div>
                                                @endif
                                                <div class="col-md-2">
                                                    <div class="position-relative form-group">
                                                        <label for="course" class="">Khoá học</label>
                                                        <input id="course" placeholder="" type="text" class="form-control" value="{{ $contract->course }}" readonly>
                                                    </div>
                                                </div>                                                
                                            </div>
                                            <div class="form-row">
                                                <div class="col-md-6">
                                                    <div class="position-relative form-group">
                                                        <label for="name" class="">Họ và Tên</label>
                                                        <input id="name" placeholder="with a placeholder" type="text" class="form-control" value="{{ $contract->name }}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="position-relative form-group">
                                                        <label for="depart" class="">Khoa</label>
                                                        <input id="depart" placeholder="" type="text" class="form-control" value="{{ $contract->depart }}" readonly>
                                                    </div>
                                                </div>                                                
                                                <div class="col-md-2">
                                                    <div class="position-relative form-group">
                                                        <label for="class" class="">Lớp</label>
                                                        <input id="class" placeholder="" type="text" class="form-control" value="{{ $contract->class }}" readonly>
                                                    </div>
                                                </div>                                               
                                            </div>
                                            <div class="form-row">
                                                <div class="col-md-4">
                                                    <div class="position-relative form-group">
                                                        <label for="email" class="">Email</label>
                                                        <input id="email" placeholder="with a placeholder" type="text" class="form-control" value="{{ $contract->email }}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="position-relative form-group">
                                                        <label for="card" class="">CMND/ CCCD</label>
                                                        <input id="card" placeholder="" type="text" class="form-control" value="{{ $contract->card_id }}" readonly>
                                                    </div>
                                                </div>                                                
                                                <div class="col-md-4">
                                                    <div class="position-relative form-group">
                                                        <label for="phone" class="">Số điện thoại</label>
                                                        <input id="phone" placeholder="" type="text" class="form-control" value="{{ $contract->phone }}" readonly>
                                                    </div>
                                                </div>                                               
                                            </div>
                                        
                                            <h5 class="card-title">Thông tin phòng đăng ký</h5>
                                        
                                            <div class="form-row">
                                                <div class="col-md-3">
                                                    <div class="position-relative form-group">
                                                        <label for="roomId" class="">Mã phòng</label>
                                                        <input id="roomId" placeholder="with a placeholder" type="text" class="form-control" value="{{ $contract->room_id }}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="position-relative form-group">
                                                        <label for="roomCode" class="">Số phòng</label>
                                                        <input id="roomCode" placeholder="" type="text" class="form-control" value="{{ $contract->room_code }}" readonly>
                                                    </div>
                                                </div>                                                
                                                <div class="col-md-3">
                                                    <div class="position-relative form-group">
                                                        <label for="max" class="">Số lượng đăng ký cho phép</label>
                                                        <input id="max" placeholder="" type="text" class="form-control" value="{{ $contract->regist_max }}" readonly>
                                                    </div>
                                                </div> 
                                                <div class="col-md-3">
                                                    <div class="position-relative form-group">
                                                        <label for="num" class="">Số lượng đã đăng ký</label>
                                                        <input id="num" placeholder="" type="text" class="form-control" value="{{ $contract->regist_num }}" readonly>
                                                    </div>
                                                </div>                                                
                                            </div>
                                        
                                            <h5 class="card-title">Học kỳ đăg ký {{ $semester->name}}</h5>
                                       
                                            <div class="form-row">
                                                <div class="col-md-3">
                                                    <div class="position-relative form-group">
                                                        <label for="start" class="">Thời gian bắt đầu</label>
                                                        <input id="start" placeholder="with a placeholder" type="text" class="form-control" value="{{ \Carbon\Carbon::parse($semester->start_date)->format('d/m/Y')}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="position-relative form-group">
                                                        <label for="end" class="">Thời gian kết thúc</label>
                                                        <input id="end" placeholder="" type="text" class="form-control" value="{{ \Carbon\Carbon::parse($semester->end_date)->format('d/m/Y')}}" readonly>
                                                    </div>
                                                </div>                                                                  
                                            </div>
                                                @if($contract->status == 0)
                                                    @if($contract->regist_num < $contract->regist_max)
                                                    <button class="mb-2 mr-2 btn btn-primary">Phê duyệt</button>  
                                                    @endif
                                                @endif
                                                    @csrf
                                        </form>
                                        @if($contract->status == 0)
                                        <form method="post" action="cancel/{{$contract->id}}">
                                            <div class="form-row">                                         
                                            
                                                <div class="col-md-6">
                                                    <button class="mb-2 mr-2 btn btn-danger">Từ chối</button> 
                                                    <label  class="" style="color: red">Quản lý nêu rõ lý do nếu từ chối yêu cầu đăng ký phòng.</label> 
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="position-relative form-group">
                                                        <label for="end" class="">Lý do</label>
                                                        <textarea name="reason" id="end" placeholder="" type="text" class="form-control" >Phòng bạn chọn đăng ký đã hết giường trống</textarea>
                                                    </div>  
                                                </div>                                           
                                            </div>
                                            @csrf
                                        </form>   
                                        @endif                                                                                   
                                    </div>
                                </div>
                            </div>
                        </div>
</div>
@endsection