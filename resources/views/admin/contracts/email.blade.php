<h3>Xin chào sinh viên {{ $email_data['name'] }}</h3>

Yêu cầu đăng ký phòng nội trú tại Ký túc xá Đại học Sư Phạm Kỹ Thuật Đà Nẵng của bạn đã được phê duyệt
<br><br>
Hãy đăng nhập hệ thống để xem cập nhật và thông báo từ BQL: 
<br>
<h4>Số phòng: {{ $email_data['room'] }}</h4>
<h4>Kì nội trú bắt đầu từ {{ $email_data['start'] }} - {{ $email_data['end'] }}</h4>
<p>Sinh viên hoàn thành lệ phí <span style="color: red;"> {{number_format($email_data['total'], 3, ",", ".").' VND' }} </span>tại phòng Kế toán - tài vụ 
    hoặc chuyển khoản qua </p></br>
    STK: 01201204784105 </br>
    Chủ TK:BQL KY TUC XA UTE DHDN </br>
    Ngân hàng: ViettinBank </br>
    Nội dung: <span style="color: red;">SoPhong_MSSV_HocKy</span>
Mọi thắc mắc vui lòng liên hệ BQL Ký túc xá Đại học Sư Phạm Kỹ Thuật Đà Nẵng.
<br>
<p>------------------------------------------------------</p>

Mr. Le Van Anh</br>

University of Technology and Education dormitory</br>

University of Technology and Education</br>

The University of Danang</br>

Address: 48 Cao Thang, Da Nang, Viet Nam</br>

Tel.: (+84) 789 451 245, (+84) 904 512 032</br>

E-mail: UTEdomytory@ute.udn.vn, UTEdomytory@gmail.com 
