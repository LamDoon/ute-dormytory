@extends('admin.main')
@section('content')

<div class="app-main__inner">                       
                            <div class="app-page-title">
                                <form method="get">
                                    <div class="page-title-wrapper">
                                        <div class="app-header__content">                                    
                                            <div class="app-header-left">
                                                <div class="search-wrapper active">
                                                    <div class="input-holder">
                                                        <input type="text" class="search-input" placeholder="Nhập mã số sinh viên" name="key_search">
                                                        <button class="search-icon"><span></span></button>
                                                    </div>
                                                    <button type="button" class="close"></button>
                                                </div>                                         
                                            </div>
                                        </div>
                                        
                                        <div class="page-title-actions">                                       
                                            <div class="mb-2 mr-2 btn-group">
                                                <button class="btn btn-focus"> <i class="pe-7s-filter"></i></button>
                                                <select name="semester_id" id="semester" class="dropdown-toggle-split dropdown-toggle btn btn-focus">
                                                        @foreach($semester as $s)
                                                        <option value="{{ $s->id }}">Học kỳ {{ $s->name }}</option>
                                                        @endforeach
                                                </select>                                      
                                            </div>                                                                           
                                        </div>   
                                    </div>
                                </form>
                            </div>  
                        
                        <div class="social-container">
			                <span>@include('admin.alert')</span>
			            </div>           
                        <ul class="body-tabs body-tabs-layout tabs-animated body-tabs-animated nav">
                            <li class="nav-item">
                                <a role="tab" class="nav-link active" id="tab-0" data-toggle="tab" href="#tab-content-0">
                                    <span>Danh sách sinh viên chưa nộp phí</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a role="tab" class="nav-link" id="tab-1" data-toggle="tab" href="#tab-content-1">
                                    <span>Danh sách đã nộp phí</span>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane tabs-animation fade show active" id="tab-content-0" role="tabpanel">
                                <div class="main-card mb-3 card">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="mb-0 table">
                                                <thead>
                                                    <tr>                                                   
                                                        <th>MSSV</th>
                                                        <th>Hình ảnh</th>
                                                        <th>Họ tên</th>                                                    
                                                        <th>Phòng</th>
                                                        <th>Học kỳ</th>
                                                        <th>Ngày đăng ký</th>
                                                        <th>Lệ phí</th>
                                                        <th>Tình trạng</th>
                                                        <th>Cập nhật</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($contracts_unpaid as $contract)
                                                    <tr>
                                                        <th scope="row">{{ $contract->student_code }}</th>
                                                        <td><img src="/images/students/{{ $contract->image }}" class="rounded-circle" width="40"/></td>
                                                        <td>{{ $contract->name }}</td>                                                    
                                                        <td>{{ $contract->room_code }}</td>
                                                        @foreach($semester as $s)
                                                            @if ($contract->semester_id == $s->id)
                                                                <td>{{ $s->name }}</td>                                                        
                                                            @endif                                                       
                                                        @endforeach 
                                                        <td>{{ $contract->created_date }}</td>  
                                                        <td>{{number_format($contract->total, 3, ",", ".")}} VND</td>                                                    
                                                        <td>
                                                            <div class="mb-2 mr-2 badge badge-pill badge-warning">Chưa nộp</div>
                                                        </td>
                                                        <td>
                                                            <a href="{{ url('admin/contracts/fee/update/'.$contract->id) }}" class="mb-2 mr-2 btn btn-info" style="color:white;">Đã nộp
                                                            </a>
                                                        </td>
                                                        <td>
                                                            <div class="page-title-actions">                                    
                                                                <div class="d-inline-block dropdown">
                                                                    <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="mb-2 mr-2 border-0 btn-transition btn btn-outline-dark">                                            
                                                                                <i class="fa fa-ellipsis-v fa-w-6"></i>
                                                                        </button>
                                                                    <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                                                                        <ul class="nav flex-column">
                                                                            <li class="nav-item">
                                                                                <a href="{{ url('admin/contracts/approve/'.$contract->id) }}" class="nav-link">
                                                                                    <i class="nav-link-icon lnr-inbox"></i>
                                                                                    <span>
                                                                                        Xem chi tiết
                                                                                    </span>                                                       
                                                                                </a>
                                                                            </li>
                                                                            <li class="nav-item">
                                                                                <a href="{{ url('admin/rooms/detail/'.$contract->room_id) }}" class="nav-link">
                                                                                    <i class="nav-link-icon lnr-book"></i>
                                                                                    <span>
                                                                                        Xem thành viên phòng
                                                                                    </span>                                                       
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>   
                                                        </td>
                                                    </tr> 
                                                @endforeach                                               
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                              
                            </div>
                            <div class="tab-pane tabs-animation fade" id="tab-content-1" role="tabpanel">
                                <div class="main-card mb-3 card">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="mb-0 table">
                                                <thead>
                                                    <tr>                                                   
                                                        <th>MSSV</th>
                                                        <th>Hình ảnh</th>
                                                        <th>Họ tên</th>                                                    
                                                        <th>Phòng</th>
                                                        <th>Học kỳ</th>
                                                        <th>Ngày nộp</th>
                                                        <th>Tình trạng</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($contracts_paid as $contract)
                                                    <tr>
                                                        <th scope="row">{{ $contract->student_code }}</th>
                                                        <td><img src="/images/students/{{ $contract->image }}" class="rounded-circle" width="40"/></td>
                                                        <td>{{ $contract->name }}</td>                                                    
                                                        <td>{{ $contract->room_code }}</td>
                                                        @foreach($semester as $s)
                                                            @if ($contract->semester_id == $s->id)
                                                                <td>{{ $s->name }}</td>                                                        
                                                            @endif                                                       
                                                        @endforeach 
                                                        <td>{{ $contract->paid_date }}</td>                                                    
                                                        <td>
                                                            <div class="mb-2 mr-2 badge badge-pill badge-success">Đã nộp</div>
                                                        </td>
                                                        <td>
                                                            <div class="page-title-actions">                                    
                                                                <div class="d-inline-block dropdown">
                                                                    <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="mb-2 mr-2 border-0 btn-transition btn btn-outline-dark">                                            
                                                                                <i class="fa fa-ellipsis-v fa-w-6"></i>
                                                                        </button>
                                                                    <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                                                                        <ul class="nav flex-column">
                                                                            <li class="nav-item">
                                                                                <a href="{{ url('admin/contracts/approve/'.$contract->id) }}" class="nav-link">
                                                                                    <i class="nav-link-icon lnr-inbox"></i>
                                                                                    <span>
                                                                                        Xem chi tiết
                                                                                    </span>                                                       
                                                                                </a>
                                                                            </li>
                                                                            <li class="nav-item">
                                                                                <a href="{{ url('admin/rooms/detail/'.$contract->room_id) }}" class="nav-link">
                                                                                    <i class="nav-link-icon lnr-book"></i>
                                                                                    <span>
                                                                                        Xem thành viên phòng
                                                                                    </span>                                                       
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>   
                                                        </td>
                                                    </tr> 
                                                @endforeach                                               
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

@endsection