@extends('admin.main')
@section('content')

                <div class="app-main__inner">
                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    <div class="page-title-icon">
                                        <i class="pe-7s-note2 icon-gradient bg-premium-dark">
                                        </i>
                                    </div>
                                    <div>{{ $title}}
                                        <div class="page-title-subheading">Wide selection of forms controls, using the Bootstrap 4 code base, but built with React.
                                        </div>
                                    </div>
                                </div>
                                <div class="page-title-actions">                                    
                                    <div class="d-inline-block dropdown">
                                        <a href="{{ url('/admin/contracts/approve') }}"    class="mb-2 mr-2 btn btn-info">                                            
                                            Phê duyệt đơn
                                        </a>                                         
                                    </div>
                                </div>   
                             </div>
                        </div>    
                        <div class="social-container">
			                <span>@include('admin.alert')</span>
			            </div>                                
                        @if(count($contracts)>0) 
                            <div class="main-card mb-3 card">
                                 <div class="card-body">
                                <h5 class="card-title">Danh sách đăng ký nội trú tại ký túc xá</h5>
                                @foreach($contracts as $contract)
                                    <div class="no-gutters row" id="c{{ $contract->id}}">
                                        <div class="col-md-6">
                                            <div class="widget-content">
                                                <div class="widget-content-wrapper">                                                
                                                    <div class="widget-content-left">
                                                        <div class="widget-heading">Số phòng: {{ $contract->room_code }}</div>
                                                        <div class="widget-heading">Sinh viên đăng ký: {{ $contract->name }} ||  {{ $contract->student_code }}</div>
                                                        <div class="widget-subheading">Ngày đăng ký {{ $contract->created_date }}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>                                   
                                        <div class="col-md-6">
                                            <div class="widget-content">
                                                <div class="widget-content-wrapper">                                                
                                                    <div class="widget-content-left">                                                  
                                                        <a href="{{ url('admin/contracts/show/'.$contract->id) }}" class="badge badge-success">Đã duyệt</a>
                                                    </div>
                                                    <div class="widget-content-right"> 
                                                        <a href="{{ url('admin/contracts/approve/'.$contract->id) }}" class="mb-2 mr-2 btn btn-info"><i class="pe-7s-look"></i></a>
                                                        <a href="javascript:void(0)" onclick="deleteContract({{$contract->id}})"
                                                        class="mb-2 mr-2 btn btn-danger"><i class="pe-7s-trash"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                               
                                @endforeach
                            </div>
                            </div>
                        @else 
                            <div class="list-group-item-warning list-group-item">Không tìm thấy yêu cầu đăng ký cho phòng cần tìm.</div>
                        @endif
                           
                            {{ $contracts->links() }}    
                    
                            
@endsection

<script>
    function deleteContract(id){       
        if(confirm("Bạn có chắc chắn muốn xoá phiếu đăng ký phòng?")){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url:'/admin/contracts/delete/'+id,                
                type:'GET',
                 data:{
                     _token : $("input[name=_token]").val()
                 },
                success:function(response){
                  $('#c'+id).remove();                  
                }
            });
        }
    }
 
</script>