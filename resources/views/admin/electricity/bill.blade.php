@extends('admin.main')
@section('content')

<div class="app-main__inner">                       
                            <div class="app-page-title">
                                <form method="get">
                                    <div class="page-title-wrapper">
                                        <div class="app-header__content">                                    
                                            <div class="app-header-left">
                                                <div class="search-wrapper active">
                                                    <div class="input-holder">
                                                        <input type="text" class="search-input" placeholder="Nhập số phòng" name="room">
                                                        <button class="search-icon"><span></span></button>
                                                    </div>
                                                    <button type="button" class="close"></button>
                                                </div>                                         
                                            </div>
                                        </div>
                                        
                                        <div class="page-title-actions">                                       
                                            <div class="mb-2 mr-2 btn-group">
                                                <button class="btn btn-focus"> <i class="pe-7s-filter"></i></button>
                                                <select name="semester_id" id="semester" class="dropdown-toggle-split dropdown-toggle btn btn-focus">
                                                        @foreach($semester as $s)
                                                        <option value="{{ $s->id }}">Học kỳ {{ $s->name }}</option>
                                                        @endforeach
                                                </select>                                      
                                            </div>  
                                            <div class="d-inline-block dropdown">
                                                <a href="{{ url('/admin/electricity/create') }}"    class="mb-2 mr-2 btn btn-info">                                            
                                                        Tạo phiếu điện nước
                                                </a>                                        
                                            </div>                                                                         
                                        </div>   
                                    </div>
                                </form>
                            </div>  
                        
                        <div class="social-container">
			                <span>@include('admin.alert')</span>
			            </div>           
                        <ul class="body-tabs body-tabs-layout tabs-animated body-tabs-animated nav">
                            <li class="nav-item">
                                <a role="tab" class="nav-link active" id="tab-0" data-toggle="tab" href="#tab-unpaid">
                                    <span>Danh sách phòng chưa nộp phí điện nước</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a role="tab" class="nav-link" id="tab-1" data-toggle="tab" href="#tab-paid">
                                    <span>Danh sách đã nộp phí</span>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane tabs-animation fade show active" id="tab-unpaid" role="tabpanel">
                                <div class="main-card mb-3 card">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="mb-0 table">
                                                <thead>
                                                    <tr>                                                   
                                                        <th>Số phòng</th>
                                                        <th>Học kỳ</th>
                                                        <th>Mã điện nước</th>
                                                        <th>Chỉ số điện</th>                                                    
                                                        <th>Chỉ số nước</th>
                                                        <th>Tổng tiền</th>
                                                        <th>Tình trạng</th>                                                        
                                                        <th>Cập nhật</th>                                                        
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($bills_unpaid as $bill)
                                                    <tr>
                                                        <th scope="row">{{ $bill->room_code }}</th> 
                                                        <td>{{ $bill->semester_name }}</td>
                                                        <td>{{ $bill->index_id }}</td>                                                    
                                                        <td>Chỉ số đầu: {{ $bill->old_e }} </br>
                                                            Chỉ số cuối: {{ $bill->new_e}}                                                    
                                                        </td>
                                                        <td>Chỉ số đầu: {{ $bill->old_w }} </br>
                                                            Chỉ số cuối: {{ $bill->new_w}}                                                    
                                                        </td>
                                                        <td>{{ number_format($bill->total, 3, ",", ".")}}.000 VND</td>                                                                                                             
                                                        <td>
                                                            <div class="mb-2 mr-2 badge badge-pill badge-warning">Chưa nộp</div>
                                                        </td>
                                                        <td>
                                                            <a href="{{ url('admin/electricity/fee/update/'.$bill->id) }}" class="mb-2 mr-2 btn btn-info" style="color:white;">Đã nộp
                                                            </a>
                                                        </td>                                                        
                                                    </tr> 
                                                @endforeach                                               
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>                              
                            </div>
                            <div class="tab-pane tabs-animation fade" id="tab-paid" role="tabpanel">
                                <div class="main-card mb-3 card">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="mb-0 table">
                                                <thead>
                                                    <tr>                                                   
                                                        <th>Số phòng</th>
                                                        <th>Học kỳ</th>
                                                        <th>Mã điện nước</th>
                                                        <th>Chỉ số điện</th>                                                    
                                                        <th>Chỉ số nước</th>
                                                        <th>Tổng tiền</th>
                                                        <th>Ngày nộp</th>
                                                        <th>Tình trạng</th>
                                                        <th></th>    
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @foreach ($bills_paid as $bill)
                                                    <tr id="b{{$bill->id}}">
                                                        <th scope="row">{{ $bill->room_code }}</th> 
                                                        <td>{{ $bill->semester_name }}</td>
                                                        <td>{{ $bill->index_id }}</td>                                                    
                                                        <td>Chỉ số đầu: {{ $bill->old_e }} </br>
                                                            Chỉ số cuối: {{ $bill->new_e}}                                                    
                                                        </td>
                                                        <td>Chỉ số đầu: {{ $bill->old_w }} </br>
                                                            Chỉ số cuối: {{ $bill->new_w}}                                                    
                                                        </td>                                                        
                                                        <td>{{ number_format($bill->total, 3, ",", ".")}}.000 VND</td> 
                                                        <td>{{ $bill->paid_date }}</td>                                                                                                             
                                                        <td>
                                                            <div class="mb-2 mr-2 badge badge-pill badge-success">Đã nộp</div>
                                                        </td>                                                        
                                                        <td>
                                                            <div class="widget-content-right">
                                                                <a href="javascript:void(0)" onclick="deleteNote({{$bill->id}})"
                                                                    class="mb-2 mr-2 btn btn-danger"><i class="pe-7s-trash"></i></a>
                                                            </div>
                                                        </td>                                                      
                                                    </tr> 
                                                @endforeach                                                
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                        </div>
                    </div>

@endsection

<script>
    function deleteNote(id){       
        if(confirm("Bạn có chắc chắn muốn xoá phiếu điện nước?")){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url:'/admin/electricity/delete/'+id,                
                type:'GET',
                 data:{
                     _token : $("input[name=_token]").val()
                 },
                success:function(response){
                  $('#b'+id).remove();                  
                }
            });
        }
    }
</script>