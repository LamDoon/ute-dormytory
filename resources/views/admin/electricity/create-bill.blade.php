@extends('admin.main')
@section('content')
<div class="app-main__inner">
<div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    <div class="page-title-icon">
                                        <i class="pe-7s-graph1 icon-gradient bg-premium-dark">
                                        </i>
                                    </div>
                                    <div>{{ $title}} học kỳ {{$semester->name}}
                                        <div class="page-title-subheading">Nhân viên tạo phiếu thông tin điện nước cho từng phòng.
                                        </div>
                                    </div>
                                </div>
                                <div class="page-title-actions">                                    
                                    <div class="d-inline-block dropdown">
                                        <a href="{{ url('/admin/electric-index/create') }}"    class="mb-2 mr-2 btn btn-info">                                            
                                            Thêm chỉ số
                                        </a>                                        
                                    </div>
                                </div>   
                             </div>
                        </div>                     
                        <div class="social-container">
			                <span>@include('admin.alert')</span>
			            </div>
                        <div class="tab-content">
                            <div class="tab-pane tabs-animation fade show active" id="tab-content-0" role="tabpanel">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="main-card mb-3 card">
                                            <div class="card-body">
                                                <h5 class="card-title">Thông tin phiếu</h5>
                                            <form class="" method="post" enctype="multipart/form-data">                                                 
                                                  <div class="row">  
                                                        <div class="col-md-12">
                                                                <label for="email" class="">Chọn chỉ số điện - nước</label>
                                                                <select class="mb-2 form-control" name="index_id"> 
                                                                    @foreach($electrics as $e)                                                            
                                                                        <option value="{{ $e->id }}">Đơn giá điện: {{ $e->e_price }} VND - Đơn giá nước: {{ $e->w_price }} VND</option>                                                                                                                  
                                                                    @endforeach                                                  
                                                                </select> 
                                                        </div> 
                                                        <div class="col-md-12">
                                                                <select class="mb-2 form-control" name="room_id"> 
                                                                    @foreach($rooms as $r)                                                            
                                                                        <option value="{{ $r->id }}" >Phòng: {{ $r->region}}{{ $r->code }}</option>                                                                                                                  
                                                                    @endforeach                                                  
                                                                </select> 
                                                        </div>
                                                        <div class="col-md-6">
                                                             <label for="old_e" class="">Chỉ số điện đầu</label>
                                                             <input name="old_e" placeholder="" type="text" class="form-control" value="" required="">
                                                        </div>
                                                        <div class="col-md-6">
                                                             <label for="new_e" class="">Chỉ số điện cuối</label>
                                                             <input name="new_e" placeholder="" type="text" class="form-control" value="" required="">
                                                        </div>
                                                        <div class="col-md-6">
                                                             <label for="old_w" class="">Chỉ số nước đầu</label>
                                                             <input name="old_w"  placeholder="" type="text" class="form-control" value="" required="">
                                                        </div>
                                                        <div class="col-md-6">
                                                             <label for="new_w" class="">Chỉ số nước cuối</label>
                                                             <input name="new_w"  placeholder="" type="text" class="form-control" value="" required="">
                                                        </div>  
                                                        <div class="col-md-12">                                                 
                                                            <button class="mt-1 btn btn-primary">Tạo phiếu thu</button>
                                                        </div>
                                                    @csrf
                                                    </div>    
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="main-card mb-3 card">
                                            <div class="card-body">
                                            <h5 class="card-title">Danh sách các phòng đã được lập phiếu</h5>
                                            @foreach($bills as $bill)
                                                <div class="no-gutters row" >
                                                    <div class="col-md-6">
                                                        <div class="widget-content">
                                                            <div class="widget-content-wrapper">                                                
                                                                <div class="widget-content-left">
                                                                    <div class="widget-heading">Phòng: {{ $bill->room_code }} - Khu vực: {{ $bill->region }}</div>                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>                                                                                      
                                                    <div class="col-md-6">
                                                        <div class="widget-content">
                                                            <div class="widget-content-wrapper">                                                
                                                                <div class="widget-content-right">    
                                                                    <div class="widget-subheading">{{ $bill->total }} VND</div>   
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                            </div>
                                        </div>                                     
                                    </div>
                                </div>
                            </div>                           
                        </div>
                    </div>

@endsection