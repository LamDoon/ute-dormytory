@extends('admin.main')
@section('content')

        <div class="app-main__inner">
                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading"> 
                                    <div class="page-title-icon">
                                        <i class="pe-7s-graph1 icon-gradient bg-premium-dark">
                                        </i>
                                    </div>                                   
                                    <div>{{ $title}}
                                        <div class="page-title-subheading">Chỉ số điện nước được dùng để tính phí điện nước cho từng phòng.
                                        </div>
                                    </div>
                                </div>
                                <div class="page-title-actions">                                   
                                    <div class="d-inline-block dropdown">
                                        <a href="{{ url('/admin/electric-index/list') }}"    class="mb-2 mr-2 btn btn-info">                                            
                                            Danh sách chỉ số
                                        </a>                                       
                                    </div>
                                </div>    
                            </div>
                        </div>       
                        <div class="social-container">
                            <span>@include('admin.alert')</span>
                        </div>         
                        <div class="row">   
                            <div class="col-md-12"> 
                                <div class="main-card mb-3 card">
                                    <div class="card-body">
                                        <h5 class="card-title">Các chỉ số điện nước trước</h5>
                                                <div class="col-md-6">
                                                        <div class="position-relative form-group"> 
                                                        <select class="mb-2 form-control"> 
                                                            @foreach($electrics as $e)                                                            
                                                                <option>Mã chỉ số {{ $e->id }}: Giá điện: {{ $e->e_price }}, Giá nước: {{ $e->w_price }}</option>                                  
                                                                                                                 
                                                            @endforeach                                                  
                                                        </select>    
                                                        </div>
                                                </div>
                                        <h5 class="card-title">Thêm chỉ số điện nước</h5>
                                            <form class="needs-validation" method="post" enctype="multipart/form-data">                                                
                                                 
                                                <div class="form-row">
                                                    <div class="col-md-6">
                                                        <div class="position-relative form-group">
                                                        <label for="exampleE" class="">Đơn giá điện</label>
                                                        <input name="e_price" id="exampleE"  type="text" class="form-control" value="" required="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="position-relative form-group">
                                                        <label for="exampleW" class="">Đơn giá nước</label>
                                                        <input name="w_price" id="exampleW" type="text" class="form-control" value="" required="">
                                                        </div>
                                                    </div>
                                                </div>                     
                                                                                                                                                      
                                                <button class="mt-1 btn btn-primary">Thêm chỉ số</button>
                                                @csrf
                                            </form>
                                    </div>
                                </div>                            
                            </div>   
                        </div>                  
        </div>
                 
@endsection
