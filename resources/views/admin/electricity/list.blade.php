@extends('admin.main')
@section('content')

        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon">
                            <i class="pe-7s-graph1 icon-gradient bg-premium-dark">
                            </i>
                        </div>
                        <div>{{ $title}}
                            <div class="page-title-subheading">Chỉ số điện nước được dùng để tính phí điện nước cho từng phòng.
                            </div>
                        </div>
                    </div>
                    <div class="page-title-actions">                                    
                        <div class="d-inline-block dropdown">
                        <a href="{{ url('/admin/electric-index/create') }}"    class="mb-2 mr-2 btn btn-info">                                            
                                Thêm chỉ số
                            </a>                                        
                        </div>
                    </div>   
                    </div>
            </div> 
            <div class="social-container">
                <span>@include('admin.alert')</span>
            </div>                        
            <div class="main-card mb-3 card">
                    <div class="card-body"><h5 class="card-title">{{ $title }}</h5>
                        <div class="no-gutters row">
                            <div class="table-responsive">
                                <table class="mb-0 table">
                                    <thead>
                                    <tr>                                                   
                                        <th>Mã</th>                                                    
                                        <th>Đơn giá điện</th>
                                        <th>Đơn giá nước</th>                                                   
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($electrics as $e)
                                    <tr id="e{{ $e->id }}">
                                        <th scope="row">{{ $e->id }}</th> 
                                        <td>{{ $e->e_price }}</td>                                                    
                                        <td>{{ $e->w_price }}</td>                                                                                                                                   
                                        <td>
                                            <div class="widget-content-right">                                                            
                                                <a href="javascript:void(0)" onclick="deleteElectric({{$e->id}})"
                                                class="mb-2 mr-2 btn btn-danger"><i class="pe-7s-trash"></i></a>
                                            </div>                                                            
                                        </td>
                                        
                                    </tr> 
                                    @endforeach                                               
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
            </div>
                            
    </div>                   
                    {{ $electrics->links() }}         
@endsection

<script>
    function deleteElectric(id){       
        if(confirm("Bạn có chắc chắn muốn xoá thông tin chỉ số điện?")){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url:'/admin/electric-index/delete/'+id,                
                type:'GET',
                 data:{
                     _token : $("input[name=_token]").val()
                 },
                success:function(response){
                  $('#e'+id).remove();                  
                }
            });
        }
    }
</script>