@extends('admin.main')
@section('content')
@can('admin-position') 
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading"> 
                        <div class="page-title-icon">
                            <i class="pe-7s-film icon-gradient bg-premium-dark">
                            </i>
                        </div>                                   
                        <div>{{ $title}}
                            <div class="page-title-subheading">Tin giới thiệu được hiển thị trên website hệ thống..
                            </div>
                        </div>
                    </div>
                    <div class="page-title-actions">                                   
                        <div class="d-inline-block dropdown">
                            <a href="{{ url('/admin/introductions/list') }}"    class="mb-2 mr-2 btn btn-info">                                            
                                Danh sách tin
                            </a>                                       
                        </div>
                    </div>    
                </div>
            </div>         
            <div class="social-container">
                <span>@include('admin.alert')</span>
            </div>         
            <div class="row">   
                <div class="col-md-12"> 
                    <div class="main-card mb-3 card">
                        <div class="card-body">
                            <h5 class="card-title">Thêm tin giới thiệu</h5>
                            <form class="needs-validation" method="post" enctype="multipart/form-data">                                                
                                <div class="position-relative form-group">
                                    <label for="exampleTitle" class="">Tiêu đề</label>
                                    <input name="title" id="exampleTitle" placeholder="Nhập tiêu đề" type="text" class="form-control">
                                </div>                                                     
                                <div class="position-relative form-group"> 
                                <label for="image" class="">Chọn ảnh hiển thị</label>
                                    <input class="form-control-file" name="image" type="file" id="image">
                                </div>                                         
                                <div class="position-relative form-group">
                                    <label for="exampleContent" class="">Nội dung</label>
                                    <textarea name="content" id="exampleContent" class="form-control"></textarea>
                                </div>
                                <div class="position-relative form-check form-check-inline">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" name="status" checked> Hiển thị trên website hệ thống.
                                    </label>
                                </div>                                                   
                                <button class="mt-1 btn btn-primary">Thêm tin</button>
                                @csrf
                            </form>
                        </div>
                    </div>                            
                </div>   
            </div>                  
        </div>
@endcan                 
@endsection
