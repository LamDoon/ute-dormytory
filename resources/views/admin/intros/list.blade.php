@extends('admin.main')
@section('content')

        <div class="app-main__inner">
            <div class="app-page-title">
                    <div class="page-title-wrapper">
                        <div class="page-title-heading"> 
                            <div class="page-title-icon">
                                <i class="pe-7s-film icon-gradient bg-premium-dark">
                                </i>
                            </div>                                   
                            <div>{{ $title}}
                                <div class="page-title-subheading">Tin giới thiệu được hiển thị trên website hệ thống.
                                </div>
                            </div>
                        </div>
                        <div class="page-title-actions">                                   
                            <div class="d-inline-block dropdown">
                                <a href="{{ url('/admin/introductions/create') }}"    class="mb-2 mr-2 btn btn-info">                                            
                                    Thêm tin
                                </a>                                       
                            </div>
                        </div>    
                    </div>
                </div>      
                <div class="social-container">
                    <span>@include('admin.alert')</span>
                </div>          
                    <div class="main-card mb-3 card">
                        @foreach($intros as $intro)
                            <div class="no-gutters row" id="i{{$intro->id}}">
                                <div class="col-md-6">
                                    <div class="widget-content">
                                        <div class="widget-content-wrapper">                                                
                                            <div class="widget-content-left">
                                                <div class="widget-heading">{{ $intro->title }}</div>
                                                <div class="widget-subheading">{{ $intro->content }}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                                   
                                <div class="col-md-6">
                                    <div class="widget-content">
                                        <div class="widget-content-wrapper">                                                
                                            <div class="widget-content-left">
                                                @if ($intro->status == 1)
                                                <a href="{{ url('admin/introductions/show/'.$intro->id) }}" class="badge badge-success">Cho phép hiển thị</a>
                                                @else 
                                                <a href="{{ url('admin/introductions/show/'.$intro->id) }}" class="badge badge-warning">Không hiển thị</a>
                                                @endif
                                            </div>
                                            <div class="widget-content-right">
                                                <a href="javascript:void(0)" onclick="deleteIntro({{$intro->id}})"
                                                class="mb-2 mr-2 btn btn-danger"><i class="pe-7s-trash"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
        {{ $intros->links() }}   
                            
@endsection

<script>
    function deleteIntro(id){       
        if(confirm("Bạn có chắc chắn muốn xoá tin giới thiệu?")){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url:'/admin/introductions/delete/'+id,                
                type:'GET',
                 data:{
                     _token : $("input[name=_token]").val()
                 },
                success:function(response){
                  $('#i'+id).remove();                  
                }
            });
        }
    }
</script>