<!DOCTYPE html>
<html>
<head>
<title>{{$title}}</title>
<link href="/assets/admin/css/login.css" rel="stylesheet">
</head>
<body>

<div class="container" id="container">
	<div class="form-container sign-in-container">
		<form action="/admin/login/store" method="POST">
			<h1>Đăng nhập</h1>
			<div class="social-container">
			<span>@include('admin.alert')</span>
			</div>
			<input type="email" placeholder="Email" name="email" />
			<input type="password" placeholder="Password" name="password" />
			<div class="group">
					<input id="check" type="checkbox" class="check" checked name="remember">
					<label class="a" for="check">Keep me Signed in</label>
			</div>
			<a href="#">Forgot your password?</a>
			<button>Đăng nhập</button>
			@csrf
		</form>
	</div>
	<div class="overlay-container">
		<div class="overlay">
			<div class="overlay-panel overlay-left">
				<h1>Welcome Back!</h1>
				<p>To keep connected with us please login with your personal info</p>
				<button class="ghost" id="signIn">Sign In</button>
			</div>
			<div class="overlay-panel overlay-right">
				<h1>Xin chào, Admin!</h1>
				<p>Nhập thông tin cá nhân của bạn và bắt đầu hành trình với chúng tôi</p>
				
			</div>
		</div>
	</div>
</div>
</body>
</html>
