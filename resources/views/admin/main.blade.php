
<!doctype html>
<html lang="en">
<head>   
 @include('admin.head')
</head>
<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    @include('admin.navbar')   
    <div class="ui-theme-settings">           
        </div>        
        <div class="app-main">
            @include('admin.sidebar')
                 <div class="app-main__outer">
                 <div class="app-main__inner">   
                  @yield('content')
                 </div>
                    <div class="app-wrapper-footer">
                        <div class="app-footer">
                            <div class="app-footer__inner">
                                <div class="app-footer-left">
                                    <ul class="nav">
                                        <li class="nav-item">
                                            <a href="javascript:void(0);" class="nav-link">
                                                Trường Đại học Sư phạm Kỹ thuật
                                            </a>
                                        </li>
                                        
                                    </ul>
                                </div>
                                <div class="app-footer-right">
                                    <ul class="nav">
                                        
                                        <li class="nav-item">
                                            <a href="javascript:void(0);" class="nav-link">
                                                <div class="badge badge-success mr-1 ml-0">
                                                    <small>NEW</small>
                                                </div>
                                                Trang Ký túc xá
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>    
                </div>
                <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
        </div>
    </div>
@include('admin.footer')

</body>
</html>
