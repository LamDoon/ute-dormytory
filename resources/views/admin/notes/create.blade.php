@extends('admin.main')
@section('content')
@can('admin-position') 
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon">
                            <i class="pe-7s-id icon-gradient bg-premium-dark">
                            </i>
                        </div>
                        <div>{{ $title}}
                            <div class="page-title-subheading">Tin thông báo từ ban quản lý sẽ được hiển thị trên website dành cho sinh viên đang đăng ký nội trú.
                            </div>
                        </div>
                    </div>
                    <div class="page-title-actions">                                    
                        <div class="d-inline-block dropdown">
                            <a href="{{ url('/admin/notifications/list') }}"    class="mb-2 mr-2 btn btn-info">                                            
                                Danh sách tin
                            </a>                                        
                        </div>
                    </div>   
                </div>
            </div>    
            <div class="social-container">
                <span>@include('admin.alert')</span>
            </div>         
            <div class="row">   
                <div class="col-md-12"> 
                    <div class="main-card mb-3 card">
                        <div class="card-body">
                            <h5 class="card-title">Thêm tin thông báo</h5>
                            <form class="needs-validation" method="post" enctype="multipart/form-data">                                                
                                <div class="position-relative form-group">
                                    <label for="exampleTitle" class="">Tiêu đề</label>
                                    <input name="title" id="exampleTitle" placeholder="Nhập tiêu đề" type="text" class="form-control">
                                </div>                           
                                <div class="position-relative form-group">
                                    <label for="exampleContent" class="">Nội dung</label>
                                    <textarea name="content" id="exampleContent" class="form-control"></textarea>
                                </div>                                                                                                       
                                <button class="mt-1 btn btn-primary">Thêm thông báo</button>
                                @csrf
                            </form>
                        </div>
                    </div>                            
                </div>   
            </div>                  
        </div>
 @endcan                
@endsection
