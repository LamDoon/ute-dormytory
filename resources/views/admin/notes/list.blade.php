@extends('admin.main')
@section('content')

        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon">
                            <i class="pe-7s-id icon-gradient bg-premium-dark">
                            </i>
                        </div>
                        <div>{{ $title}}
                            <div class="page-title-subheading">Tin thông báo từ ban quản lý sẽ được hiển thị trên website dành cho sinh viên đang đăng ký nội trú.
                            </div>
                        </div>
                    </div>
                    <div class="page-title-actions">                                    
                        <div class="d-inline-block dropdown">
                            <a  href="{{ url('/admin/notifications/create') }}"    class="mb-2 mr-2 btn btn-info">                                            
                                    Thêm tin
                            </a>                                        
                        </div>
                    </div>   
                </div>
            </div> 
            <div class="social-container">
                <span>@include('admin.alert')</span>
            </div>              
            <div class="main-card mb-3 card">
                @foreach($notes as $note)                            
                    <div class="no-gutters row" id="n{{$note->id}}">
                        <div class="col-md-6">
                            <div class="widget-content">
                                <div class="widget-content-wrapper">                                                
                                    <div class="widget-content-left">
                                        <div class="widget-heading">{{ $note->title }}</div>
                                        <div class="widget-subheading">{{ $note->content }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>                                   
                        <div class="col-md-6">
                            <div class="widget-content">
                                <div class="widget-content-wrapper">                                                
                                    <div class="widget-content-right">                                                   
                                        <div class="widget-content-right">
                                            <a href="javascript:void(0)" onclick="deleteNote({{$note->id}})"
                                            class="mb-2 mr-2 btn btn-danger"><i class="pe-7s-trash"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    {{ $notes->links() }}   
                            
@endsection

<script>
    function deleteNote(id){       
        if(confirm("Bạn có chắc chắn muốn xoá thông báo?")){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url:'/admin/notifications/delete/'+id,                
                type:'GET',
                 data:{
                     _token : $("input[name=_token]").val()
                 },
                success:function(response){
                  $('#n'+id).remove();                  
                }
            });
        }
    }
 
</script>