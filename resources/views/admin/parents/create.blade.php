@extends('admin.main')
@section('content')
@can('admin-position') 
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon">
                            <i class="pe-7s-user icon-gradient bg-premium-dark">
                            </i>
                        </div>
                        <div>{{ $title}}
                            <div class="page-title-subheading">Wide selection of forms controls, using the Bootstrap 4 code base, but built with React.
                            </div>
                        </div>
                    </div>
                    <div class="page-title-actions">                                    
                        <div class="d-inline-block dropdown">
                            <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="mb-2 mr-2 btn btn-info">
                                
                                Buttons
                            </button>                                        
                        </div>
                    </div>   
                    </div>
            </div>    
            <div class="social-container">
                <span>@include('admin.alert')</span>
            </div>         
            <div class="row">   
                <div class="col-md-12"> 
                    <div class="main-card mb-3 card">
                        <div class="card-body">
                            <h5 class="card-title">Thêm thông tin thân nhân</h5>
                                    <form class="needs-validation" method="post">
                                        <div class="form-row">
                                            <div class="col-md-6">
                                                <div class="position-relative form-group">
                                                    <label for="exampleName" class="">Họ và tên</label>
                                                    <input name="name" id="exampleName" placeholder="Nhập họ và tên" type="text" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="position-relative form-group">
                                                    <label for="examplePhone" class="">Số điện thoại</label>
                                                    <input name="phone" id="examplePhone" placeholder="Nhập số điện thoại" type="text" class="form-control" required>
                                                </div> 
                                            </div>
                                        </div>
                                        <div class="form-row">                                                        
                                            <div class="col-md-12">
                                                <div class="position-relative form-group">
                                                    <label for="exampleAddress" class="">Địa chỉ</label>
                                                    <textarea name="address" id="exampleAddress" placeholder="" type="text" class="form-control"></textarea>
                                                </div>
                                            </div>
                                        </div>  
                                        <button class="mt-1 btn btn-primary">Thêm thân nhân</button>
                                        @csrf
                                    </form>
                        </div>
                    </div>                            
                </div>   
            </div>                  
    </div>
  @endif               
@endsection
