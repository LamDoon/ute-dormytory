@extends('admin.main')
@section('content')

        <div class="app-main__inner">
            <div class="app-page-title">
                <form method="get">
                    <div class="page-title-wrapper">
                        <div class="app-header__content">                                    
                            <div class="app-header-left">
                                <div class="search-wrapper active">
                                    <div class="input-holder">
                                        <input type="text" class="search-input" placeholder="Nhập mã phụ huynh" name="key_search">
                                        <button class="search-icon"><span></span></button>
                                    </div>
                                    <button type="button" class="close"></button>
                                </div>                                         
                            </div>
                        </div>   
                        <div class="page-title-actions">                                   
                            <div class="d-inline-block dropdown">
                                <a href="{{ url('/admin/parents/create') }}"    class="mb-2 mr-2 btn btn-info">                                            
                                    Thêm thân nhân
                                </a>                                       
                            </div>
                        </div>         
                    </div>
                </form>
            </div>  
            @if(count($parents)>0)              
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <h5 class="card-title">{{ $title }}</h5>
                        <div class="table-responsive">
                            <table class="mb-0 table" style="font-size: 14px !important;">
                                <thead>
                                <tr>                                                   
                                    <th>Mã thân nhân</th>
                                    <th>Họ tên</th>
                                    <th>Số điện thoại</th>
                                    <th>Địa chỉ</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach ($parents as $parent)
                                    <tr id="s{{$parent->id}}">
                                        <th scope="row">{{ $parent->id }}</th>                                                   
                                        <td>{{ $parent->name }}</td>
                                        <td>{{ $parent->phone }}</td>                                 
                                                                                            
                                        @can('admin-position')                                  
                                        <td>

                                        </td>
                                    @endcan
                                </tr> 
                                @endforeach                                               
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>                    
                    
            @else 
            <div class="list-group-item-warning list-group-item">Không tìm thấy thân nhân</div>
            @endif
        {{ $parents->links() }}
@endsection

<script>
    function deleteStudent(id){       
        if(confirm("Bạn có chắc chắn muốn xoá thông tin sinh viên?")){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url:'/admin/students/delete/'+id,                
                type:'GET',
                 data:{
                     _token : $("input[name=_token]").val()
                 },
                success:function(response){
                  $('#s'+id).remove();                  
                }
            });
        }
    }
</script>