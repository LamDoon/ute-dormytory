@extends('admin.main')
@section('content')
<div class="app-main__inner">
                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading"> 
                                    <div class="page-title-icon">
                                        <i class="pe-7s-albums icon-gradient bg-premium-dark">
                                        </i>
                                    </div>                                   
                                    <div>{{ $title}}
                                        <div class="page-title-subheading">Thông tin, vị trí phòng nội trú hiện hành tại ký túc xá.
                                        </div>
                                    </div>
                                </div>
                                <div class="page-title-actions">                                   
                                    <div class="d-inline-block dropdown">
                                        <a href="{{ url('/admin/semesters/create') }}"    class="mb-2 mr-2 btn btn-info">                                            
                                            Tạo học kỳ
                                        </a>                                       
                                    </div>
                                </div>    
                            </div>
                        </div>                       
                        <div class="social-container">
			                <span>@include('admin.alert')</span>
			            </div>
                        <div class="tab-content">
                            <div class="tab-pane tabs-animation fade show active" id="tab-content-0" role="tabpanel">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="main-card mb-3 card">
                                            <div class="card-body">
                                                <h5 class="card-title">Thông tin phòng</h5>                                          
                                                    <div class="card mb-3 widget-content">
                                                        <div class="widget-content-outer">
                                                            <a href="" class="nav-link" >
                                                                <div class="widget-content-wrapper">
                                                                    <div class="widget-content-left">
                                                                        <div class="widget-numbers mt-0 fsize-3 text-danger">Phòng {{ $room->code }}</div>
                                                                        <div class="widget-heading">Khu vực: {{ $room->region }} Dãy tầng: {{ $room->floors }}</div>                                                                                       
                                                                        <div class="widget-heading">Số giường cho phép: {{ $room->regist_max }}</div>
                                                                        <div class="widget-subheading">Đã đăng ký: {{ $room->regist_num }}</div>                                                                                        
                                                                    </div>
                                                                    <div class="widget-content-right">
                                                                        <div class="widget-subheading">Số giường trống: {{ $room->regist_max - $room->regist_num }}</div>                                                                                       
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div> 
                                                <h5 class="card-title">Thành viên</h5>  
                                                @foreach ($students as $student)
                                                <div class="card mb-3 widget-content">
                                                    <div class="widget-content-outer">                                                        
                                                            <div class="widget-content-wrapper">
                                                                <a href="{{ url('admin/students/detail/'.$student->student_id)  }}" class="nav-link" >
                                                                    <div class="widget-content-left">
                                                                        <div class="widget-heading">MSSV: {{ $student->code }}</div>
                                                                        <div class="widget-heading">Họ tên: {{ $student->name }}</div>  
                                                                        <div class="widget-heading">Khoá học: {{ $student->course }}</div>                                                                        
                                                                    </div>
                                                                </a>                                                               
                                                                    @if($student->paid == 1)
                                                                        <div class="widget-content-right">
                                                                        <a href="{{ url('admin/contracts/fee')}}" class="nav-link" style="float: right;" >
                                                                            <div class="badge badge-success">Đã nộp phí</div>  
                                                                        </a>                                                                                     
                                                                        </div>
                                                                    @else 
                                                                        <div class="widget-content-right">
                                                                        <a href="{{ url('admin/contracts/fee')}}" class="nav-link" style="float: right;" >
                                                                            <div class="badge badge-warning">Chưa nộp phí</div>    
                                                                        </a>                                                                                   
                                                                        </div>
                                                                    @endif                                                               
                                                            </div>                                                           
                                                    </div>
                                                </div> 
                                                @endforeach                                              
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="main-card mb-3 card">
                                            <div class="card-body"><h5 class="card-title">Thông tin ghi chú</h5>
                                            <form class="" method="post" enctype="multipart/form-data" action="">
                                                    <div class="position-relative form-group">
                                                        <label for="message_title" class="">Tiêu đề</label>
                                                        <input name="message_title" id="message_title" placeholder="" type="text" class="form-control" value="{{ $room->message_title }}">
                                                    </div>
                                                    <div class="position-relative form-group">
                                                        <label for="message" class="">Nội dung</label>
                                                        <textarea name="message" id="message" placeholder="" type="text" class="form-control">{{ $room->message }}</textarea>
                                                    </div>                       
                                                    <button class="mt-1 btn btn-primary">Lưu cập nhật</button>                                                    
                                                    @csrf
                                                </form>
                                            </div>
                                        </div>                                        
                                    </div>
                                </div>
                            </div>
                           
                        </div>
                    </div>

@endsection