@extends('admin.main')
@section('content')

<div class="app-main__inner">
                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading"> 
                                    <div class="page-title-icon">
                                        <i class="pe-7s-albums icon-gradient bg-premium-dark">
                                        </i>
                                    </div>                                   
                                    <div>{{ $title}}
                                        <div class="page-title-subheading">Thông tin, vị trí phòng nội trú hiện hành tại ký túc xá.
                                        </div>
                                    </div>
                                </div>
                                <div class="page-title-actions">                                   
                                    <div class="d-inline-block dropdown">
                                        <a href="{{ url('/admin/semesters/create') }}"class="mb-2 mr-2 btn btn-info">                                            
                                            Thêm học kỳ
                                        </a>                                       
                                    </div>
                                </div>    
                            </div>
                        </div>  
                        <div class="social-container">
			                <span>@include('admin.alert')</span>
			            </div>            
                        <ul class="body-tabs body-tabs-layout tabs-animated body-tabs-animated nav">
                            <li class="nav-item">
                                <a role="tab" class="nav-link active" id="tab-0" data-toggle="tab" href="#tab-content-a">
                                    <span>Khu A</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a role="tab" class="nav-link" id="tab-1" data-toggle="tab" href="#tab-content-b">
                                    <span>Khu B</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a role="tab" class="nav-link" id="tab-2" data-toggle="tab" href="#tab-content-c">
                                    <span>Khu C</span>
                                </a>
                            </li>                           
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane tabs-animation fade show active" id="tab-content-a" role="tabpanel">
                                <div class="row">                                   
                                    <div class="main-card mb-3 card">
                                            <div class="card-header">
                                                <i class="header-icon lnr-license icon-gradient bg-plum-plate"> </i>Danh sách phòng
                                                <div class="btn-actions-pane-right">
                                                    <div class="nav">
                                                        <a data-toggle="tab" href="#tab-a-1" class="btn-pill btn-wide active btn btn-outline-alternate btn-sm">Tầng 1</a>
                                                        <a data-toggle="tab" href="#tab-a-2" class="btn-pill btn-wide mr-1 ml-1  btn btn-outline-alternate btn-sm">Tầng 2</a>
                                                        <a data-toggle="tab" href="#tab-a-3" class="btn-pill btn-wide  btn btn-outline-alternate btn-sm">Tầng 3</a>
                                                        <a data-toggle="tab" href="#tab-a-4" class="btn-pill btn-wide mr-1 ml-1  btn btn-outline-alternate btn-sm">Tầng 4</a>
                                                        <a data-toggle="tab" href="#tab-a-5" class="btn-pill btn-wide  btn btn-outline-alternate btn-sm">Tầng 5</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="tab-a-1" role="tabpanel">
                                                        <div class="row">
                                                            @foreach($a1 as $a1)
                                                                <div class="col-md-6 col-xl-4">                                                                   
                                                                    <div class="card mb-3 widget-content">
                                                                        <div class="widget-content-outer">
                                                                            <a href="{{ url('admin/rooms/detail/'.$a1->id) }}" class="nav-link" >
                                                                                <div class="widget-content-wrapper">
                                                                                    <div class="widget-content-left">
                                                                                        <div class="widget-numbers mt-0 fsize-3 text-danger">{{ $a1->code }}</div>
                                                                                        <div class="widget-heading">Số giường: {{ $a1->regist_max }}</div>
                                                                                        <div class="widget-subheading">Đã đăng ký: {{ $a1->regist_num }}</div>
                                                                                        <div class="widget-heading">Ghi chú: {{ $a1->message_title }}</div>
                                                                                    </div>
                                                                                    <div class="widget-content-right">
                                                                                        <div class="widget-numbers text-success">{{ $a1->regist_max - $a1->regist_num }}</div>
                                                                                    </div>
                                                                                </div>
                                                                            </a>
                                                                        </div>
                                                                    </div>                                                                    
                                                                </div>
                                                            @endforeach                                                         
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="tab-a-2" role="tabpanel">
                                                        <div class="row">
                                                                @foreach($a2 as $a2)
                                                                    <div class="col-md-6 col-xl-4">
                                                                        <div class="card mb-3 widget-content">
                                                                            <div class="widget-content-outer">
                                                                            <a href="{{ url('admin/rooms/detail/'.$a2->id) }}" class="nav-link" >
                                                                                <div class="widget-content-wrapper">
                                                                                    <div class="widget-content-left">
                                                                                        <div class="widget-numbers mt-0 fsize-3 text-danger">{{ $a2->code }}</div>
                                                                                        <div class="widget-heading">Số giường: {{ $a2->regist_max }}</div>
                                                                                        <div class="widget-subheading">Đã đăng ký: {{ $a2->regist_num }}</div>
                                                                                        <div class="widget-heading">Ghi chú: {{ $a2->message_title }}</div>
                                                                                    </div>
                                                                                    <div class="widget-content-right">
                                                                                        <div class="widget-numbers text-success">{{ $a2->regist_max - $a2->regist_num }}</div>
                                                                                    </div>
                                                                                </div>
                                                                            </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endforeach                                                         
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="tab-a-3" role="tabpanel">
                                                        <div class="row">
                                                                @foreach($a3 as $a3)
                                                                    <div class="col-md-6 col-xl-4">
                                                                        <div class="card mb-3 widget-content">
                                                                            <div class="widget-content-outer">
                                                                            <a href="{{ url('admin/rooms/detail/'.$a3->id) }}" class="nav-link" >
                                                                                <div class="widget-content-wrapper">
                                                                                    <div class="widget-content-left">
                                                                                        <div class="widget-numbers mt-0 fsize-3 text-danger">{{ $a3->code }}</div>
                                                                                        <div class="widget-heading">Số giường: {{ $a3->regist_max }}</div>
                                                                                        <div class="widget-subheading">Đã đăng ký: {{ $a3->regist_num }}</div>
                                                                                        <div class="widget-heading">Ghi chú: {{ $a3->message_title }}</div>
                                                                                    </div>
                                                                                    <div class="widget-content-right">
                                                                                        <div class="widget-numbers text-success">{{ $a3->regist_max - $a3->regist_num }}</div>
                                                                                    </div>
                                                                                </div>
                                                                            </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endforeach                                                         
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="tab-a-4" role="tabpanel">
                                                        <div class="row">
                                                                @foreach($a4 as $a4)
                                                                    <div class="col-md-6 col-xl-4">
                                                                        <div class="card mb-3 widget-content">
                                                                            <div class="widget-content-outer">
                                                                            <a href="{{ url('admin/rooms/detail/'.$a4->id) }}" class="nav-link" >
                                                                                <div class="widget-content-wrapper">
                                                                                    <div class="widget-content-left">
                                                                                        <div class="widget-numbers mt-0 fsize-3 text-danger">{{ $a4->code }}</div>
                                                                                        <div class="widget-heading">Số giường: {{ $a4->regist_max }}</div>
                                                                                        <div class="widget-subheading">Đã đăng ký: {{ $a4->regist_num }}</div>
                                                                                        <div class="widget-heading">Ghi chú: {{ $a4->message_title }}</div>
                                                                                    </div>
                                                                                    <div class="widget-content-right">
                                                                                        <div class="widget-numbers text-success">{{ $a4->regist_max - $a4->regist_num }}</div>
                                                                                    </div>
                                                                                </div>
                                                                            </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endforeach                                                         
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="tab-a-5" role="tabpanel">
                                                        <div class="row">
                                                                @foreach($a5 as $a5)
                                                                    <div class="col-md-6 col-xl-4">
                                                                        <div class="card mb-3 widget-content">
                                                                            <div class="widget-content-outer">
                                                                            <a href="{{ url('admin/rooms/detail/'.$a5->id) }}" class="nav-link" >
                                                                                <div class="widget-content-wrapper">
                                                                                    <div class="widget-content-left">
                                                                                        <div class="widget-numbers mt-0 fsize-3 text-danger">{{ $a5->code }}</div>
                                                                                        <div class="widget-heading">Số giường: {{ $a5->regist_max }}</div>
                                                                                        <div class="widget-subheading">Đã đăng ký: {{ $a5->regist_num }}</div>
                                                                                        <div class="widget-heading">Ghi chú: {{ $a5->message_title }}</div>
                                                                                    </div>
                                                                                    <div class="widget-content-right">
                                                                                        <div class="widget-numbers text-success">{{ $a5->regist_max - $a5->regist_num }}</div>
                                                                                    </div>
                                                                                </div>
                                                                            </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endforeach                                                         
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @can('admin-position') 
                                            <form>
                                                <div class="d-block text-right card-footer">
                                                    <a href="{{ url('admin/rooms/update')}}" class="btn-wide btn btn-success">Reset về 0</a>
                                                </div>
                                                @csrf
                                            </form>
                                            @endcan
                                        </div>                                                                    
                                </div>
                            </div>
                            <div class="tab-pane tabs-animation fade" id="tab-content-b" role="tabpanel">
                                <div class="row">                                   
                                    <div class="main-card mb-3 card">
                                            <div class="card-header">
                                                <i class="header-icon lnr-license icon-gradient bg-plum-plate"> </i>Danh sách phòng
                                                <div class="btn-actions-pane-right">
                                                    <div class="nav">
                                                        <a data-toggle="tab" href="#tab-b-1" class="btn-pill btn-wide active btn btn-outline-alternate btn-sm">Tầng 1</a>
                                                        <a data-toggle="tab" href="#tab-b-2" class="btn-pill btn-wide mr-1 ml-1  btn btn-outline-alternate btn-sm">Tầng 2</a>
                                                        <a data-toggle="tab" href="#tab-b-3" class="btn-pill btn-wide  btn btn-outline-alternate btn-sm">Tầng 3</a>
                                                        <a data-toggle="tab" href="#tab-b-4" class="btn-pill btn-wide mr-1 ml-1  btn btn-outline-alternate btn-sm">Tầng 4</a>
                                                        <a data-toggle="tab" href="#tab-b-5" class="btn-pill btn-wide  btn btn-outline-alternate btn-sm">Tầng 5</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="tab-b-1" role="tabpanel">
                                                        <div class="row">
                                                            @foreach($b1 as $b1)
                                                                <div class="col-md-6 col-xl-4">
                                                                    <div class="card mb-3 widget-content">
                                                                        <div class="widget-content-outer">
                                                                        <a href="{{ url('admin/rooms/detail/'.$b1->id) }}" class="nav-link" >
                                                                            <div class="widget-content-wrapper">
                                                                                <div class="widget-content-left">
                                                                                    <div class="widget-numbers mt-0 fsize-3 text-danger">{{ $b1->code }}</div>
                                                                                    <div class="widget-heading">Số giường: {{ $b1->regist_max }}</div>
                                                                                    <div class="widget-subheading">Đã đăng ký: {{ $b1->regist_num }}</div>
                                                                                    <div class="widget-heading">Ghi chú: {{ $b1->message_title }}</div>
                                                                                </div>
                                                                                <div class="widget-content-right">
                                                                                    <div class="widget-numbers text-success">{{ $b1->regist_max - $b1->regist_num }}</div>
                                                                                </div>
                                                                            </div>
                                                                        </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endforeach                                                         
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="tab-b-2" role="tabpanel">
                                                        <div class="row">
                                                                @foreach($b2 as $b2)
                                                                    <div class="col-md-6 col-xl-4">
                                                                        <div class="card mb-3 widget-content">
                                                                            <div class="widget-content-outer">
                                                                            <a href="{{ url('admin/rooms/detail/'.$b2->id) }}" class="nav-link" >
                                                                                <div class="widget-content-wrapper">
                                                                                    <div class="widget-content-left">
                                                                                        <div class="widget-numbers mt-0 fsize-3 text-danger">{{ $b2->code }}</div>
                                                                                        <div class="widget-heading">Số giường: {{ $b2->regist_max }}</div>
                                                                                        <div class="widget-subheading">Đã đăng ký: {{ $b2->regist_num }}</div>
                                                                                        <div class="widget-heading">Ghi chú: {{ $b2->message_title }}</div>
                                                                                    </div>
                                                                                    <div class="widget-content-right">
                                                                                        <div class="widget-numbers text-success">{{ $b2->regist_max - $b2->regist_num }}</div>
                                                                                    </div>
                                                                                </div>
                                                                            </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endforeach                                                         
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="tab-b-3" role="tabpanel">
                                                        <div class="row">
                                                                @foreach($b3 as $b3)
                                                                    <div class="col-md-6 col-xl-4">
                                                                        <div class="card mb-3 widget-content">
                                                                            <div class="widget-content-outer">
                                                                            <a href="{{ url('admin/rooms/detail/'.$b3->id) }}" class="nav-link" >
                                                                                <div class="widget-content-wrapper">
                                                                                    <div class="widget-content-left">
                                                                                        <div class="widget-numbers mt-0 fsize-3 text-danger">{{ $b3->code }}</div>
                                                                                        <div class="widget-heading">Số giường: {{ $b3->regist_max }}</div>
                                                                                        <div class="widget-subheading">Đã đăng ký: {{ $b3->regist_num }}</div>
                                                                                        <div class="widget-heading">Ghi chú: {{ $b3->message_title }}</div>
                                                                                    </div>
                                                                                    <div class="widget-content-right">
                                                                                        <div class="widget-numbers text-success">{{ $b3->regist_max - $b3->regist_num }}</div>
                                                                                    </div>
                                                                                </div>
                                                                            </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endforeach                                                         
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="tab-b-4" role="tabpanel">
                                                        <div class="row">
                                                                @foreach($b4 as $b4)
                                                                    <div class="col-md-6 col-xl-4">
                                                                        <div class="card mb-3 widget-content">
                                                                            <div class="widget-content-outer">
                                                                            <a href="{{ url('admin/rooms/detail/'.$b4->id) }}" class="nav-link" >
                                                                                <div class="widget-content-wrapper">
                                                                                    <div class="widget-content-left">
                                                                                        <div class="widget-numbers mt-0 fsize-3 text-danger">{{ $b4->code }}</div>
                                                                                        <div class="widget-heading">Số giường: {{ $b4->regist_max }}</div>
                                                                                        <div class="widget-subheading">Đã đăng ký: {{ $b4->regist_num }}</div>
                                                                                        <div class="widget-heading">Ghi chú: {{ $b4->message_title }}</div>
                                                                                    </div>
                                                                                    <div class="widget-content-right">
                                                                                        <div class="widget-numbers text-success">{{ $b4->regist_max - $b4->regist_num }}</div>
                                                                                    </div>
                                                                                </div>
                                                                            </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endforeach                                                         
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="tab-b-5" role="tabpanel">
                                                        <div class="row">
                                                                @foreach($b5 as $b5)
                                                                    <div class="col-md-6 col-xl-4">
                                                                        <div class="card mb-3 widget-content">
                                                                            <div class="widget-content-outer">
                                                                            <a href="{{ url('admin/rooms/detail/'.$b5->id) }}" class="nav-link" >
                                                                                <div class="widget-content-wrapper">
                                                                                    <div class="widget-content-left">
                                                                                        <div class="widget-numbers mt-0 fsize-3 text-danger">{{ $b5->code }}</div>
                                                                                        <div class="widget-heading">Số giường: {{ $b5->regist_max }}</div>
                                                                                        <div class="widget-subheading">Đã đăng ký: {{ $b5->regist_num }}</div>
                                                                                        <div class="widget-heading">Ghi chú: {{ $b5->message_title }}</div>
                                                                                    </div>
                                                                                    <div class="widget-content-right">
                                                                                        <div class="widget-numbers text-success">{{ $b5->regist_max - $b5->regist_num }}</div>
                                                                                    </div>
                                                                                </div>
                                                                            </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endforeach                                                         
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @can('admin-position') 
                                            <form>
                                                <div class="d-block text-right card-footer">
                                                    <a href="{{ url('admin/rooms/update')}}" class="btn-wide btn btn-success">Reset về 0</a>
                                                </div>
                                                @csrf
                                            </form>
                                            @endcan
                                        </div>                                                                    
                                </div>            
                            </div>
                            <div class="tab-pane tabs-animation fade" id="tab-content-c" role="tabpanel">
                                <div class="row">                                   
                                    <div class="main-card mb-3 card">
                                            <div class="card-header">
                                                <i class="header-icon lnr-license icon-gradient bg-plum-plate"> </i>Danh sách phòng
                                                <div class="btn-actions-pane-right">
                                                    <div class="nav">
                                                        <a data-toggle="tab" href="#tab-c-1" class="btn-pill btn-wide active btn btn-outline-alternate btn-sm">Tầng 1</a>
                                                        <a data-toggle="tab" href="#tab-c-2" class="btn-pill btn-wide mr-1 ml-1  btn btn-outline-alternate btn-sm">Tầng 2</a>
                                                        <a data-toggle="tab" href="#tab-c-3" class="btn-pill btn-wide  btn btn-outline-alternate btn-sm">Tầng 3</a>
                                                        <a data-toggle="tab" href="#tab-c-4" class="btn-pill btn-wide mr-1 ml-1  btn btn-outline-alternate btn-sm">Tầng 4</a>
                                                        <a data-toggle="tab" href="#tab-c-5" class="btn-pill btn-wide  btn btn-outline-alternate btn-sm">Tầng 5</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="tab-c-1" role="tabpanel">
                                                        <div class="row">
                                                            @foreach($c1 as $c1)
                                                                <div class="col-md-6 col-xl-4">
                                                                    <div class="card mb-3 widget-content">
                                                                        <div class="widget-content-outer">
                                                                        <a href="{{ url('admin/rooms/detail/'.$c1->id) }}" class="nav-link" >
                                                                            <div class="widget-content-wrapper">
                                                                                <div class="widget-content-left">
                                                                                    <div class="widget-numbers mt-0 fsize-3 text-danger">{{ $c1->code }}</div>
                                                                                    <div class="widget-heading">Số giường: {{ $c1->regist_max }}</div>
                                                                                    <div class="widget-subheading">Đã đăng ký: {{ $c1->regist_num }}</div>
                                                                                    <div class="widget-heading">Ghi chú: {{ $c1->message_title }}</div>
                                                                                </div>
                                                                                <div class="widget-content-right">
                                                                                    <div class="widget-numbers text-success">{{ $c1->regist_max - $c1->regist_num }}</div>
                                                                                </div>
                                                                            </div>
                                                                        </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endforeach                                                         
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="tab-c-2" role="tabpanel">
                                                        <div class="row">
                                                                @foreach($c2 as $c2)
                                                                    <div class="col-md-6 col-xl-4">
                                                                        <div class="card mb-3 widget-content">
                                                                            <div class="widget-content-outer">
                                                                            <a href="{{ url('admin/rooms/detail/'.$c2->id) }}" class="nav-link" >
                                                                                <div class="widget-content-wrapper">
                                                                                    <div class="widget-content-left">
                                                                                        <div class="widget-numbers mt-0 fsize-3 text-danger">{{ $c2->code }}</div>
                                                                                        <div class="widget-heading">Số giường: {{ $c2->regist_max }}</div>
                                                                                        <div class="widget-subheading">Đã đăng ký: {{ $c2->regist_num }}</div>
                                                                                        <div class="widget-heading">Ghi chú: {{ $c2->message_title }}</div>
                                                                                    </div>
                                                                                    <div class="widget-content-right">
                                                                                        <div class="widget-numbers text-success">{{ $c2->regist_max - $c2->regist_num }}</div>
                                                                                    </div>
                                                                                </div>
                                                                            </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endforeach                                                         
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="tab-c-3" role="tabpanel">
                                                        <div class="row">
                                                                @foreach($c3 as $c3)
                                                                    <div class="col-md-6 col-xl-4">
                                                                        <div class="card mb-3 widget-content">
                                                                            <div class="widget-content-outer">
                                                                            <a href="{{ url('admin/rooms/detail/'.$c3->id) }}" class="nav-link" >
                                                                                <div class="widget-content-wrapper">
                                                                                    <div class="widget-content-left">
                                                                                        <div class="widget-numbers mt-0 fsize-3 text-danger">{{ $c3->code }}</div>
                                                                                        <div class="widget-heading">Số giường: {{ $c3->regist_max }}</div>
                                                                                        <div class="widget-subheading">Đã đăng ký: {{ $c3->regist_num }}</div>
                                                                                        <div class="widget-heading">Ghi chú: {{ $c3->message_title }}</div>
                                                                                    </div>
                                                                                    <div class="widget-content-right">
                                                                                        <div class="widget-numbers text-success">{{ $c3->regist_max - $c3->regist_num }}</div>
                                                                                    </div>
                                                                                </div>
                                                                            </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endforeach                                                         
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="tab-c-4" role="tabpanel">
                                                        <div class="row">
                                                                @foreach($c4 as $c4)
                                                                    <div class="col-md-6 col-xl-4">
                                                                        <div class="card mb-3 widget-content">
                                                                            <div class="widget-content-outer">
                                                                            <a href="{{ url('admin/rooms/detail/'.$c4->id) }}" class="nav-link" >
                                                                                <div class="widget-content-wrapper">
                                                                                    <div class="widget-content-left">
                                                                                        <div class="widget-numbers mt-0 fsize-3 text-danger">{{ $c4->code }}</div>
                                                                                        <div class="widget-heading">Số giường: {{ $c4->regist_max }}</div>
                                                                                        <div class="widget-subheading">Đã đăng ký: {{ $c4->regist_num }}</div>
                                                                                        <div class="widget-heading">Ghi chú: {{ $c4->message_title }}</div>
                                                                                    </div>
                                                                                    <div class="widget-content-right">
                                                                                        <div class="widget-numbers text-success">{{ $c4->regist_max - $c4->regist_num }}</div>
                                                                                    </div>
                                                                                </div>
                                                                            </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endforeach                                                         
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="tab-c-5" role="tabpanel">
                                                        <div class="row">
                                                                @foreach($c5 as $c5)
                                                                    <div class="col-md-6 col-xl-4">
                                                                        <div class="card mb-3 widget-content">
                                                                            <div class="widget-content-outer">
                                                                            <a href="{{ url('admin/rooms/detail/'.$c5->id) }}" class="nav-link" >
                                                                                <div class="widget-content-wrapper">
                                                                                    <div class="widget-content-left">
                                                                                        <div class="widget-numbers mt-0 fsize-3 text-danger">{{ $c5->code }}</div>
                                                                                        <div class="widget-heading">Số giường: {{ $c5->regist_max }}</div>
                                                                                        <div class="widget-subheading">Đã đăng ký: {{ $c5->regist_num }}</div>
                                                                                        <div class="widget-heading">Ghi chú: {{ $c5->message_title }}</div>
                                                                                    </div>
                                                                                    <div class="widget-content-right">
                                                                                        <div class="widget-numbers text-success">{{ $c5->regist_max - $c5->regist_num }}</div>
                                                                                    </div>
                                                                                </div>
                                                                            </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endforeach                                                         
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @can('admin-position') 
                                            <form>
                                                <div class="d-block text-right card-footer">
                                                    <a href="{{ url('admin/rooms/update')}}" class="btn-wide btn btn-success">Reset về 0</a>
                                                </div>
                                                @csrf
                                            </form>
                                            @endcan
                                        </div>                                                                    
                                </div>
                            </div>
                        </div>
                    </div>

@endsection