@extends('admin.main')
@section('content')
@can('admin-position') 
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading"> 
                        <div class="page-title-icon">
                            <i class="pe-7s-albums icon-gradient bg-premium-dark">
                            </i>
                        </div>                                   
                        <div>{{ $title}}
                            <div class="page-title-subheading">Học kỳ được tạo có thời gian tương ứng với học kỳ sinh viên học tập tại trường.
                            </div>
                        </div>
                    </div>
                    <div class="page-title-actions">                                   
                        <div class="d-inline-block dropdown">
                            <a href="{{ url('/admin/rooms/list') }}"    class="mb-2 mr-2 btn btn-info">                                            
                                Danh sách phòng
                            </a>                                       
                        </div>
                    </div>    
                </div>
            </div>       
            <div class="social-container">
                <span>@include('admin.alert')</span>
            </div>         
            <div class="row">   
                <div class="col-md-12"> 
                    <div class="main-card mb-3 card">
                        <div class="card-body">
                            <h5 class="card-title">Các học kỳ trước</h5>
                                <div class="col-md-6">
                                    <div class="position-relative form-group"> 
                                        <select class="mb-2 form-control"> 
                                            @foreach($semesters as $s)                                                            
                                                <option>Học kỳ {{ $s->name }}: Từ {{ $s->start_date }} đến {{ $s->end_date }}</option>                                                   
                                            @endforeach                                                  
                                        </select>    
                                    </div>
                                </div>
                            <h5 class="card-title">Thêm học kỳ</h5>
                                <form class="needs-validation" method="post" enctype="multipart/form-data">                                                
                                    <div class="position-relative form-group">
                                        <label for="exampleTitle" class="">Tên học kỳ</label>
                                        <input name="name" id="exampleTitle" placeholder="Nhập tên học kỳ" type="text" class="form-control">
                                    </div>  
                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <div class="position-relative form-group">
                                            <label for="exampleStart" class="">Ngày bắt đầu</label>
                                            <input name="start_date" id="exampleStart"  type="date" class="form-control" value="Việt Nam" required="">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="position-relative form-group">
                                            <label for="exampleEnd" class="">Ngày kết thúc</label>
                                            <input name="end_date" id="exampleEnd" type="date" class="form-control" value="Kinh" required="">
                                            </div>
                                        </div>
                                    </div>                         
                                    <div class="position-relative form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" name="status" checked> Chọn làm học kỳ hiện tại.
                                        </label>
                                    </div>                                                                                                       
                                    <button class="mt-1 btn btn-primary">Thêm học kỳ</button>
                                    @csrf
                                </form>
                        </div>
                    </div>                            
                </div>   
            </div>                  
        </div>
@endcan                 
@endsection
