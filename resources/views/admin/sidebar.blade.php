<div class="app-sidebar sidebar-shadow bg-royal sidebar-text-light">
                    <div class="app-header__logo">
                        <div class="logo-src"></div>
                        <div class="header__pane ml-auto">
                            <div>
                                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                                    <span class="hamburger-box">
                                        <span class="hamburger-inner"></span>
                                    </span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="app-header__mobile-menu">
                        <div>
                            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                            </button>
                        </div>
                    </div>
                    <div class="app-header__menu">
                        <span>
                            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                                <span class="btn-icon-wrapper">
                                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                                </span>
                            </button>
                        </span>
                    </div>    
                    <div class="scrollbar-sidebar">
                        <div class="app-sidebar__inner">
                            <ul class="vertical-nav-menu">
                                <li class="app-sidebar__heading">Trang chủ</li>
                                <li>
                                    <a href="{{ url('/admin/main') }}" class="mm-active" >
                                        <i class="metismenu-icon pe-7s-rocket"></i>
                                        Home
                                    </a>
                                </li>
                                <li class="app-sidebar__heading">Quản lý</li>
                                <li>
                                    <a href="#" >
                                        <i class="metismenu-icon pe-7s-user" ></i>
                                        Nhân viên                                       
                                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                    </a>
                                    <ul>
                                        <li>
                                            <a href="{{ url('/admin/employees/list') }}">
                                              Danh sách
                                            </a>
                                        </li>
                                        @can('admin-position')  
                                        <li>
                                            <a href="{{ url('/admin/employees/create') }}">
                                                Thêm mới
                                            </a>
                                        </li>
                                        @endcan
                                    </ul>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="metismenu-icon pe-7s-albums"></i>
                                        Phòng
                                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                    </a>
                                    <ul>
                                        <li>
                                            <a href="{{ url('/admin/rooms/list') }}">
                                                Danh sách phòng
                                            </a>
                                        </li>
                                        @can('admin-position') 
                                        <li>
                                            <a href="{{ url('/admin/semesters/create') }}">
                                                Tạo học kỳ
                                            </a>
                                        </li>
                                        @endcan
                                    </ul>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="metismenu-icon pe-7s-film"></i>
                                        Tin giới thiệu
                                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                    </a>
                                    <ul>
                                        <li>
                                            <a href="{{ url('/admin/introductions/list') }}">
                                                Danh sách tin
                                            </a>
                                        </li>
                                        @can('admin-position') 
                                        <li>
                                            <a href="{{ url('/admin/introductions/create') }}">
                                                Thêm tin
                                            </a>
                                        </li>
                                        @endcan
                                    </ul>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="metismenu-icon pe-7s-id"></i>
                                        Tin thông báo
                                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                    </a>
                                    <ul>
                                        <li>
                                            <a href="{{ url('/admin/notifications/list') }}">
                                                Danh sách tin thông báo
                                            </a>
                                        </li>
                                        @can('admin-position') 
                                        <li>
                                            <a href="{{ url('/admin/notifications/create') }}">
                                                Thêm tin thông báo
                                            </a>
                                        </li>
                                        @endcan
                                    </ul>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="metismenu-icon pe-7s-users"></i>
                                        Sinh viên
                                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                    </a>
                                    <ul>
                                        <li>
                                            <a href="{{ url('/admin/students/list') }}">
                                                Danh sách sinh viên
                                            </a>
                                        </li>
                                        @can('admin-position') 
                                        <li>
                                            <a href="{{ url('/admin/students/create') }}">
                                                Tạo tài khoản
                                            </a>
                                        </li>
                                        @endcan
                                        <li>
                                            <a href="{{ url('/admin/parents/list') }}">
                                                Danh sách thân nhân
                                            </a>
                                        </li>
                                        @can('admin-position')
                                        <li>
                                            <a href="{{ url('/admin/activities/commend') }}">
                                                Danh sách khen thưởng
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ url('/admin/activities/discipline') }}">
                                                Danh sách kỷ luật
                                            </a>
                                        </li>
                                        @endcan
                                    </ul>
                                </li>
                                @can('admin-position')  
                                <li>
                                    <a href="#">
                                        <i class="metismenu-icon pe-7s-note2"></i>
                                        Đơn đăng ký 
                                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                    </a>
                                    <ul>
                                    <li>
                                            <a href="{{ url('/admin/contracts/approve') }}">
                                                Phê duyệt đơn
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ url('/admin/contracts/list') }}">
                                                Đơn đăng ký nội trú
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ url('/admin/semesters/vehicle') }}">
                                                Tạo kì đăng ký xe
                                            </a>
                                        </li>

                                    </ul>
                                </li>
                                @endcan
                                <li>
                                    <a href="#">
                                        <i class="metismenu-icon pe-7s-cash"></i>
                                        Quản lý chi phí
                                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                    </a>
                                    <ul>
                                    <li>
                                            <a href="{{ url('/admin/contracts/fee') }}">
                                                Phí nội trú
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ url('/admin/electricity/list') }}">
                                                Phí điện nước
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ url('/admin/electric-index/list') }}">
                                                Chỉ số điện nước
                                            </a>
                                        </li>

                                    </ul>
                                </li>
                                <li>
                                    <a href="#">                                    
                                        <i class="metismenu-icon pe-7s-graph2"></i>
                                        Thống kê
                                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                    </a>
                                    <ul>
                                        <li>
                                            <a href="{{ url('/admin/reports/detail') }}">
                                                Thống kê đăng ký phòng
                                            </a>
                                        </li>                                        

                                    </ul>
                                </li>
                                
                                
                            </ul>
                        </div>
                    </div>
                </div>
