@extends('admin.main')
@section('content')
@can('admin-position') 
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon">
                            <i class="pe-7s-users icon-gradient bg-premium-dark">
                            </i>
                        </div>
                        <div>{{ $title}}
                            <div class="page-title-subheading">Thông tin tài khoản sinh viên đảm bảo chính xác, xác thực.
                            </div>
                        </div>
                    </div>
                    <div class="page-title-actions">                                    
                        <div class="d-inline-block dropdown">
                            <a href="{{ url('/admin/students/list') }}"    class="mb-2 mr-2 btn btn-info">                                            
                                Danh sách sinh viên
                            </a>                                       
                        </div>
                    </div>   
                </div>
            </div>    
            <div class="social-container">
                <span>@include('admin.alert')</span>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="main-card mb-3 card">
                        <div class="card-body">
                            <h5 class="card-title">Tạo tài khoản sinh viên</h5>
                                    <form class="needs-validation" method="post" enctype="multipart/form-data">
                                        <div class="form-row">
                                        <div class="col-md-6">
                                        <div class="position-relative form-group">
                                            <label for="exampleName" class="">Họ và tên</label>
                                            <input name="name" id="exampleName" placeholder="Nhập họ và tên" type="text" class="form-control" required="">
                                        </div>
                                        </div>
                                        <div class="col-md-6">
                                        <div class="position-relative form-group">
                                            <label for="exampleEmail" class="">Email đăng ký</label>
                                            <input name="email" id="exampleEmail" placeholder="Nhập email" type="email" class="form-control" required="">
                                        </div>
                                        </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-6">
                                                <div class="position-relative form-group">
                                                <label for="exampleNational" class="">Quốc tịch</label>
                                                <input name="national" id="exampleNational" placeholder="Nhập Quốc tịch" type="text" class="form-control" value="Việt Nam" required="">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="position-relative form-group">
                                                <label for="exampleEthnic" class="">Dân tộc</label>
                                                <input name="ethnic" id="exampleEthnic" placeholder="Nhập dân tộc" type="text" class="form-control" value="Kinh" required="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                        <div class="col-md-6">
                                        <div class="position-relative form-group">
                                            <label for="exampleCard" class="">Số CMND/ CCCD</label>
                                            <input name="card_id" id="exampleCard" placeholder="Nhập số CMND" type="text" class="form-control" required="">
                                        </div>
                                        </div>
                                        <div class="col-md-6">
                                        <div class="position-relative form-group">
                                            <label for="examplePhone" class="">Số điện thoại</label>
                                            <input name="phone" id="examplePhone" placeholder="Nhập số điện thoại" type="text" class="form-control" required="">
                                        </div>
                                        </div>
                                        </div>
                                        <div class="form-row">
                                        <div class="col-md-4 mb-3">
                                        <div class="position-relative form-group">
                                            <label for="exampleCourse" class="">Khoá học</label>
                                            <input name="course" id="exampleCourse" placeholder="Nhập khoá học của sinh viên" type="text" class="form-control" required="">
                                        </div>
                                        </div>
                                        <div class="col-md-4 mb-3">
                                        <div class="position-relative form-group">
                                            <label for="exampleDob" class="">Ngày sinh</label>
                                            <input name="dob" id="exampleDob" placeholder="Nhập ngày sinh" type="date" class="form-control" required="">
                                        </div>
                                        </div>
                                        <div class="col-md-4 mb-3">
                                        <div class="position-relative form-group">
                                        <label for="exampleDob" class="">Giới tính</label>
                                        <div class="position-relative form-check">
                                            <label class="form-check-label" for="gender" style="margin-right: 50px;">
                                            <input name="gender" type="radio" class="form-check-input" value="1" id="gender">Nam</label>
                                            <label class="form-check-label">
                                            <input name="gender" type="radio" class="form-check-input" value="0">Nữ</label>
                                        </div>
                                        </div>
                                        </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-4 mb-3">
                                                <div class="position-relative form-group">
                                                    <label for="exampleSelect" class="">Khoa</label>
                                                    <select name="depart" id="exampleSelect" class="form-control">
                                                    @foreach($depart as $d)
                                                    <option value="{{ $d->name }}">{{ $d->name }}</option>
                                                    @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4 mb-3">
                                                <div class="position-relative form-group">
                                                    <label for="exampleCode" class="">MSSV</label>
                                                    <input name="code" id="exampleCode" placeholder="Nhập mã số sinh viên" type="text" class="form-control" required="">
                                                </div>
                                            </div>
                                            <div class="col-md-4 mb-3">
                                                <div class="position-relative form-group">
                                                    <label for="exampleClass" class="">Lớp sinh hoạt</label>
                                                    <input name="class" id="exampleClass" placeholder="Nhập lớp sinh hoạt" type="text" class="form-control" required="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="position-relative form-group">
                                            <label for="exampleSelect" class="">Thân nhân</label>
                                            <select name="relat_id" id="exampleSelect" class="form-control">
                                                <option value="">Chọn thân nhân</option>
                                                @foreach($parents as $p)
                                                <option value="{{ $p->id }}">Họ tên: {{ $p->name }} - Quê quán: {{ $p->address }}</option>
                                                @endforeach
                                            </select>
                                        </div>                                     
                                        
                                        <div class="position-relative form-group">
                                            <label for="exampleAddress" class="">Địa chỉ thường trú</label>
                                            <textarea name="address" id="exampleAddress" class="form-control" required=""></textarea>
                                        </div>
                                        <div class="position-relative form-group"> 
                                            <label for="image" class="">Chọn ảnh thẻ sinh viên</label>
                                            <input class="form-control-file" name="image" type="file" id="image">
                                        </div> 
                                        <div class="position-relative form-group">
                                            <small class="form-text text-muted">Hệ thống sẽ gửi tin xác nhận đến tài khoản email vừa đăng ký với mật khẩu mặc định là <span class="pl-1">"Abc123*"</span> .</small>
                                        </div>
                                        <button class="mt-1 btn btn-primary">Tạo tài khoản</button>
                                        @csrf
                                    </form>
                                </div>
                    </div>
                </div>
            </div>
        </div>
@endcan
@endsection
