@extends('admin.main')
@section('content')
<div class="app-main__inner">
                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    <div class="page-title-icon">
                                        <i class="pe-7s-users icon-gradient bg-premium-dark">
                                        </i>
                                    </div>
                                    <div>{{ $title}}
                                        <div class="page-title-subheading">Thông tin tài khoản sinh viên đảm bảo chính xác, xác thực.
                                        </div>
                                    </div>
                                </div>
                                <div class="page-title-actions">                                    
                                    <div class="d-inline-block dropdown">
                                        <a href="{{ url('/admin/students/list') }}"    class="mb-2 mr-2 btn btn-info">                                            
                                            Danh sách sinh viên
                                        </a>                                       
                                    </div>
                                </div>   
                            </div>
                        </div>                                          
                        
                        <div class="tab-content">
                        <div class="tab-pane tabs-animation fade show active" id="tab-content-0" role="tabpanel">
                                <div class="main-card mb-3 card">
                                    <div class="card-body">
                                        <form class="">
                                            <div class="form-row">
                                                <div class="col-md-4 mb-3">
                                                <img src="/images/students/{{ $student->image }}" class="rounded-circle" width="150"/></br></br>                                             
                                                </div>                                               
                                                <div class="col-md-4 mb-3">
                                                <h5 class="card-title">Thông tin cá nhân sinh viên</h5>
                                                <p class="text-primary">Họ tên: {{ $student->name }}</p>
                                                <p class="text-primary">Mã sinh viên: {{ $student->code }}</p>
                                                <p class="text-primary">Khoá: {{ $student->course }}</p>
                                                <p class="text-primary">Khoa: {{ $student->depart }}</p>
                                                <p class="text-primary">Lớp sinh hoạt: {{ $student->class }}</p>                                                                                                   
                                                <p class="text-primary">Email: {{ $student->email }}</p>
                                                <p class="text-primary">Quốc tịch: {{ $student->national }}</p>
                                                <p class="text-primary">Dân tộc: {{ $student->ethnic }}</p>
                                                @if( $student->gender == 0)
                                                <p class="text-primary">Giới tính: Nữ</p>
                                                @else 
                                                <p class="text-primary">Giới tính: Nam</p>
                                                @endif
                                                <p class="text-primary">Số điện thoại: {{ $student->phone }}</p>
                                                <p class="text-primary">Số CMND/CCCD: {{ $student->card_id }}</p>
                                                <p class="text-primary">Họ tên thân nhân: {{ $parent->name }} || Mã: {{ $parent->id }} </p>
                                                <p class="text-primary">Số điện thoại thân nhân: {{ $parent->phone }}</p>
                                                <p class="text-primary">Địa chỉ thường trú: {{ $student->address }}</p>                                                
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                <h5 class="card-title">Thông tin tài khoản ngân hàng</h5>
                                                <p class="text-danger">Số tài khoản ATM: {{ $student->account_number }}</p>
                                                <p class="text-danger">Ngân hàng thụ hưởng: {{ $student->bank_name }}</p>
                                                </div>
                                            </div>                                            
                                        </form>
                                    </div>
                                </div>
                               
                    </div>
@endsection