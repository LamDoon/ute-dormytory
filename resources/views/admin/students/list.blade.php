@extends('admin.main')
@section('content')

                    <div class="app-main__inner">
                        <div class="app-page-title">
                            <form method="get">
                                <div class="page-title-wrapper">
                                    <div class="app-header__content">                                    
                                        <div class="app-header-left">
                                            <div class="search-wrapper active">
                                                <div class="input-holder">
                                                    <input type="text" class="search-input" placeholder="Type to search" name="key_search">
                                                    <button class="search-icon"><span></span></button>
                                                </div>
                                                <button type="button" class="close"></button>
                                            </div>                                         
                                        </div>
                                    </div>
                                    
                                    <div class="page-title-actions">                                       
                                        <div class="mb-2 mr-2 btn-group">
                                            <button class="btn btn-focus"> <i class="pe-7s-filter"></i></button>
                                            <select name="filter" id="exampleSelect" class="dropdown-toggle-split dropdown-toggle btn btn-focus">                                                       
                                                    <option value="">--Choose an option--</option>
                                                    <option value="code">Mã số sinh viên</option>
                                                    <option value="name">Tên sinh viên</option>
                                                    <option value="course">Khoá học</option>                                                       
                                                </select>                                            
                                        </div>                                                                           
                                    </div>   
                                </div>
                            </form>
                        </div>  
                        @if(count($students)>0)          
                        <div class="row">   
                            <div class="main-card mb-3 card">
                                    <div class="card-body"><h5 class="card-title">Danh sách sinh viên đang lưu trú tại KTX</h5>
                                        <div class="table-responsive">
                                            <table class="mb-0 table" style="font-size: 14px !important;">
                                                <thead>
                                                <tr>                                                   
                                                    <th>MSSV</th>
                                                    <th>Hình ảnh</th>
                                                    <th>Họ tên</th>
                                                    <th>Email</th>                                                    
                                                    <th>Số điện thoại</th>
                                                    <th>Giới tính</th>
                                                    <th>Khoá học</th>
                                                    <th>Khoa</th>
                                                    <th>Lớp</th>
                                                    <th></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($students as $student)
                                                <tr id="s{{$student->id}}">
                                                    <th scope="row">{{ $student->code }}</th>
                                                    <td><img src="/images/students/{{ $student->image }}" class="rounded-circle" width="40"/></td>
                                                    <td>{{ $student->name }}</td>
                                                    <td>{{ $student->email }}</td>                                                    
                                                    <td>{{ $student->phone }}</td>
                                                    @if ($student->gender ==0)
                                                        <td>Nữ</td>                                                    
                                                    @else
                                                        <td>Nam</td>                                                    
                                                    @endif
                                                    <td>{{ $student->course }}</td>
                                                    <td>{{ $student->depart }}</td>
                                                    <td>{{ $student->class }}</td>                                                      
                                                    @can('admin-position')                                  
                                                    <td>
                                                    <div class="page-title-actions">                                    
                                    <div class="d-inline-block dropdown">
                                        <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="mb-2 mr-2 border-0 btn-transition btn btn-outline-dark">                                            
                                                <i class="fa fa-ellipsis-v fa-w-6"></i>
                                        </button>
                                        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                                            <ul class="nav flex-column">
                                                <li class="nav-item">
                                                    <a href="{{ url('admin/students/detail/'.$student->id) }}" class="nav-link">
                                                        <i class="nav-link-icon lnr-inbox"></i>
                                                        <span>
                                                            Xem chi tiết
                                                        </span>                                                       
                                                    </a>
                                                </li>
                                               
                                                <li class="nav-item">
                                                    <a href="{{ url('admin/students/active/'.$student->id) }}" class="nav-link">
                                                        <i class="nav-link-icon lnr-book"></i>
                                                        <span>
                                                            Vô hiệu hoá tài khoản
                                                        </span>                                                       
                                                    </a>
                                                </li> 
                                                @if ($student->vehicle == null)
                                                <li class="nav-item">
                                                    <a href="{{ url('admin/students/vehicle/'.$student->id) }}" class="nav-link">
                                                        <i class="nav-link-icon lnr-book"></i>
                                                        <span>
                                                            Thêm đăng ký xe
                                                        </span>                                                       
                                                    </a>
                                                </li>    
                                                @endif   
                                                <li class="nav-item">
                                                    <a href="javascript:void(0)" onclick="deleteStudent({{$student->id}})"class="nav-link">
                                                        <i class="nav-link-icon lnr-book"></i>
                                                        <span>
                                                            Xoá khỏi danh sách
                                                        </span>                                                       
                                                    </a>
                                                </li>                            
                                               
                                            </ul>
                                        </div>
                                    </div>
                                </div>   
                                                    </td>
                                                    @endcan
                                                </tr> 
                                                @endforeach                                               
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>                     
                    </div>
                    @else 
                    <div class="list-group-item-warning list-group-item">Không tìm thấy sinh viên</div>
                    @endif
                    {{ $students->links() }}
@endsection

<script>
    function deleteStudent(id){       
        if(confirm("Bạn có chắc chắn muốn xoá thông tin sinh viên?")){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url:'/admin/students/delete/'+id,                
                type:'GET',
                 data:{
                     _token : $("input[name=_token]").val()
                 },
                success:function(response){
                  $('#s'+id).remove();                  
                }
            });
        }
    }
</script>