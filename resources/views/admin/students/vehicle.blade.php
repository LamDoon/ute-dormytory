@extends('admin.main')
@section('content')

                    <div class="app-main__inner">
                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    <div class="page-title-icon">
                                        <i class="pe-7s-users icon-gradient bg-premium-dark">
                                        </i>
                                    </div>
                                    <div>{{ $title}}
                                        <div class="page-title-subheading">Wide selection of forms controls, using the Bootstrap 4 code base, but built with React.
                                        </div>
                                    </div>
                                </div>
                                <div class="page-title-actions">                                    
                                    <div class="d-inline-block dropdown">
                                        <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="mb-2 mr-2 btn btn-info">
                                            
                                            Buttons
                                        </button>                                        
                                    </div>
                                </div>   
                             </div>
                        </div>    
                        <div class="social-container">
			<span>@include('admin.alert')</span>
			</div>         
                        <div class="row">   
                            <div class="col-md-12"> 
                            <div class="main-card mb-3 card">
                            <div class="card-body">
                                <h5 class="card-title">Thêm thông tin đăng ký xe</h5>
                                                <form class="needs-validation" method="post" enctype="multipart/form-data">                                                
                                                    <div class="position-relative form-group">
                                                        <label for="name" class="">Sinh viên</label>
                                                        <input id="name" placeholder="" type="text" class="form-control" value="{{$student->name}}" readonly>
                                                    </div>   
                                                    <div class="position-relative form-group">
                                                        <label for="code" class="">MSSV</label>
                                                        <input id="code" placeholder="" type="text" class="form-control" value="{{$student->code}}" readonly>
                                                    </div>                         
                                                    <div class="position-relative form-group">
                                                        <label for="vehicle" class="">Biển số xe</label>
                                                        <input id="vehicle" name="vehicle" placeholder="Nhập biển số xe" type="text" class="form-control" required>
                                                    </div>                                                                                                        
                                                    <button class="mt-1 btn btn-primary">Thêm</button>
                                                    @csrf
                                                </form>
                                            </div>
                                </div>                            
                            </div>   
                            </div>                  
                    </div>
                 
@endsection
