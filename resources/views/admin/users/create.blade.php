@extends('admin.main')
@section('content')
@can('admin-position')  
                    <div class="app-main__inner">
                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    <div class="page-title-icon">
                                        <i class="pe-7s-user icon-gradient bg-premium-dark">
                                        </i>
                                    </div>
                                    <div>{{ $title}}
                                        <div class="page-title-subheading">Thông tin nhân viên phải đảm bảo chính xác để thuận lợi cho việc liên lạc.
                                        </div>
                                    </div>
                                </div>
                                <div class="page-title-actions">                                    
                                    <div class="d-inline-block dropdown">
                                        <a href="{{ url('/admin/employees/list') }}"    class="mb-2 mr-2 btn btn-info">                                            
                                            Danh sách nhân viên
                                        </a>                                       
                                    </div>
                                </div>   
                             </div>
                        </div>    
                        <div class="social-container">
                            <span>@include('admin.alert')</span>
                        </div>         
                        <div class="row">   
                            <div class="col-md-12"> 
                                <div class="main-card mb-3 card">
                                    <div class="card-body">
                                        <h5 class="card-title">Tạo tài khoản nhân viên</h5>
                                        <form class="needs-validation" method="post">
                                            <div class="form-row">
                                                <div class="col-md-6">
                                                    <div class="position-relative form-group">
                                                        <label for="exampleName" class="">Họ và tên</label>
                                                        <input name="name" id="exampleName" placeholder="Nhập họ và tên" type="text" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="position-relative form-group">
                                                        <label for="exampleEmail" class="">Email</label>
                                                        <input name="email" id="exampleEmail" placeholder="Nhập email" type="email" class="form-control">
                                                    </div> 
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-md-6">                                                  
                                                    <div class="position-relative form-group">
                                                        <label for="exampleCard" class="">Số CMND</label>
                                                        <input name="card_id" id="exampleCard" placeholder="Nhập số CMND" type="text" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="position-relative form-group">
                                                        <label for="examplePhone" class="">Số điện thoại</label>
                                                        <input name="phone" id="examplePhone" placeholder="Nhập số điện thoại" type="text" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-md-6">
                                                    <div class="position-relative form-group">
                                                        <label for="exampleSelect" class="">Chức vụ</label>
                                                        <select name="position_id" id="exampleSelect" class="form-control">
                                                            @foreach($positions as $p)
                                                            <option value="{{ $p->id }}">{{ $p->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>  
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="position-relative form-group">
                                                        <label for="exampleDob" class="">Giới tính</label>
                                                            <div class="position-relative form-check">
                                                                <label class="form-check-label" style="margin-right: 50px;">
                                                                    <input name="gender" type="radio" class="form-check-input" value="1">Nam</label>                                                    
                                                                <label class="form-check-label">
                                                                    <input name="gender" type="radio" class="form-check-input" value="0">Nữ</label>                                                    
                                                            </div>
                                                    </div>
                                                </div>
                                            </div>                                                                                          
                                            <div class="position-relative form-group">
                                                <label for="exampleAddress" class="">Địa chỉ</label>
                                                <textarea name="address" id="exampleAddress" class="form-control"></textarea>
                                            </div>                                        
                                            <div class="form-row">                                                    
                                                <div class="position-relative form-group">                                                        
                                                    <small class="form-text text-muted">Mật khẩu mặc định cho tài khoản của nhân viên là "Abc123*".</small>
                                                </div>
                                            </div>
                                            <button class="mt-1 btn btn-primary">Thêm nhân viên</button>
                                            @csrf
                                        </form>
                                    </div>
                                </div>                            
                            </div>   
                        </div>                  
                    </div>
@endcan                
@endsection
