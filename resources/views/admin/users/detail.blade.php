@extends('admin.main')
@section('content')
<div class="app-main__inner">
                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    <div class="page-title-icon">
                                        <i class="pe-7s-user icon-gradient bg-premium-dark">
                                        </i>
                                    </div>
                                    <div>{{ $title}}
                                        <div class="page-title-subheading">Thông tin nhân viên đang làm việc trong ký túc xá để thuận lợi cho việc liên lạc.
                                        </div>
                                    </div>
                                </div>
                                <div class="page-title-actions">                                    
                                    <div class="d-inline-block dropdown">
                                        <a href="{{ url('/admin/employees/list') }}"    class="mb-2 mr-2 btn btn-info">                                            
                                            Danh sách nhân viên
                                        </a>                                         
                                    </div>
                                </div>   
                             </div>
                        </div>                                        
                        
                        <div class="tab-content">
                        <div class="tab-pane tabs-animation fade show active" id="tab-content-0" role="tabpanel">
                                <div class="main-card mb-3 card">
                                    <div class="card-body">
                                        <form class="">
                                            <div class="form-row">
                                                <div class="col-md-4 mb-3">
                                                <img src="/images/{{ $user->image }}" class="rounded-circle" width="150"/></br></br>
                                            @foreach($positions as $p)
                                                        @if ($user->position_id == $p->id)
                                                            <h5 class="card-title">{{ $p->name }}</h5>                                                        
                                                        @endif                                                       
                                            @endforeach   
                                                </div>                                               
                                                <div class="col-md-4 mb-3">
                                                <h5 class="card-title">Thông tin cá nhân nhân viên</h5>
                                                <p class="text-primary">Họ tên: {{ $user->name }}</p>
                                                <p class="text-primary">Mã nhân viên: {{ $user->id }}</p>
                                                @foreach($positions as $p)
                                                        @if ($user->position_id == $p->id)
                                                            <p class="text-primary">Chức vụ: {{ $p->name }}</p>                                                        
                                                        @endif                                                       
                                                @endforeach                                                
                                                <p class="text-primary">Email: {{ $user->email }}</p>
                                                @if( $user->gender == 0)
                                                <p class="text-primary">Giới tính: Nữ</p>
                                                @else 
                                                <p class="text-primary">Giới tính: Nam</p>
                                                @endif
                                                <p class="text-primary">Số điện thoại: {{ $user->phone }}</p>
                                                <p class="text-primary">Số CMND/CCCD: {{ $user->card_id }}</p>
                                                <p class="text-primary">Địa chỉ thường trú: {{ $user->address }}</p>                                                
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                <h5 class="card-title">Thông tin tài khoản ngân hàng</h5>
                                                <p class="text-danger">Số tài khoản ATM: {{ $user->account_number }}</p>
                                                <p class="text-danger">Ngân hàng thụ hưởng: {{ $user->bank_name }}</p>
                                                </div>
                                            </div>                                            
                                        </form>
                                    </div>
                                </div>
                               
                    </div>
@endsection