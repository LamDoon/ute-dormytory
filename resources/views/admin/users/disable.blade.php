@extends('admin.main')
@section('content')

<div class="app-main__inner">
                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    <div class="page-title-icon">
                                        <i class="pe-7s-user icon-gradient bg-premium-dark">
                                        </i>
                                    </div>
                                    <div>{{ $title}}
                                        <div class="page-title-subheading">Tài khoản nhân viên bị vô hiệu hoá không thể thực hiện nghiệp vụ hệ thống.
                                        </div>
                                    </div>
                                </div>
                                <div class="page-title-actions">                                    
                                    <div class="d-inline-block dropdown">
                                        <a href="{{ url('/admin/employees/list') }}"    class="mb-2 mr-2 btn btn-info">                                            
                                            Danh sách nhân viên
                                        </a>                                       
                                    </div>
                                </div>   
                             </div>
                        </div> 
                        <div class="social-container">
			                <span>@include('admin.alert')</span>
			            </div>           
                        <ul class="body-tabs body-tabs-layout tabs-animated body-tabs-animated nav">
                            <li class="nav-item">
                                <a role="tab" class="nav-link active" id="tab-0" data-toggle="tab" href="#tab-content-0">
                                    <span>Vô hiệu hoá tài khoản</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a role="tab" class="nav-link" id="tab-1" data-toggle="tab" href="#tab-content-1">
                                    <span>Danh sách tài khoản bị vô hiệu hoá</span>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane tabs-animation fade show active" id="tab-content-0" role="tabpanel">
                                <div class="row">                            
                                    <div class="col-md-6">                               
                                        <div class="main-card mb-3 card">
                                            <div class="card-body">
                                            <img src="/images/{{ $user->image }}" class="rounded-circle" width="90"/></br>                                            
                                                <h5 class="card-title">Thông tin tài khoản nhân viên</h5>
                                                <p>Họ tên: {{ $user->name }}</p>                                                
                                                @foreach($positions as $p)
                                                        @if ($user->position_id == $p->id)
                                                            <p>Chức vụ: {{ $p->name }}</p>                                                        
                                                        @endif                                                       
                                                @endforeach                                                
                                                <p>Email: {{ $user->email }}</p>  
                                                @if ($user->status == 1)  
                                                <div class="mb-2 mr-2 badge badge-success">Đang hoạt động</div>
                                                @else 
                                                <div class="mb-2 mr-2 badge badge-danger">Tài khoản bị vô hiệu hoá</div>  
                                                @endif                                          
                                    </div> 
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="main-card mb-3 card">
                                        @if ($user->status == 1)
                                        <div class="card-body">
                                            <div class="alert alert-danger fade show">
                                                Lưu ý: Khi bạn vô hiệu hoá tài khoản. Nhân viên sẽ không thể truy cập vào hệ thống để thực hiện nghiệp vụ. </br>
                                                Bạn có muốn tiếp tục vô hiệu hoá tài khoản nhân viên <a href="{{ url('admin/employees/detail/'.$user->id) }}" class="alert-link"> {{$user->name }}</a> không?
                                            <form method="post">
                                                <a href="" class="mb-2 mr-2 border-0 btn-transition btn btn-outline-info">Quay lại</a>
                                                <button class="mb-2 mr-2 border-0 btn-transition btn btn-outline-danger">Tiếp tục</button>
                                            @csrf
                                            </form>
                                            </div>                                          

                                        </div>
                                        @else
                                        <div class="card-body">
                                        <div class="card mb-3 widget-content bg-midnight-bloom">
                                        <div class="widget-content-wrapper text-white">
                                                <div class="widget-content-left">
                                                    <div class="widget-heading">Bạn đã vô hiệu hoá tài khoản {{$user->name }}</div>
                                                    <div class="widget-subheading">Hãy kích hoạt tài khoản!</div>
                                                </div>
                                                <div class="widget-content-right">
                                                    <div class="widget-numbers text-white">
                                                        <form method="post">
                                                        <button class="mb-2 mr-2 btn btn-success">Kích hoạt</button>
                                                        @csrf
                                                        </form>                                                    
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="alert alert-danger fade show">
                                                Lưu ý: Khi bạn vô hiệu hoá tài khoản. Nhân viên sẽ không thể truy cập vào hệ thống để thực hiện nghiệp vụ. </br></div>
                                    </div> 
                                    @endif
                                    </div>
                                </div>
                            </div>                                                           
                            </div>
                            <div class="tab-pane tabs-animation fade" id="tab-content-1" role="tabpanel">
                                <div class="main-card mb-3 card">
                                    <div class="card-body">
                                    <div class="table-responsive">
                                            <table class="mb-0 table">
                                                <thead>
                                                <tr>                                                   
                                                    <th>Mã</th>
                                                    <th>Hình ảnh</th>
                                                    <th>Họ tên</th>
                                                    <th>Email</th>
                                                    <th>Chức vụ</th>
                                                    <th>Tình trạng</th>
                                                    <th></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($userdisable as $user)
                                                <tr>
                                                    <th scope="row">{{ $user->id }}</th>
                                                    <td><img src="/images/{{ $user->image }}" class="rounded-circle" width="40"/></td>
                                                    <td>{{ $user->name }}</td>
                                                    <td>{{ $user->email }}</td>                                                   
                                                    @foreach($positions as $p)
                                                        @if ($user->position_id == $p->id)
                                                            <td>{{ $p->name }}</td>                                                        
                                                        @endif                                                       
                                                    @endforeach                                   
                                                    
                                                    <td>
                                                        <div class="mb-2 mr-2 badge badge-pill badge-alternate">Vô hiệu hoá</div>
                                                    </td>
                                                    <td>
                                                    <div class="page-title-actions">                                    
                                    <div class="d-inline-block dropdown">
                                    <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="mb-2 mr-2 border-0 btn-transition btn btn-outline-dark">                                            
                                                <i class="fa fa-ellipsis-v fa-w-6"></i>
                                        </button>
                                        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                                            <ul class="nav flex-column">
                                                <li class="nav-item">
                                                    <a href="{{ url('admin/employees/detail/'.$user->id) }}" class="nav-link">
                                                        <i class="nav-link-icon lnr-inbox"></i>
                                                        <span>
                                                            Xem chi tiết
                                                        </span>                                                       
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="{{ url('admin/employees/active/'.$user->id) }}" class="nav-link">
                                                        <i class="nav-link-icon lnr-book"></i>
                                                        <span>
                                                            Active tài khoản
                                                        </span>                                                       
                                                    </a>
                                                </li>                               
                                               
                                            </ul>
                                        </div>
                                    </div>
                                </div>   
                                                    </td>
                                                </tr> 
                                                @endforeach                                               
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

@endsection