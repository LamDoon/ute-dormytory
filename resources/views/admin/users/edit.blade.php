@extends('admin.main')
@section('content')
<div class="app-main__inner">
<div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    <div class="page-title-icon">
                                        <i class="pe-7s-user icon-gradient bg-premium-dark">
                                        </i>
                                    </div>
                                    <div>{{ $title}}
                                        <div class="page-title-subheading">Nhân viên cập nhật chính xác thông tin cá nhân để thuận tiệN trong việc liên lạc.
                                        </div>
                                    </div>
                                </div>
                                <div class="page-title-actions">                                    
                                    <div class="d-inline-block dropdown">
                                        <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="mb-2 mr-2 btn btn-info">
                                            
                                            Buttons
                                        </button>                                        
                                    </div>
                                </div>   
                             </div>
                        </div>                     
                        <div class="social-container">
			                <span>@include('admin.alert')</span>
			            </div>
                        <div class="tab-content">
                            <div class="tab-pane tabs-animation fade show active" id="tab-content-0" role="tabpanel">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="main-card mb-3 card">
                                            <div class="card-body"><h5 class="card-title">Thông tin cá nhân</h5>
                                            <form class="" method="post" enctype="multipart/form-data">
                                                    <div class="position-relative form-group">
                                                        <img src="/images/{{ $user->image }}" class="rounded-circle" width="80"/>                                                        
                                                    </div>
                                                    <div class="position-relative form-group"> 
                                                    <label for="image" class="">Chọn file ảnh</label>
                                                        <input class="form-control-file" name="image" type="file" id="image">
                                                    </div>
                                                    <div class="position-relative form-group">
                                                        <label for="name" class="">Họ tên</label>
                                                        <input name="name" id="name" placeholder="" type="text" class="form-control" value="{{ $user->name }}">
                                                    </div>
                                                    <div class="position-relative form-group">
                                                        <label for="email" class="">Email</label>
                                                        <input name="email" id="email" placeholder="" type="email" class="form-control" value="{{ $user->email }}">
                                                    </div>
                                                    <div class="position-relative form-group">
                                                        <label for="password" class="">Password</label>
                                                        <input name="password" id="password" placeholder="" type="password" class="form-control" value="{{ $user->password }}">
                                                    </div>
                                                    <div class="position-relative form-group">
                                                    <div class="position-relative form-check">
                                                        <label class="form-check-label">
                                                            <input name="gender" type="radio" class="form-check-input" value="1" {{$user->gender ==1? 'checked=""' :''}} >Nam</label>
                                                    </div>                                                    
                                                    <div class="position-relative form-check">
                                                        <label class="form-check-label">
                                                            <input name="gender" type="radio" class="form-check-input" value="0" {{$user->gender ==0? 'checked=""' :''}}>Nữ</label>
                                                    </div> 
                                                    </div>                                                    
                                                    <div class="position-relative form-group">
                                                        <label for="card_id" class="">Số CMND/ CCCD</label>
                                                        <input name="card_id" id="card_id" placeholder="" type="text" class="form-control" value="{{ $user->card_id }}">
                                                    </div>
                                                    <div class="position-relative form-group">
                                                        <label for="phone" class="">Số điện thoại</label>
                                                        <input name="phone" id="phone" placeholder="" type="text" class="form-control" value="{{ $user->phone }}">
                                                    </div>                                                    
                                                    <div class="position-relative form-group">
                                                        <label for="address" class="">Địa chỉ</label>
                                                        <textarea name="address" id="address" class="form-control">{{ $user->address }}</textarea>
                                                    </div>
                                                   
                                                    <button class="mt-1 btn btn-primary">Lưu cập nhật</button>
                                                    @csrf
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="main-card mb-3 card">
                                            <div class="card-body"><h5 class="card-title">Thông tin tài khoản ngân hàng</h5>
                                            <form class="" method="post" enctype="multipart/form-data" action="atm/{{$user->id}}">
                                                    <div class="position-relative form-group">
                                                        <label for="account_number" class="">Số tài khoản</label>
                                                        <input name="account_number" id="account_number" placeholder="" type="text" class="form-control" value="{{ $user->account_number }}">
                                                    </div>
                                                    <div class="position-relative form-group">
                                                        <label for="bank_name" class="">Ngân hàng thụ hưởng</label>
                                                        <input name="bank_name" id="bank_name" placeholder="" type="text" class="form-control" value="{{ $user->bank_name }}">
                                                    </div>                       
                                                    <button class="mt-1 btn btn-primary">Lưu cập nhật</button>
                                                    
                                                    @csrf
                                                </form>
                                            </div>
                                        </div>                                        
                                    </div>
                                </div>
                            </div>
                           
                        </div>
                    </div>

@endsection