@extends('admin.main')
@section('content')

                    <div class="app-main__inner">
                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    <div class="page-title-icon">
                                        <i class="pe-7s-user icon-gradient bg-premium-dark">
                                        </i>
                                    </div>
                                    <div>{{ $title}}
                                        <div class="page-title-subheading">Thông tin nhân viên đang làm việc trong ký túc xá để thuận lợi cho việc liên lạc.
                                        </div>
                                    </div>
                                </div>
                                <div class="page-title-actions">                                    
                                    <div class="d-inline-block dropdown">
                                        <a href="{{ url('/admin/employees/create') }}"    class="mb-2 mr-2 btn btn-info">                                            
                                            Thêm nhân viên
                                        </a>                                      
                                    </div>
                                </div>   
                             </div>
                        </div> 
                        @if(count($users)>0)           
                         
                            <div class="main-card mb-3 card">
                                    <div class="card-body"><h5 class="card-title">Danh sách nhân viên</h5>
                                        <div class="table-responsive">
                                            <table class="mb-0 table">
                                                <thead>
                                                <tr>                                                   
                                                    <th>Mã</th>
                                                    <th>Hình ảnh</th>
                                                    <th>Họ tên</th>
                                                    <th>Email</th>                                                    
                                                    <th>Số điện thoại</th>
                                                    <th>Giới tính</th>
                                                    <th>Chức vụ</th>
                                                    <th></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($users as $user)
                                                <tr>
                                                    <th scope="row">{{ $user->id }}</th>
                                                    <td><img src="/images/{{ $user->image }}" class="rounded-circle" width="40"/></td>
                                                    <td>{{ $user->name }}</td>
                                                    <td>{{ $user->email }}</td>                                                    
                                                    <td>{{ $user->phone }}</td>
                                                    @if ($user->gender ==0)
                                                        <td>Nữ</td>                                                    
                                                    @else
                                                        <td>Nam</td>                                                    
                                                    @endif
                                                    @foreach($positions as $p)
                                                        @if ($user->position_id == $p->id)
                                                            <td>{{ $p->name }}</td>                                                        
                                                        @endif                                                       
                                                    @endforeach   
                                                    @can('admin-position')                                  
                                                    <td>
                                                    <div class="page-title-actions">                                    
                                    <div class="d-inline-block dropdown">
                                    <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="mb-2 mr-2 border-0 btn-transition btn btn-outline-dark">                                            
                                                <i class="fa fa-ellipsis-v fa-w-6"></i>
                                        </button>
                                        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                                            <ul class="nav flex-column">
                                                <li class="nav-item">
                                                    <a href="{{ url('admin/employees/detail/'.$user->id) }}" class="nav-link">
                                                        <i class="nav-link-icon lnr-inbox"></i>
                                                        <span>
                                                            Xem chi tiết
                                                        </span>                                                       
                                                    </a>
                                                </li>
                                               
                                                <li class="nav-item">
                                                    <a href="{{ url('admin/employees/active/'.$user->id) }}" class="nav-link">
                                                        <i class="nav-link-icon lnr-book"></i>
                                                        <span>
                                                            Vô hiệu hoá tài khoản
                                                        </span>                                                       
                                                    </a>
                                                </li>                               
                                               
                                            </ul>
                                        </div>
                                    </div>
                                </div>   
                                                    </td>
                                                    @endcan
                                                </tr> 
                                                @endforeach                                               
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>                     
                  
                    @else 
                    <div class="list-group-item-warning list-group-item">Không tìm thấy nhân viên</div>
                    @endif
                    {{ $users->links() }}         
@endsection