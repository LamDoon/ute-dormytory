@extends('admin.main')
@section('content')

        <div class="app-main__inner">
                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading"> 
                                    <div class="page-title-icon">
                                        <i class="pe-7s-bicycle icon-gradient bg-premium-dark">
                                        </i>
                                    </div>                                   
                                    <div>{{ $title}}
                                        <div class="page-title-subheading">Tabs are used to split content between multiple sections. Wide variety available.
                                        </div>
                                    </div>
                                </div>
                                <div class="page-title-actions">                                   
                                    <div class="d-inline-block dropdown">
                                        <a href="{{ url('/admin/vehicles/list') }}"    class="mb-2 mr-2 btn btn-info">                                            
                                            Danh sách đăng ký xe
                                        </a>                                       
                                    </div>
                                </div>    
                            </div>
                        </div>       
                        <div class="social-container">
                            <span>@include('admin.alert')</span>
                        </div>         
                        <div class="row">   
                            <div class="col-md-12"> 
                                <div class="main-card mb-3 card">
                                    <div class="card-body">
                                        <h5 class="card-title">Tạo đăng ký xe học kì {{$semester->name}}</h5> 
                                        @if ($semester->veh_max != null)     
                                        <p>Học kì {{$semester->name}} đã được tạo với {{$semester->veh_max}} vé gửi xe. Nếu bạn muốn cập nhật, hãy nhập số mới.</p>  
                                        @endif                                      
                                            <form class="needs-validation" action="vehicle/{{$semester->id}}"  enctype="multipart/form-data" method="post">                                                
                                                <div class="position-relative form-group">
                                                    <label for="exampleTitle" class="">Nhập số vé</label>
                                                    <input name="veh_max" id="exampleTitle" placeholder="" type="number" class="form-control" required>
                                                </div>                                                                                            
                                                <button class="mt-1 btn btn-primary">Tạo đăng ký</button>
                                                @csrf
                                            </form>
                                    </div>
                                </div>                            
                            </div>   
                        </div>                  
        </div>
                 
@endsection
