<h3>Xin chào sinh viên {{ $email_data['name'] }}</h3>

Bạn đã đăng ký thành công vé gửi xe tại ký túc xá trường Đại học Sư phạm Kỹ thuật - ĐHĐN học kì {{ $email_data['semester'] }}
cho xe hiệu số {{ $email_data['vehicle'] }}
<br><br>
Hãy đưa mail này cho ban quản lý để lấy vé xe của bạn. 
</br>
<p>------------------------------------------------------</p>

Mr. Le Van Anh</br>

University of Technology and Education dormitory</br>

University of Technology and Education</br>

The University of Danang</br>

Address: 48 Cao Thang, Da Nang, Viet Nam</br>

Tel.: (+84) 789 451 245, (+84) 904 512 032</br>

E-mail: UTEdomytory@ute.udn.vn, UTEdomytory@gmail.com 