@extends('admin.main')
@section('content')

                    <div class="app-main__inner">
                    <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading"> 
                                    <div class="page-title-icon">
                                        <i class="pe-7s-bicycle icon-gradient bg-premium-dark">
                                        </i>
                                    </div>                                   
                                    <div>{{ $title}}
                                        <div class="page-title-subheading">Tabs are used to split content between multiple sections. Wide variety available.
                                        </div>
                                    </div>
                                </div>
                                <div class="page-title-actions">                                   
                                    <div class="d-inline-block dropdown">
                                        <a href="{{ url('/admin/contracts/list') }}"    class="mb-2 mr-2 btn btn-info">                                            
                                            Thêm tin
                                        </a>                                       
                                    </div>
                                </div>    
                            </div>
                        </div>      
                        <div class="social-container">
			                <span>@include('admin.alert')</span>
			            </div>                                
                         
                        <div class="main-card mb-3 card">
                            @foreach($forms as $form)
                                <div class="no-gutters row" id="v{{$form->id}}">
                                    <div class="col-md-6">
                                        <div class="widget-content">
                                            <div class="widget-content-wrapper">                                                
                                                <div class="widget-content-left">
                                                    <div class="widget-heading">Sinh viên: {{ $form->name }}</div>
                                                    <div class="widget-subheading">MSSV: {{ $form->student_code }}</div>
                                                </div>
                                                <div class="widget-content-right">
                                                    <div class="widget-subheading">Hiệu số xe: {{ $form->vehicle }}</div>                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                   
                                    <div class="col-md-6">
                                        <div class="widget-content">
                                            <div class="widget-content-wrapper">                                                
                                                <div class="widget-content-left">
                                                   Học kỳ {{ $form->semester_name }}
                                                </div>
                                                <div class="widget-content-right">
                                                    <a href="javascript:void(0)" onclick="deleteVehicle({{$form->id}})"
                                                    class="mb-2 mr-2 btn btn-danger"><i class="pe-7s-trash"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            {{ $forms->links() }}
          
                            
@endsection

<script>
    function deleteVehicle(id){       
        if(confirm("Bạn có chắc chắn muốn xoá phiếu đăng ký gửi xe?")){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url:'/admin/vehicles/delete/'+id,                
                type:'GET',
                 data:{
                     _token : $("input[name=_token]").val()
                 },
                success:function(response){
                  $('#v'+id).remove();                  
                }
            });
        }
    }
</script>