
<!DOCTYPE html>
<html lang="en">
  <head>
      @include('index.head')      
</head>
    
    <body>
    
    <!-- ***** Preloader Start ***** -->
    <div id="preloader">
        <div class="jumper">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>  
    <!-- ***** Preloader End ***** -->
    
    
    <!-- ***** Header Area Start ***** -->
    <header class="header-area header-sticky">
        @include('index.navbar')
    </header>
    <!-- ***** Header Area End ***** -->

    <!-- ***** Main Banner Area Start ***** -->
    @if (session()->has('student')) 
    <!-- ***** Main Banner Area End ***** -->

    <!-- ***** History Registration Area Starts ***** -->
    <section class="section" id="history">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 offset-lg-4 text-center">
                    <div class="section-heading">                        
                        <h2>Lịch sử đăng ký phòng</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="row" id="tabs">                       
                        <div class="col-lg-12">
                            <section class='tabs-content' >
                                <article id='tabs-a'>
                                    <div class="row">
                                        @foreach($contracts as $contract)
                                        <div class="col-lg-12">                                            
                                           <div class="tab-item">   
                                                <h5>Sinh viên: {{ $contract->name}}</h5>                                             
                                                <h4>Phòng đăng ký: {{ $contract->room_code}}</h4>
                                                <p>Loại phòng: {{ $contract->room_type}} </p>
                                                <p>Ngày đăng ký: {{ \Carbon\Carbon::parse($contract->created_date)->format('d/m/Y H:i:s')}} </p>
                                                
                                                <div class="choose">
                                                    <h6><a href=""> {{ $contract->semester_name}}</a></h6>
                                                </div>                                                
                                            </div>    
                                        </div>
                                        @endforeach   
                                    </div>
                                </article>     
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** History Registration Area End ***** -->
    @else
    <!-- ***** Login Area Starts ***** -->
    <section class="section" id="login">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 align-self-center">
                    <div class="left-text-content">
                        <div class="section-heading">
                            <h6>Đăng nhập hệ thống</h6>
                            <h2>Việc đăng ký phòng chỉ áp dụng cho những sinh viên đã liên hệ với BQL và được cấp tài khoản đăng nhập</h2>
                        </div>
                        <p>Nếu bạn chưa có tài khoản. Hãy liên hệ với BQL để thực hiện đăng ký.</p>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="phone">
                                    <i class="fa fa-phone"></i>
                                    <h4>Phone Numbers</h4>
                                    <span><a href="#">078-945-1245</a><br><a href="#">090-451-2032</a></span>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="message">
                                    <i class="fa fa-envelope"></i>
                                    <h4>Emails</h4>
                                    <span><a href="#">ktxUTE@gmail.com</a><br><a href="#">BQLute@gmail.com</a></span>
                                </div>
                            </div>
                        </div>                    

                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="contact-form">
                        <form id="contact" action="/login" method="post">
                          <div class="row">
                            <div class="col-lg-12">
                                <h4>Đăng nhập hệ thống</h4>
                            </div>
                            <div class="col-lg-12">
                            <span>@include('admin.alert')</span>
                            </div>
                            <div class="col-sm-12">
                              <fieldset>
                                <input name="code" type="text" id="code" placeholder="Mã số sinh viên *" required="">
                              </fieldset>
                            </div>
                            <div class="col-sm-12">
                              <fieldset>
                              <input name="password" type="password" id="password" placeholder="Mật khẩu" required="">
                            </fieldset>
                            </div></br>
                            <div class="col-lg-12">
                              <fieldset>
                                <button type="submit" id="form-submit" class="main-button-icon">Đăng nhập</button>
                              </fieldset>
                            </div>
                          </div>
                          @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Login Area Ends ***** -->

    @endif
    <!-- ***** Footer Start ***** -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-xs-12">
                    <div class="right-text-content">
                            <ul class="social-icons">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                            </ul>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="logo">
                        <a href="index.html"><img src="assets/images/white-logo.png" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-4 col-xs-12">
                    <div class="left-text-content">
                        <p>© Ky Tuc Xa Trương Dai Hoc Su Pham Ky Thuat.
                        
                        <br>Dai hoc Da Nang.</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>

  @include('index.footer')
  </body>
</html>