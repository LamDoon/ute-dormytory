
<!DOCTYPE html>
<html lang="en">
  <head>
      @include('index.head')      
</head>
    
    <body>
    
    <!-- ***** Preloader Start ***** -->
    <div id="preloader">
        <div class="jumper">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>  
    <!-- ***** Preloader End ***** -->
    
    
    <!-- ***** Header Area Start ***** -->
    <header class="header-area header-sticky">
        @include('index.navbar')
    </header>
    <!-- ***** Header Area End ***** -->

    <!-- ***** Main Banner Area Start ***** -->
    <div id="top">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4">
                    <div class="left-content">
                        <div class="inner-content">
                            <h4>Ký túc xá</h4>
                            <h6>Đại học Sư phạm Kỹ thuật</h6>
                            <h6>Đại học Đà Nẵng</h6>
                            <div class="main-white-button scroll-to-section">
                                <a href="#login">Đăng nhập</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="main-banner header-text">
                        <div class="Modern-Slider">
                          <!-- Item -->
                          <div class="item">
                            <div class="img-fill">
                                <img src="/assets/index/assets/images/banner1.jpg" alt="">
                            </div>
                          </div>
                          <!-- // Item -->
                          <!-- Item -->
                          <div class="item">
                            <div class="img-fill">
                                <img src="/assets/index/assets/images/banner2.jpg" alt="">
                            </div>
                          </div>
                          <!-- // Item -->
                          <!-- Item -->
                          <div class="item">
                            <div class="img-fill">
                                <img src="/assets/index/assets/images/banner3.jpg" alt="">
                            </div>
                          </div>
                          <!-- // Item -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ***** Main Banner Area End ***** -->

    <!-- ***** About Area Starts ***** -->
    <section class="section" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="left-text-content">
                        <div class="section-heading">
                            <h6>Tin tức</h6>
                            <h2>Sinh viên Đà Nẵng dọn đồ, nhường ký túc xá làm nơi cách ly</h2>
                        </div>
                        <p>Ký túc xá một số trường đại học tại Đà Nẵng đang được gấp rút dọn dẹp để thành nơi ở cho người cách ly COVID-19, sinh viên gấp rút dọn đồ chuyển đi.
                            </br></br>
                            Tại trường ĐH Sư phạm kỹ thuật Đà Nẵng, nhân viên nhà trường và sinh viên cũng đang tích cực thu dọn đồ đạc để kịp tiếp nhận người cách ly trong ít ngày tới. Ông Nguyễn Văn Dũng, phụ trách Tổ quản lý ký túc xá, cho biết trường sẽ dành 52 phòng, mỗi phòng 8 giường cho người cách ly. Sinh viên tại đây sẽ được dồn vào ở các khu ký túc xá còn lại.
                        </p>
                        <div class="row">
                            <div class="col-4">
                                <img src="/assets/index/assets/images/new1.jpg" alt="">
                            </div>
                            <div class="col-4">
                                <img src="/assets/index/assets/images/new2.jpg" alt="">
                            </div>
                            <div class="col-4">
                                <img src="/assets/index/assets/images/new3.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="right-content">
                        <div class="thumb">
                            <a rel="nofollow" href="https://www.youtube.com/watch?v=qseQBNT2hXY"><i class="fa fa-play"></i></a>
                            <img src="/assets/index/assets/images/image_video.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** About Area Ends ***** -->

    <!-- ***** Intros Area Starts ***** -->
    <section class="section" id="intros">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="section-heading">
                        <h6>GiớI thiệu</h6>                        
                    </div>
                </div>
            </div>
        </div>
        <div class="menu-item-carousel">
            <div class="col-lg-12">
                <div class="owl-menu-item owl-carousel">
                    @foreach($intros as $intro)
                    <div class="item">
                        <div class='card card1' style="background-image: url(/assets/index/assets/images/{{ $intro->image }});">
                        <!-- <img width="284" height="400" src="/assets/index/assets/images/{{ $intro->image }}"/>                         -->
                            <div class="choose"><h6>New</h6></div>
                            <div class='info'>                             
                              <h1 class='title'>{{ $intro->title }}</h1>
                              <p class='description'>{{ $intro->content }}</p>
                              <div class="main-text-button">
                                  <div class="scroll-to-section"><a href="#reservation">Make Reservation <i class="fa fa-angle-down"></i></a></div>
                              </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Intros Area Ends ***** -->

    <!-- ***** Note Area Starts ***** -->
    <section class="section" id="notes">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 offset-lg-4 text-center">
                    <div class="section-heading">
                        <h6>Dành cho sinh viên đang nội trú tại Ký túc xá</h6>
                        <h2>Thông Báo</h2>
                    </div>
                </div>
            </div>
            @if (session()->has('student')) 
            <div class="row">
                @foreach($notes as $note)
                <div class="col-lg-4">
                    <div class="chef-item">                        
                        <div class="down-content">
                            <h4>{{ $note->title }}</h4>
                            <span>{{ $note->content }}</span></br>
                            <p>{{ $note->created_at }}</p>
                        </div>
                    </div>
                </div>            
              @endforeach
            </div>
            @endif
        </div>
    </section>
    <!-- ***** Note Area Ends ***** -->  

    <!-- ***** Login Area Starts ***** -->
    <section class="section" id="login">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 align-self-center">
                    <div class="left-text-content">
                        <div class="section-heading">
                            <h6>Đăng nhập hệ thống</h6>
                            <h2>Tại đây sinh viên có thể đăng nhập để thực hiện đăng ký phòng tại KTX</h2>
                        </div>
                        <p>Việc đăng nhập chỉ áp dụng cho những sinh viên đã được BQL cấp tài khoản</p>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="phone">
                                    <i class="fa fa-phone"></i>
                                    <h4>Phone Numbers</h4>
                                    <span><a href="#">078-945-1245</a><br><a href="#">090-451-2032</a></span>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="message">
                                    <i class="fa fa-envelope"></i>
                                    <h4>Emails</h4>
                                    <span><a href="#">ktxUTE@gmail.com</a><br><a href="#">BQLute@gmail.com</a></span>
                                </div>
                            </div>
                        </div>                    

                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="contact-form">
                        <form id="contact" action="/login" method="post">
                          <div class="row">
                            <div class="col-lg-12">
                                <h4>Đăng nhập hệ thống</h4>
                            </div>
                            <div class="col-lg-12">
                            <span>@include('admin.alert')</span>
                            </div>
                            <div class="col-sm-12">
                              <fieldset>
                                <input name="code" type="text" id="code" placeholder="Mã số sinh viên *" required="">
                              </fieldset>
                            </div>
                            <div class="col-sm-12">
                              <fieldset>
                              <input name="password" type="password" id="password" placeholder="Mật khẩu" required="">
                            </fieldset>
                            </div></br>
                            <div class="col-lg-12">
                              <fieldset>
                                <button type="submit" id="form-submit" class="main-button-icon">Đăng nhập</button>
                              </fieldset>
                            </div>
                          </div>
                          @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Login Area Ends ***** -->

    <!-- ***** Rooms Area Starts ***** -->
    <section class="section" id="rooms">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 offset-lg-4 text-center">
                    <div class="section-heading">
                        <h6>Danh sách phòng Ký túc xá</h6>
                        <h2>Học kỳ đăng ký {{ $semester->name }}</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="row" id="tabs">
                        <div class="col-lg-12">
                            <div class="heading-tabs">
                                <div class="row">
                                    <div class="col-lg-6 offset-lg-3">
                                        <ul>
                                          <li><a href='#tabs-a'>Khu A</a></li>
                                          <li><a href='#tabs-b'>Khu B</a></li>
                                          <li><a href='#tabs-c'>Khu C</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <section class='tabs-content'>
                                <article id='tabs-a'>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="row">
                                                <div class="left-list">                                                   
                                                    @foreach ($aF as $room)
                                                    <div class="col-lg-12">
                                                        <div class="tab-item">
                                                            <img src="assets/images/tab-item-01.png" alt="">
                                                            <h4>Phòng {{ $room->code }}</h4>
                                                            <p>Loại phòng: {{ $room->type }}</p>
                                                            <p>Số lượng đăng ký cho phép: {{ $room->regist_max }}. Số lượng đã đăng ký: {{ $room->regist_num }}</p>
                                                            @if($room->regist_num == $room->regist_max)
                                                            <div class="cancel">
                                                                <h6><a>Hết</a></h6>
                                                            </div>
                                                            @else
                                                            <div class="choose">
                                                                <h6><a href="{{ url('contract/'.$room->id) }}">Chọn</a></h6>
                                                            </div>
                                                            @endif
                                                        </div>
                                                    </div>                                                   
                                                    @endforeach                                                  
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="row">
                                                <div class="right-list">
                                                @foreach ($aL as $room)
                                                    <div class="col-lg-12">
                                                        <div class="tab-item">
                                                            <img src="assets/images/tab-item-04.png" alt="">
                                                            <h4>Phòng {{ $room->code }}</h4>
                                                            <p>Loại phòng: {{ $room->type }}</p>
                                                            <p>Số lượng đăng ký cho phép: {{ $room->regist_max }}. Số lượng đã đăng ký: {{ $room->regist_num }}</p>
                                                            @if($room->regist_num == $room->regist_max)
                                                            <div class="cancel">
                                                                <h6><a >Hết</a></h6>
                                                            </div>
                                                            @else
                                                            <div class="choose">
                                                                <h6><a href="{{ url('contract/'.$room->id) }}">Chọn</a></h6>
                                                            </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    @endforeach                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>  
                                <article id='tabs-b'>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="row">
                                                <div class="left-list">                                                   
                                                    @foreach ($bF as $room)
                                                    <div class="col-lg-12">
                                                        <div class="tab-item">
                                                            <img src="assets/images/tab-item-01.png" alt="">
                                                            <h4>Phòng {{ $room->code }}</h4>
                                                            <p>Loại phòng: {{ $room->type }}</p>
                                                            <p>Số lượng đăng ký cho phép: {{ $room->regist_max }}. Số lượng đã đăng ký: {{ $room->regist_num }}</p>
                                                            @if($room->regist_num == $room->regist_max)
                                                            <div class="cancel">
                                                                <h6><a >Hết</a></h6>
                                                            </div>
                                                            @else
                                                            <div class="choose">
                                                                <h6><a href="{{ url('contract/'.$room->id) }}">Chọn</a></h6>
                                                            </div>
                                                            @endif
                                                        </div>
                                                    </div>                                                   
                                                    @endforeach                                                  
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="row">
                                                <div class="right-list">
                                                @foreach ($bL as $room)
                                                    <div class="col-lg-12">
                                                        <div class="tab-item">
                                                            <img src="assets/images/tab-item-04.png" alt="">
                                                            <h4>Phòng {{ $room->code }}</h4>
                                                            <p>Loại phòng: {{ $room->type }}</p>
                                                            <p>Số lượng đăng ký cho phép: {{ $room->regist_max }}. Số lượng đã đăng ký: {{ $room->regist_num }}</p>
                                                            @if($room->regist_num == $room->regist_max)
                                                            <div class="cancel">
                                                                <h6><a >Hết</a></h6>
                                                            </div>
                                                            @else
                                                            <div class="choose">
                                                                <h6><a href="{{ url('contract/'.$room->id) }}">Chọn</a></h6>
                                                            </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    @endforeach                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>  
                                <article id='tabs-c'>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="row">
                                                <div class="left-list">                                                   
                                                    @foreach ($cF as $room)
                                                    <div class="col-lg-12">
                                                        <div class="tab-item">
                                                            <img src="assets/images/tab-item-01.png" alt="">
                                                            <h4>Phòng {{ $room->code }}</h4>
                                                            <p>Loại phòng: {{ $room->type }}</p>
                                                            <p>Số lượng đăng ký cho phép: {{ $room->regist_max }}. Số lượng đã đăng ký: {{ $room->regist_num }}</p>
                                                            @if($room->regist_num == $room->regist_max)
                                                            <div class="cancel">
                                                                <h6><a >Hết</a></h6>
                                                            </div>
                                                            @else
                                                            <div class="choose">
                                                                <h6><a href="{{ url('contract/'.$room->id) }}">Chọn</a></h6>
                                                            </div>
                                                            @endif
                                                        </div>
                                                    </div>                                                   
                                                    @endforeach                                                  
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="row">
                                                <div class="right-list">
                                                @foreach ($cL as $room)
                                                    <div class="col-lg-12">
                                                        <div class="tab-item">
                                                            <img src="assets/images/tab-item-04.png" alt="">
                                                            <h4>Phòng {{ $room->code }}</h4>
                                                            <p>Loại phòng: {{ $room->type }}</p>
                                                            <p>Số lượng đăng ký cho phép: {{ $room->regist_max }}. Số lượng đã đăng ký: {{ $room->regist_num }}</p>
                                                            @if($room->regist_num == $room->regist_max)
                                                            <div class="cancel">
                                                                <h6><a >Hết</a></h6>
                                                            </div>
                                                            @else
                                                            <div class="choose">
                                                                <h6><a href="{{ url('contract/'.$room->id) }}">Chọn</a></h6>
                                                            </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    @endforeach                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>   
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Rooms Area Ends ***** --> 
    
    <!-- ***** Footer Start ***** -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-xs-12">
                    <div class="right-text-content">
                            <ul class="social-icons">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                            </ul>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="logo">
                        <a href="index.html"><img src="assets/images/white-logo.png" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-4 col-xs-12">
                    <div class="left-text-content">
                        <p>© Copyright Klassy Cafe Co.
                        
                        <br>Design: TemplateMo</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>

  @include('index.footer')
  </body>
</html>