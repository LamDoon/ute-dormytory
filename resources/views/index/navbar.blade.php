<div class="container">
            <div class="row">
                <div class="col-12">
                    <nav class="main-nav">
                        <!-- ***** Logo Start ***** -->
                        <a href="/" class="logo">
                            <img src="/images/logo_banner.png" align="logo UTE dormitory">
                        </a>
                        <!-- ***** Logo End ***** -->
                        <!-- ***** Menu Start ***** -->
                        <ul class="nav">
                            <li class="scroll-to-section"><a href="#top" class="active">Trang chủ</a></li>
                            <li class="scroll-to-section"><a href="#intros">Giới thiệu</a></li>                      
                            <li class="scroll-to-section"><a href="#notes">Thông báo</a></li>
                            <li class="scroll-to-section"><a href="#rooms">Danh sách phòng</a></li>                             
                            @if (session()->has('student'))
                            <li class="submenu">
                                <a href="javascript:;">{{ session('name')}}</a>
                                <ul>
                                    <li><a href="{{ url('/profile') }}">Thông tin cá nhân</a></li>
                                    <li><a href="{{ url('/history') }}">Lịch sử đăng ký</a></li>
                                    <li><a href="{{ url('/vehicle') }}">Đăng ký vé xe</a></li>
                                    <li><a href="{{ url('/logout') }}">Đăng xuất</a></li>
                                </ul>
                            </li>
                             <!-- <li class=""><a rel="sponsored" href="https://templatemo.com" target="_blank">External URL</a></li> -->
                            @else
                            <li class="scroll-to-section"><a href="#login">Đăng nhập</a></li>                             
                        @endif
                        </ul>        
                        <a class='menu-trigger'>
                            <span>Menu</span>
                        </a>
                        <!-- ***** Menu End ***** -->
                    </nav>
                </div>
            </div>
        </div>