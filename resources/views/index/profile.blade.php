
<!DOCTYPE html>
<html lang="en">
  <head>
      @include('index.head')      
</head>
    
    <body>
    
    <!-- ***** Preloader Start ***** -->
    <div id="preloader">
        <div class="jumper">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>  
    <!-- ***** Preloader End ***** -->
    
    
    <!-- ***** Header Area Start ***** -->
    <header class="header-area header-sticky">
        @include('index.navbar')
    </header>
    <!-- ***** Header Area End ***** -->

    <!-- ***** Main Banner Area Start ***** -->
    @if (session()->has('student')) 
    <!-- ***** Main Banner Area End ***** -->

    <!-- ***** Reservation Room Area Starts ***** -->
    <section class="section" id="registration">
        <div class="container">
        <div class="social-container">
			<span>@include('admin.alert')</span>
			</div>   
            <div class="row">
                <div class="section-heading">
                            <h6>Thông tin cá nhân</h6>
                            <h2>Sinh viên không thể cập nhật thông tin của mình trên hệ thống. Nếu có sai sót, vui lòng liên hệ ban quản lý để được hỗ trợ.</h2>
                </div>
                <div class="col-lg-6 align-self-center">
                    <div class="contact-form">
                        <form id="contact" action="" method="post">
                          <div class="row">
                            <div class="col-lg-12">
                                <h4>Thông tin sinh viên</h4>
                            </div>
                            <div class="col-lg-6 col-sm-12">
                                <fieldset>
                                    <h6>Họ và tên: {{ $student->name }}</h6>  
                                </fieldset>                           
                            </div>
                            <div class="col-lg-6 col-sm-12"> 
                                <fieldset> 
                                    @if ($student->gender == 0)                            
                                    <h6>Giới tính: Nữ</h6>  
                                    @else
                                    <h6>Giới tính: Nam</h6>
                                    @endif
                                </fieldset>                         
                            </div>
                            <div class="col-lg-6 col-sm-12">
                                <fieldset>
                                    <h6>CMND/CCCD: {{ $student->card_id }}</h6>
                                </fieldset>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <fieldset>
                                    <h6>Ngày sinh: {{ $student->dob }}</h6>
                                </fieldset>
                            </div>
                            <div class="col-md-6 col-sm-12">
                              <fieldset>
                              <h6>Quốc tịch: {{ $student->national }}</h6>
                              </fieldset>
                            </div>
                            <div class="col-md-6 col-sm-12">
                              <fieldset>
                              <h6>Dân tộc: {{ $student->ethnic }}</h6>
                              </fieldset>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <fieldset>
                                    <h6>MSSV: {{ $student->code }}</h6>
                                </fieldset> 
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <fieldset>
                                    <h6>Khoa: {{ $student->depart }}</h6>
                                </fieldset>
                            </div>
                            <div class="col-md-6 col-sm-12">
                              <fieldset>
                              <h6>Email: {{ $student->email }}</h6>
                              </fieldset>
                            </div>
                            <div class="col-md-6 col-sm-12">
                              <fieldset>
                              <h6>Số điện thoại: {{ $student->phone }}</h6>
                              </fieldset>
                            </div>
                            <div class="col-lg-12">
                              <fieldset>
                                <h6>Địa chỉ: {{ $student->address }}</h6>                             
                              </fieldset>
                            </div>                                             
                          </div>
                        </form>
                    </div>
                </div>
                
                <div class="col-lg-6">
                    <div class="contact-form">
                        <form id="contact" action="" method="post">
                          <div class="row">
                            <div class="col-lg-12">
                                <h4>Cập nhật mật khẩu</h4>
                            </div>
                            <div class="col-sm-12">
                              <fieldset>
                                  <label>Mật khẩu cũ</label>
                                    <input value="{{ $student->password }}" type="password" readonly></input>
                              </fieldset>
                            </div>
                            <div class="col-sm-12">
                              <fieldset>
                                  <label>Mật khẩu mới</label>
                                    <input name="password" type="password" placeholder="Nhập mật khẩu mới" required></input>
                              </fieldset>
                            </div>                                                                 
                            <div class="col-lg-12">
                              <fieldset>
                                <button type="submit" id="form-submit" class="main-button-icon">Đổi mật khẩu</button>
                              </fieldset>
                            </div>                           
                          </div>
                          @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Reservation Room Area End ***** -->  
@else 
<!-- ***** Login Area Starts ***** -->
<section class="section" id="login">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 align-self-center">
                    <div class="left-text-content">
                        <div class="section-heading">
                            <h6>Đăng nhập hệ thống</h6>
                            <h2>Việc đăng ký phòng chỉ áp dụng cho những sinh viên đã liên hệ với BQL và được cấp tài khoản đăng nhập</h2>
                        </div>
                        <p>Nếu bạn chưa có tài khoản. Hãy liên hệ với BQL để thực hiện đăng ký.</p>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="phone">
                                    <i class="fa fa-phone"></i>
                                    <h4>Phone Numbers</h4>
                                    <span><a href="#">078-945-1245</a><br><a href="#">090-451-2032</a></span>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="message">
                                    <i class="fa fa-envelope"></i>
                                    <h4>Emails</h4>
                                    <span><a href="#">ktxUTE@gmail.com</a><br><a href="#">BQLute@gmail.com</a></span>
                                </div>
                            </div>
                        </div>                    

                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="contact-form">
                        <form id="contact" action="/login" method="post">
                          <div class="row">
                            <div class="col-lg-12">
                                <h4>Đăng nhập hệ thống</h4>
                            </div>
                            <div class="col-lg-12">
                            <span>@include('admin.alert')</span>
                            </div>
                            <div class="col-sm-12">
                              <fieldset>
                                <input name="code" type="text" id="code" placeholder="Mã số sinh viên *" required="">
                              </fieldset>
                            </div>
                            <div class="col-sm-12">
                              <fieldset>
                              <input name="password" type="password" id="password" placeholder="Mật khẩu" required="">
                            </fieldset>
                            </div></br>
                            <div class="col-lg-12">
                              <fieldset>
                                <button type="submit" id="form-submit" class="main-button-icon">Đăng nhập</button>
                              </fieldset>
                            </div>
                          </div>
                          @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Login Area Ends ***** -->
    @endif
    <!-- ***** Footer Start ***** -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-xs-12">
                    <div class="right-text-content">
                            <ul class="social-icons">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                            </ul>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="logo">
                        <a href="index.html"><img src="assets/images/white-logo.png" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-4 col-xs-12">
                    <div class="left-text-content">
                        <p>© Ky Tuc Xa Trương Dai Hoc Su Pham Ky Thuat.
                        
                        <br>Dai hoc Da Nang.</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>

  @include('index.footer')
  </body>
</html>