<?php

use App\Http\Controllers\Admin\ActivityController;
use App\Http\Controllers\Admin\ElectricityBillController;
use App\Http\Controllers\Admin\ElectricityIndexController;
use App\Http\Controllers\Admin\EmployeeController;
use App\Http\Controllers\Admin\LoginController;
use App\Http\Controllers\Admin\MainController;
use App\Http\Controllers\Admin\RoomController;
use App\Http\Controllers\Admin\StudentController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Index\MainIndexController;
use App\Http\Controllers\Admin\IntroController;
use App\Http\Controllers\Admin\NotificationController;
use App\Http\Controllers\Admin\ReportController;
use App\Http\Controllers\Admin\RoomRegistrationFormController;
use App\Http\Controllers\Admin\SemesterController;
use App\Http\Controllers\Admin\StudentParentController;
use App\Http\Controllers\Admin\VehicleController;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|*/


Route::get('/admin/login',[LoginController::class,'getLogin'])->name('login');
Route::post('/admin/login/store',[LoginController::class,'postLogin']);
Route::get('/pass',[LoginController::class,'create']);
Route::get('/admin/logout',[LoginController::class,'getLogout']);

Route::get('/test',[IntroController::class,'getCheckbox']);
Route::post('/test',[IntroController::class,'postCheckbox']);


Route::middleware(['auth'])->group(function (){
    Route::prefix('admin')->group(function (){
        Route::get('/',[MainController::class,'index'])->name('admin');
        Route::get('main',[MainController::class,'index']);
        
        Route::prefix('employees')->group(function (){
            Route::get('/',[UserController::class,'index']);
            Route::get('list',[UserController::class,'index']);
            Route::get('create',[UserController::class,'create']);
            Route::post('create',[UserController::class,'store']);
            Route::get('detail/{user}',[UserController::class,'detail']);
            Route::get('active/{user}',[UserController::class,'getActive']);
            Route::post('active/{user}',[UserController::class,'postActive']);
            Route::get('edit/{user}',[UserController::class,'show']);
            Route::post('edit/{user}',[UserController::class,'update']);
            Route::post('edit/atm/{user}',[UserController::class,'postAtm']);
        });

        Route::prefix('rooms')->group(function (){
            Route::get('/',[RoomController::class,'index']);
            Route::get('list',[RoomController::class,'index']);
            Route::get('update',[RoomController::class,'reset']);
            Route::get('detail/{room}',[RoomController::class,'detail']);
            Route::post('detail/{room}',[RoomController::class,'update']);
        });

        Route::prefix('students')->group(function (){
            Route::get('/',[StudentController::class,'index']);
            Route::get('list',[StudentController::class,'index']);
            Route::get('create',[StudentController::class,'create']);
            Route::post('create',[StudentController::class,'store']);
            Route::get('detail/{student}',[StudentController::class,'detail']);
            Route::get('active/{student}',[StudentController::class,'getActive']);
            Route::post('active/{student}',[StudentController::class,'postActive']);
            Route::get('vehicle/{student}',[StudentController::class,'getVehicle']);
            Route::post('vehicle/{student}',[StudentController::class,'postVehicle']);
            Route::get('delete/{id}',[StudentController::class,'delete']);
        });

        Route::prefix('introductions')->group(function (){
            Route::get('/',[IntroController::class,'index']);
            Route::get('list',[IntroController::class,'index']);
            Route::get('create',[IntroController::class,'create']);
            Route::post('create',[IntroController::class,'store']);
            Route::get('show/{intro}',[IntroController::class,'showIntro']);
            Route::get('delete/{id}',[IntroController::class,'delete']);
        });

        Route::prefix('notifications')->group(function (){
            Route::get('/',[NotificationController::class, 'index']);
            Route::get('list',[NotificationController::class, 'index']);
            Route::get('create',[NotificationController::class,'create']);
            Route::post('create',[NotificationController::class,'store']);
            Route::get('delete/{id}',[NotificationController::class,'delete']);
        });

        Route::prefix('contracts')->group(function (){
            Route::get('/',[RoomRegistrationFormController::class, 'index']);
            Route::get('list',[RoomRegistrationFormController::class, 'index']);
            Route::get('approve',[RoomRegistrationFormController::class,'approveForm'])->name('approveContract');
            Route::get('approve/{contract}',[RoomRegistrationFormController::class,'detail']);
            Route::post('approve/{contract}',[RoomRegistrationFormController::class,'update']);
            Route::post('approve/cancel/{contract}',[RoomRegistrationFormController::class,'cancel']);
            Route::get('fee',[RoomRegistrationFormController::class, 'getFeeList']);
            Route::get('fee/update/{contract}',[RoomRegistrationFormController::class, 'updatePaid']);
            Route::get('delete/{id}',[RoomRegistrationFormController::class,'delete']);
        });

        Route::prefix('semesters')->group(function (){
            Route::get('/',[SemesterController::class, 'create']);
            Route::get('create',[SemesterController::class,'create']);
            Route::post('create',[SemesterController::class,'store']);
            Route::get('vehicle',[SemesterController::class,'getCreateVehicle']);
            Route::post('vehicle/{semester}',[SemesterController::class,'postCreateVehicle']);
        });

        Route::prefix('electric-index')->group(function (){
            Route::get('/',[ElectricityIndexController::class, 'index']);
            Route::get('list',[ElectricityIndexController::class,'index']);
            Route::get('create',[ElectricityIndexController::class,'create']);
            Route::post('create',[ElectricityIndexController::class,'store']);
            Route::get('update/{electric}',[ElectricityIndexController::class,'update']);
            Route::get('delete/{id}',[ElectricityIndexController::class,'delete']);
        });

        Route::prefix('electricity')->group(function (){
            Route::get('/',[ElectricityBillController::class, 'index']);
            Route::get('list',[ElectricityBillController::class,'index']);
            Route::get('create',[ElectricityBillController::class,'create']);
            Route::post('create',[ElectricityBillController::class,'store']);
            Route::get('update/{electric}',[ElectricityIndexController::class,'update']);
            Route::get('fee/update/{bill}',[ElectricityBillController::class, 'updatePaid']);
            Route::get('delete/{id}',[ElectricityBillController::class,'delete']);
        });

        Route::prefix('vehicles')->group(function (){
            Route::get('/',[VehicleController::class, 'index']);
            Route::get('list',[VehicleController::class,'index']); 
            Route::get('delete/{id}',[VehicleController::class,'delete']);
        });   
        
        Route::prefix('parents')->group(function (){
            Route::get('/',[StudentParentController::class, 'index']);
            Route::get('list',[StudentParentController::class,'index']);
            Route::get('create',[StudentParentController::class,'create']);
            Route::post('create',[StudentParentController::class,'store']); 
        });

        Route::prefix('reports')->group(function (){
            Route::get('/',[ReportController::class, 'getReport']);
            Route::get('detail',[ReportController::class,'getReport']);
            Route::get('export-excel',[ReportController::class,'exportIntoExcel']);
            Route::get('export-csv',[ReportController::class,'exportIntoCSV']);
        });

        Route::prefix('activities')->group(function (){
            Route::get('/',[ActivityController::class, 'commendList']);
            Route::get('commend',[ActivityController::class, 'commendList']);
            Route::get('commend/create',[ActivityController::class,'createCommend']); 
            Route::post('commend/create',[ActivityController::class,'storeCommend']); 
            Route::get('commend/delete/{id}',[ActivityController::class,'deleteCommend']);
            Route::get('discipline',[ActivityController::class, 'disciplineList']);
            Route::get('discipline/create',[ActivityController::class,'createDiscipline']); 
            Route::post('discipline/create',[ActivityController::class,'storeDiscipline']); 
            Route::get('discipline/delete/{id}',[ActivityController::class,'deleteDiscipline']);
        });  

    });
});

Route::group(['prefix'=>''],function(){
    Route::get('/',[MainIndexController::class,'index'])->name('home');
    Route::get('home',[MainIndexController::class,'index']);
    Route::post('login',[StudentController::class,'login']);   
    Route::get('logout',[StudentController::class,'logout']);  
    Route::get('contract/{room}',[MainIndexController::class,'getRegist']);
    Route::post('contract/{room}',[MainIndexController::class,'postRegist']); 
    Route::get('vehicle',[MainIndexController::class,'getRegistVehicle']);
    Route::post('vehicle',[MainIndexController::class,'postRegistVehicle']);   
    Route::get('profile',[MainIndexController::class,'getProfile']);
    Route::post('profile',[MainIndexController::class,'changePass']);
    Route::get('history',[MainIndexController::class,'history']);
});


